version=$1
onlyone=${#version}
onlyone=$[onlyone-1]
todayversion=${version:${onlyone}:1}
NEWCONTENT=1.2.5.$(date +%Y%m%d)
if [[ ! "${version}" =~ ^"${NEWCONTENT}".* ]]
then
  echo $NEWCONTENT
  exit
fi
echo $version
echo $NEWCONTENT
if [[ "${version}" == "${NEWCONTENT}" ]]
then
  version=${NEWCONTENT}a
  todayversion="a"
fi
digit=`printf "%d" "'${todayversion}"`
if [[ $digit -le 121 ]]
then
  lastVersion=`echo $[digit+1] | awk '{printf("%c\n", $1)}'`
  writeText=${version:0:$[${#version}-1]}${lastVersion}
  echo $writeText
else
  hour=`date "+%H"`
  writeText=${version}${hour}a
  echo $writeText
fi
