FILENAME="/usr/local/apache-maven-3.8.3/conf/settings.xml"
KEYWORD="localRepository"
CONTENT=`cat ${FILENAME} | awk -v ORS=',,,,,' '{print $0}' | sed -r 's/<!--[^-]+-->/a/' | awk -v RS=',,,,,' '{print $0}' | grep -n ${KEYWORD}`
MPATH=`echo ${CONTENT} | awk -F "[:<> ]+" '{print $3}'`
echo "maven path="$MPATH
cd $MPATH
GROUPID="com.yapai"
ARTIFICATID=("syn-thingsboard" "shuiliandong" "shuiliandong-admin" "shuiliandong-interface"  "soft.nanshan" "soft.nanshan-api" "soft.wuxingshan-api");
#ARTIFICATID=("syn-thingsboard")
FILEPATH=`echo ${GROUPID} | sed 's/\./\//'`
cd ${FILEPATH}
HOMEPATH=`pwd`
for i in ${ARTIFICATID[*]}
do
    SUBPATH=`echo project/$i | jq -r '.sub_path'`
    cd $SUBPATH
    PWD=`pwd`
    echo "当前路径："+$PWD
    FILES=`ls -l | awk '{print $9}' | sort -n`
    FILESIZE=`ls | wc -l`
    echo "文件个数："${FILESIZE}
    let index=0
    for file in ${FILES[*]}
    do
        let "index++"
        echo ${file}
        if [[ $index -eq $FILESIZE ]]
        then
            echo "the last one"
        else
            #delete
            rm -rf ${file}
        fi
    done
    cd $HOMEPATH
done
