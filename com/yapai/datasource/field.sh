# --------------------------------configuration--------------------------------
DATABASE_HOST="192.168.0.190"
DATABASE_USER_NAME="yapai"
DATABASE_PASSWORD="ypkj@1234"
# --------------------------------configuration--------------------------------
DIR=$( dirname "$(readlink -f  ${BASH_SOURCE[0]})" )

database_name=$1
table_name=$2
files=`ls -1 ${DIR}/template`
for file in ${files[@]}
do
  real_sql=`cat ${DIR}/template/${file} | sed -n 's/{{table_name}}/'"${table_name}"'/g;p'`
  res=`mysql -h${DATABASE_HOST} -u${DATABASE_USER_NAME} -p${DATABASE_PASSWORD} -D${database_name} -e "${real_sql}" 2>&1 | sed -n '2,$p'`
  errors_regex=`ls -1 ${DIR}/error`
  for regex in ${errors_regex[@]}
  do
    REG=`cat ${DIR}/error/${regex} | sed -n '1p'`
    if [[ "${res}" =~ ${REG} ]]
    then
      echo -e "----------------------------------\033[32m errors \033[0m----------------------------------"
      echo -e "\033[31m ${BASH_REMATCH[1]} \033[0m"
      echo -e "----------------------------------\033[32m errors \033[0m----------------------------------"
      exit
    fi
  done
done
