####################configuration####################

NEED_CHECK_PROJECT_NAME=$1

DATABASE_HOST="127.0.0.1"
DATABASE_USERNAME='root'
DATABASE_PASSWORD='123456'

####################configuration####################
dd=(`mysql -h${DATABASE_HOST} -u${DATABASE_USERNAME} -p${DATABASE_PASSWORD} -e 'show databases' 2>&1 | sed -n '2,$p' | grep "${NEED_CHECK_PROJECT_NAME}_"`)

echo -e "\033[33m total:${#dd[*]} \033[0m"

for d in ${dd[@]}
do

  echo "database="$d

  mysql -h${DATABASE_HOST} -u${DATABASE_USERNAME} -p${DATABASE_PASSWORD} -e 'drop database '"${d}"'' 2>&1 | sed -n '2,$p'
done
