#!/bin/bash
need_hander_xml=$1
name=`uname -s`
if [[ "${name}" = "Linux" ]]; then
sed -i 's/ifnull/\nifnull/g;s/ifnull(\(\w\+\.*\w\+\),\([^(]\+\))/\nCASE WHEN (\1 is null or LENGTH(\1)=0) THEN\n\2\nELSE\n\1\nEND/g' ${need_hander_xml}
else
  # mac gsed can't work nomally
  # sed - '' 's/ifnull/\nifnull/g' ${need_hander_xml}
  # sed -i '' 's/ifnull(\([^(]*\))/\nifnull(\1)\n/g'
  sed -i '' 's/ifnull(\([^(]*\))/\nifnull(\1)\n/g;' ${need_hander_xml}
  sed -i '' 's/ifnull(\(.*\),\([^(]*\))/\nCASE WHEN (\1 is null or LENGTH(\1)=0) THEN\n\2\nELSE\n\1\nEND/g' ${need_hander_xml}
fi
