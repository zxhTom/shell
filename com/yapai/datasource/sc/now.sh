need_hander_xml=$1
name=`uname -s`
if [[ "${name}" = "Linux" ]]; then
  sed -i 's/Sysdate()/NOW()/Ig' ${need_hander_xml}
else
  sed -i '' 's/Sysdate()/NOW()/Ig' ${need_hander_xml}
fi
