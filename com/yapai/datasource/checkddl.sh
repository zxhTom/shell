####################configuration####################

name=`uname -s`
if [[ "${name}" = "Linux" ]]; then
currentHome=$( dirname "$(readlink -f  ${BASH_SOURCE[0]})" )
else
currentHome=$( dirname "$(greadlink -f  ${BASH_SOURCE[0]})" )
fi
YAPAI_HOME=$(dirname "$currentHome")

BASE_INIT_PATH="${HOME}/temp/yapai/system-init"

NEED_CHECK_PROJECT_NAME=$1

PROJECT_DATABASE_NAME=(`cat ${YAPAI_HOME}/project/${NEED_CHECK_PROJECT_NAME} | jq -r '.database_name'`)
NEED_CHECK_PROJECT_NAME=(`cat ${YAPAI_HOME}/project/${NEED_CHECK_PROJECT_NAME} | jq -r '.project_name'`)

PROJECT_NAME=${BASE_INIT_PATH##*/}

COMPARE_PATH="${HOME}/temp/compare"

PARENT_MAIN_PROJECT_PATH="${HOME}/zxh/project/yapai/${NEED_CHECK_PROJECT_NAME}"

MAIN_PROJECT_PATH=$(dirname `find ${PARENT_MAIN_PROJECT_PATH} -depth -name "src" | xargs ls -1d`)

DATABASE_HOST="127.0.0.1"
DATABASE_USERNAME='root'
DATABASE_PASSWORD='123456'

DIR=$( dirname "$(readlink -f  ${BASH_SOURCE[0]})" )
####################configuration####################

cd ${BASE_INIT_PATH}
git pull
REMOTE_URL=`git remote get-url origin`

if [[ -d ${COMPARE_PATH}/${PROJECT_NAME} ]]
then
  rm -rf ${COMPARE_PATH}/${PROJECT_NAME}
fi

git clone ${REMOTE_URL} ${COMPARE_PATH}/${PROJECT_NAME}

if [[ ! -f ${COMPARE_PATH}/${PROJECT_NAME}/10_db_mysql/${NEED_CHECK_PROJECT_NAME}.sql ]]
then
  echo -e "\033[31m not found your project , please check... \033[0m" 
  exit
fi

# cp -rv ${COMPARE_PATH}/${PROJECT_NAME}/10_db_mysql/${NEED_CHECK_PROJECT_NAME}.sql ${MAIN_PROJECT_PATH}/src/main/resources/sql
cd ${MAIN_PROJECT_PATH}
git pull
BASE_FILE_SQL=(old ${NEED_CHECK_PROJECT_NAME})
for bfs in ${BASE_FILE_SQL[@]}
do
  DATABASE_NAME=${NEED_CHECK_PROJECT_NAME}_${RANDOM}

  echo $bfs---${DATABASE_NAME}

  mysql -h${DATABASE_HOST} -u${DATABASE_USERNAME} -p${DATABASE_PASSWORD} -e 'create database '"${DATABASE_NAME}"'' 2>/dev/null

  echo "start import"

  files=(`find ${MAIN_PROJECT_PATH}/src/main/resources/sql/update -type f | xargs ls -1d | sort -r`)

  files[${#files[@]}]=${MAIN_PROJECT_PATH}/src/main/resources/sql/${bfs}.sql

  for (( i=${#files[@]};i>0;i--))
  do

    import_file=${files[i-1]}
    echo -e "\033[32m ${import_file} \033[0m"

    IMPORT_RESULT=`mysql -h${DATABASE_HOST} -u${DATABASE_USERNAME} -p${DATABASE_PASSWORD} -f -D${DATABASE_NAME} < ${import_file} 2>&1`
    # IMPORT_RESULT=`mysql -h${DATABASE_HOST} -u${DATABASE_USERNAME} -p${DATABASE_PASSWORD} -D${DATABASE_NAME} < /home/zxhtom/temp/mysql/nanshan.sql 2>&1`
    errors_regex=`ls -1 ${DIR}/error`
    for regex in ${errors_regex[@]}
    do
    # echo "${IMPORT_RESULT}"
      # REG='ERROR\s+1064.*:(.*)'
      REG=`cat ${DIR}/error/${regex} | sed -n '1p'`
      if [[ "${IMPORT_RESULT}" =~ ${REG} ]]
      then
        echo -e "----------------------------------\033[32m errors \033[0m----------------------------------"
        echo -e "\033[31m ${BASH_REMATCH[1]} \033[0m"
        echo -e "----------------------------------\033[32m errors \033[0m----------------------------------"
        mysql -h${DATABASE_HOST} -u${DATABASE_USERNAME} -p${DATABASE_PASSWORD} -e 'drop database '"${DATABASE_NAME}"''
        exit
      fi
    done
  done

  # mysql -h${DATABASE_HOST} -u${DATABASE_USERNAME} -p${DATABASE_PASSWORD} -e 'drop database '"${DATABASE_NAME}"'' 2>/dev/null
done



