|脚本名称|说明|示例|
|----|----|----|
| checkddl | 检测项目中全量脚本和增量脚本正确性  | sh checkddl.sh shuiliandong |
| cleandatabase | 清理产生的垃圾数据库   | sh cleandatabase.sh shuiliandong  |
| compare | 全量脚本与目标数据库对比差别 | 可以查看差别的table和差别的column sh compare.sh shuiliandong |
| outputddl | 对比目标数据库产生sql ， 对全量修改，对增量新增 | sh outputddl shuiliandong t_sys_user |
| fieldsh | 对指定table生成公共字段 | sh field.sh shuiliandong t_sys_user  |
| tranmysql2postgres | 将数据库从mysql转换成postgresql | sh tranmysql2postgres.sh shuiliandong |
| onekeytranxmlsql | 将xml文件中mysql特有语法进行转换 | sh onekeytranxmlsql.sh xml_folder |
