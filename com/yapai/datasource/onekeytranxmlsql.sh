DIR=$( dirname "$(readlink -f  ${BASH_SOURCE[0]})" )
echo $DIR
base_folder=$1
xml_list=()
if [[ -d $base_folder ]]
then
  cd ${base_folder}
  xml_list=`ls -1 ${base_folder} | sed "s:^:\`pwd\`/:"`
else
  xml_list[${#xml_list[@]}]=${base_folder}
fi
cd ${DIR}/sc
sc_list=`ls -1 ${DIR}/sc | sed "s:^:\`pwd\`/:"`
for xml_file_name in ${xml_list[*]}
do
  for sc_file_name in ${sc_list[*]}
  do
    echo "sh ${sc_file_name} ${xml_file_name}" >> ${HOME}/temp/tran.log 2>&1
    sh ${sc_file_name} ${xml_file_name}
  done
done
