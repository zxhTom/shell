TARGET_DATABASE_HOST="192.168.0.190"
TARGET_DATABASE_USERNAME='yapai'
TARGET_DATABASE_PASSWORD='ypkj@1234'

DATABASE_HOST="127.0.0.1"
DATABASE_USERNAME='root'
DATABASE_PASSWORD='123456'

NEED_CHECK_PROJECT_NAME=$1
PARENT_MAIN_PROJECT_PATH="${HOME}/zxh/project/yapai/${NEED_CHECK_PROJECT_NAME}"
# MAIN_PROJECT_PATH=$(dirname `find ${PARENT_MAIN_PROJECT_PATH} -depth -name "src" | xargs ls -1d`)
MAIN_PROJECT_PATH=`find ${PARENT_MAIN_PROJECT_PATH} -depth -regex ".*src.*Application\.java" | awk -F 'src' '{print $1}'`
TARGET_DATABASE_NAME=$1
TARGET_TABLE_NAME=$2

name=`uname -s`
if [[ "${name}" = "Linux" ]]; then
currentHome=$( dirname "$(readlink -f  ${BASH_SOURCE[0]})" )
else
currentHome=$( dirname "$(greadlink -f  ${BASH_SOURCE[0]})" )
fi
YAPAI_HOME=$(dirname "$currentHome")
# TARGET_DATABASE_HOST=(`cat ${YAPAI_HOME}/enviroment/${environment} | jq -r '.ip'`)
# TARGET_DATABASE_USERNAME=(`cat ${YAPAI_HOME}/enviroment/${environment} | jq -r '.database_user_name'`)
# TARGET_DATABASE_PASSWORD=(`cat ${YAPAI_HOME}/enviroment/${environment} | jq -r '.database_password'`)
PROJECT_DATABASE_NAME=(`cat ${YAPAI_HOME}/project/${NEED_CHECK_PROJECT_NAME} | jq -r '.database_name'`)
TEMP_FOLDER_PATH="${HOME}/temp/mysql"
CURRENT_DATE=`date +'%Y%m%d'`


DATABASE_NAME=${NEED_CHECK_PROJECT_NAME}_${RANDOM}
echo $DATABASE_NAME
mysql -h${DATABASE_HOST} -u${DATABASE_USERNAME} -p${DATABASE_PASSWORD} -e 'create database '"${DATABASE_NAME}"'' 2>&1 | sed -n '2,$p'
IMPORT_RESULT=`mysql -h${DATABASE_HOST} -u${DATABASE_USERNAME} -p${DATABASE_PASSWORD} -D${DATABASE_NAME} < ${MAIN_PROJECT_PATH}/src/main/resources/sql/${NEED_CHECK_PROJECT_NAME}.sql 2>&1 | sed -n '2,$p'`

array=(${TARGET_TABLE_NAME//,/ })  
echo > ${MAIN_PROJECT_PATH}/src/main/resources/sql/update/${TARGET_DATABASE_NAME}_${CURRENT_DATE}.sql
for table in ${array[@]}
do
  database_table_exists=`mysql -h${DATABASE_HOST} -u${DATABASE_USERNAME} -p${DATABASE_PASSWORD} -e "use ${DATABASE_NAME} ; show tables" 2>&1 | sed -n '2,$p' | grep -E "^${table}$"`
  echo "dddd:"$database_table_exists
  if [[ -z ${database_table_exists} ]]
  then
    echo "create new whole sql"
    `mysqldump -h${TARGET_DATABASE_HOST} -u${TARGET_DATABASE_USERNAME} -p${TARGET_DATABASE_PASSWORD} --default-character-set=utf8 --skip-add-locks --skip-extended-insert --compact --skip-comments ${PROJECT_DATABASE_NAME} --tables ${table} | sed 's/\/\*.*\*\/;*//g' > ${TEMP_FOLDER_PATH}/${TARGET_DATABASE_NAME}_${CURRENT_DATE}.sql 2>&1 | sed -n '3,$p'`
    # whole
    new_result=`cat ${TEMP_FOLDER_PATH}/${TARGET_DATABASE_NAME}_${CURRENT_DATE}.sql |  tr '\n' '|' | sed 's/\//\\\\\//g'`
    cat ${TEMP_FOLDER_PATH}/${TARGET_DATABASE_NAME}_${CURRENT_DATE}.sql >> ${MAIN_PROJECT_PATH}/src/main/resources/sql/update/${TARGET_DATABASE_NAME}_${CURRENT_DATE}.sql
    # new_result="INSERT INTO whole_config VALUES \('1729294452920832','制县八算通','aliquip','http:\/\/ummyimage.com\/400x400','http:\/\/dummyimage.com\/400x400','2023-05-19 13:09:27','2023-05-19 13:09:27',NULL,NULL,1,NULL\)"
    # new_result="CREATE TABLE whole_config (| id varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '主键',| name varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '名称',| logo varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT 'logo',| login_pic varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '登录页背景图',| driver_pic varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '驾驶仓背景图',| create_time datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',| update_time datetime DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',| create_by varchar(100) DEFAULT NULL COMMENT '创建人',| update_by varchar(100) DEFAULT NULL COMMENT '更新人',| delete_flag int DEFAULT NULL COMMENT '删除标记',| version int DEFAULT NULL COMMENT '版本记录',| PRIMARY KEY ( id )|) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COMMENT='系统全局配置';||INSERT INTO whole_config VALUES ('1729294452920832','制县八算通','aliquip','http://dummyimage.com/400x400','http://dummyimage.com/400x400','2023-05-19 13:09:27','2023-05-19 13:09:27',NULL,NULL,1,NULL),('1729294486819328','长回层阶','esse culpa Duis','http://dummyimage.com/400x400','http://dummyimage.com/400x400','2023-05-19 13:09:27','2023-05-19 13:09:27',NULL,NULL,1,NULL),('1729356084974080','1111',NULL,NULL,NULL,'2023-05-19 15:27:55','2023-05-19 15:27:55','1','1',1,1),('1729356310077952','111','tttt',NULL,NULL,'2023-05-19 15:28:50','2023-05-19 15:28:50','1','1',1,1),('1729364207362560','1111','/prod_api/v190/baojinnang/get?file=2023/05/19/f3ea68cd-f73b-4297-8712-d054cb110676.png','/prod_api/v190/baojinnang/get?file=2023/05/19/ea5613ae-9006-4d44-842d-35ebbbc0deed.png','/prod_api/v190/baojinnang/get?file=2023/05/19/233ce7e9-2f95-4783-a804-db78ea484188.png','2023-05-19 16:00:58','2023-05-19 16:00:58','1','1',1,1),('1729365119967744','1111','/prod_api/v190/baojinnang/get?file=2023/05/19/f3ea68cd-f73b-4297-8712-d054cb110676.png','/prod_api/v190/baojinnang/get?file=2023/05/19/03848273-2c3e-4c46-b73d-5c80cb9523c6.png','/prod_api/v190/baojinnang/get?file=2023/05/19/233ce7e9-2f95-4783-a804-db78ea484188.png','2023-05-19 16:00:58','2023-05-19 16:00:58','1','1',1,1),('1729370606191104','1111','/prod_api/v190/baojinnang/get?file=2023/05/19/f6621c84-c7f6-459c-a591-293a556a6192.png','/prod_api/v190/baojinnang/get?file=2023/05/19/93c10ade-afe9-49ba-96db-8bd447426c70.jpg','/prod_api/v190/baojinnang/get?file=2023/05/19/1246a2b4-9220-484d-a95f-b281c8c07201.jpg','2023-05-19 16:00:58','2023-05-19 16:00:58','1','1',0,1),('1729381271790080','特压技','ut dolore non voluptate','http://dummyimage.com/400x400','http://dummyimage.com/400x400','2023-05-19 17:10:25','2023-05-19 17:10:25','1','1',1,1),('1729381904720384','机它达','mollit Lorem Ut et','http://dummyimage.com/400x400','http://dummyimage.com/400x400','2023-05-19 17:12:59','2023-05-19 17:12:59','1','1',1,1),('1729383562129920','下感量','cillum','http://dummyimage.com/400x400','http://dummyimage.com/400x400','2023-05-19 17:19:44','2023-05-19 17:19:44','1','1',1,1)"
    line_number=`cat ${MAIN_PROJECT_PATH}/src/main/resources/sql/${NEED_CHECK_PROJECT_NAME}.sql | grep -n "INSERT TABLE ABOVE" | awk -F ":" '{print $1}'`
    if [[ -z ${line_number} ]]
    then
      line_number=`cat ${MAIN_PROJECT_PATH}/src/main/resources/sql/${NEED_CHECK_PROJECT_NAME}.sql | grep -nE "CREATE\s+TABLE" | sed -n '$p' | awk -F ":" '{print $1}'`
    fi
    echo $line_number
    echo $new_result
    sed -i ''"${line_number}"'i\'"${new_result}"'' ${MAIN_PROJECT_PATH}/src/main/resources/sql/${NEED_CHECK_PROJECT_NAME}.sql 
    rs=`cat ${MAIN_PROJECT_PATH}/src/main/resources/sql/${NEED_CHECK_PROJECT_NAME}.sql  |  tr '|' '\n' | sed -n '1,$p'`
    echo ---
    echo "$rs" > ${MAIN_PROJECT_PATH}/src/main/resources/sql/${NEED_CHECK_PROJECT_NAME}.sql
  else
    echo "append sql"
    echo hhh
    # no export data
    `mysqldump -d -h${TARGET_DATABASE_HOST} -u${TARGET_DATABASE_USERNAME} -p${TARGET_DATABASE_PASSWORD} --default-character-set=utf8 --skip-add-locks --skip-extended-insert --compact --skip-comments ${PROJECT_DATABASE_NAME} --tables ${table} | sed 's/\/\*.*\*\/;*//g' > ${TEMP_FOLDER_PATH}/${TARGET_DATABASE_NAME}_${CURRENT_DATE}.sql 2>&1 | sed -n '3,$p'`
    # sed -i '/^$/d' ${TEMP_FOLDER_PATH}/${TARGET_DATABASE_NAME}_${CURRENT_DATE}.sql
    new_result=`cat ${TEMP_FOLDER_PATH}/${TARGET_DATABASE_NAME}_${CURRENT_DATE}.sql |  tr '\n' '|' | sed 's/\//\\\\\//g'`
    temp_content=`mysqldiff --server1=${DATABASE_USERNAME}:${DATABASE_PASSWORD}@${DATABASE_HOST} --server2=${TARGET_DATABASE_USERNAME}:${TARGET_DATABASE_PASSWORD}@${TARGET_DATABASE_HOST} --character-set=utf8 --difftype=sql ${DATABASE_NAME}.${table}:${NEED_CHECK_PROJECT_NAME}.${table} | sed  's/#.*/ /g;s/\`//g;s/'"${DATABASE_NAME}"'\.//g' | grep -E '\w+'`
    active_line_number=` echo ${temp_content} | wc -l` 
    if [[ ${active_line_number} -ge 0 ]]
    then
      echo "${temp_content}" >> ${MAIN_PROJECT_PATH}/src/main/resources/sql/update/${TARGET_DATABASE_NAME}_${CURRENT_DATE}.sql
      if [[ -z ${temp_content} ]]
      then
        continue
      fi
      echo ......${table}
      echo ......${new_result}
      sed -i '/INSERT.*'"${table}"'/d' ${MAIN_PROJECT_PATH}/src/main/resources/sql/${NEED_CHECK_PROJECT_NAME}.sql
      rs=`cat ${MAIN_PROJECT_PATH}/src/main/resources/sql/${NEED_CHECK_PROJECT_NAME}.sql  | tr '\n' '|' | sed  's/CREATE TABLE \`'"${table}"'\`[^;]\+;/'"${new_result}"'/g' | tr '|' '\n' | sed -n '1,$p'`
      echo --- 
      echo "$rs" > ${MAIN_PROJECT_PATH}/src/main/resources/sql/${NEED_CHECK_PROJECT_NAME}.sql
    else
      echo nnnnn
    fi
  fi
done
