DROP PROCEDURE IF EXISTS proc_tempPro;
delimiter ;;
CREATE PROCEDURE proc_tempPro()
BEGIN
SELECT count(*) into @count FROM INFORMATION_SCHEMA.Columns 
WHERE table_schema= DATABASE() AND table_name="t_assets" AND column_name="repository_id4";
if(@count=0) THEN
 ALTER TABLE t_assets add repository_id4 VARCHAR(255)  NULL COMMENT '所属仓库';
end if;
end;;
delimiter ;
call proc_tempPro;

DROP PROCEDURE IF EXISTS proc_tempPro;
