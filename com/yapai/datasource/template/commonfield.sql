ALTER TABLE {{table_name}} ADD create_time DATETIME DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间';
ALTER TABLE {{table_name}} ADD update_time DATETIME DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间';
ALTER TABLE {{table_name}} ADD create_by VARCHAR(100) NULL COMMENT '创建人';
ALTER TABLE {{table_name}} ADD update_by VARCHAR(100) NULL COMMENT '更新人';
ALTER TABLE {{table_name}} ADD delete_flag INT NULL COMMENT '删除标记';
ALTER TABLE {{table_name}} ADD version INT NULL COMMENT '版本记录';
