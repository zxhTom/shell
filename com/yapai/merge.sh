MAINPATH=$1
CURRENTBRANCH=$2
MERGEBRANCH=$3
while getopts ":m:c:M:" opt
do
    case $opt in
        m)
        MAINPATH=$OPTARG
        echo "您输入的主路径="${MAINPATH}
        ;;
        c)
        CURRENTBRANCH=$OPTARG
        echo "您输入的当前分支="+${CURRENTBRANCH}
        ;;
        M)
        MERGEBRANCH=$OPTARG
        echo "您输入的合并分支="${MERGEBRANCH}
        ;;
        ?)
        echo "未知参数"
        exit 1;;
    esac
done
cd $MAINPATH
git fetch origin ${MERGEBRANCH}
MSTATUS=`git status`
if [[ ${MSTATUS} =~ "You have unmerged paths" ]]
then
    echo "not merge up"
else
  RESULT=`git merge origin/${MERGEBRANCH}`
  echo "@@@"${RESULT}
  if [[ ${RESULT} =~ "Automatic merge failed" ]]
  then
      echo "sorry"
  elif [[ ${RESULT} =~ "Merge made by the 'recursive' strategy" ]] || [[ ${RESULT} =~ "Fast-forward" ]]
  then
      echo "合并成功"
      git push -u origin ${CURRENTBRANCH}
  elif [[ ${RESULT} =~ "Already up to date" ]]
  then
      echo "code 不需要更新，已经是最新...."
      git push -u origin ${CURRENTBRANCH}
  fi
fi
