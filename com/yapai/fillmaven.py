import xml.etree.ElementTree as ET
import sys
import os
print(sys.argv[1])
with open(sys.argv[1], 'tr', encoding='utf-8') as rf:
    tree = ET.parse(rf)

current_file=os.path.abspath(sys.argv[0])
parent_path=os.path.dirname(current_file)
root=tree.getroot()
default_namespacetom=root.tag.split('}')[0][1:]
ns={}
ns['tomns']=default_namespacetom
tem=ET.parse(os.path.join(parent_path,'mvnset.xml'))
chi=tem.getroot()
for node in chi:
    firstNode=list(node)
    profiles=tree.find(node.tag)
    if profiles!=None:
        for temno in firstNode:
                    id_node=temno.find('.//tomns:id',ns)
            id=id_node.text
            print('id='+id)
            search_source_node=profiles.findall('.//'+temno.tag+'['+id_node.tag+'="'+id+'"]')
            if len(search_source_node)>1:
                print("{} file is wrong file , exits more than one unique tag".format(sys.argv[1]))
            elif len(search_source_node)==1:
                print('is existes:'+search_source_node[0].tag)
            else:
                print("not found tag , you can insert it....")
                print(temno.tag+"added...")
                profiles.insert(0,temno)
tree.write(sys.argv[1],default_namespace=default_namespacetom)
