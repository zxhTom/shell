# 该脚本主要用来升级发包期间修改各服务版本
# 可以修改主项目版本号，也可以修改项目中引用的指定坐标的版本
FILEPATH=$1
GROUPID=$2
ARTIFACTID=$3
FILENAME=$4
while getopts ":f:g:a:" opt
do
    case $opt in
        f)
        FILENAME=$OPTARG
        echo "您输入的文件配置:$FILENAME"
        ;;
        g)
        GROUPID=$OPTARG
        echo "您输入的groupid配置:$GROUPID"
        ;;
        a)
        ARTIFACTID=$OPTARG
        echo "您输入的artifactid配置:$ARTIFACTID"
        ;;
        ff)
        FILENAME=$OPTARG
        echo "您输入的带修改文件为:$FILENAME"
        ;;
        ?)
        echo "未知参数"
        exit 1;;
    esac
done
echo "开始修改版本号"
NEWCONTENT=1.2.5.$(date +%Y%m%d)
OLD_VERSION=`cat ${FILENAME} | grep -n -A 2 '<groupId>'"${GROUPID}"'<\/groupId>'| grep -n -A 1 '<artifactId>'"${ARTIFACTID}"'<\/artifactId>' | grep 'version' | awk -F "[<> ]+" '{print $3}'`
NEWCONTENT=sh version.sh $OLD_VERSION
LINE=`cat ${FILENAME} | grep -n -A 1 '<groupId>'"${GROUPID}"'<\/groupId>'| grep -n '<artifactId>'"${ARTIFACTID}"'<\/artifactId>' | awk -F "[:-]+" '{print $2}'`
echo 具体行号:$LINE
if [[ -z $LINE  ]]
then
    echo 未匹配
    exit
fi
VERSIONOLDCONTENT=`sed -n ''"$((LINE+1))"'p' ${FILENAME}| grep '[0-9a-zA-Z\.-]+' -Eo | sed -n '2p'`
echo ${VERSIONOLDCONTENT}
#gsed -i  ''"$((LINE+1))"'c\'"${NEWCONTENT}"'' pom.xml
name=`uname -s`
if [[ "${name}" = "Linux" ]]; then
sed -i ''"$((LINE+1))"'s/'"${VERSIONOLDCONTENT}"'/'"${NEWCONTENT}"'/' ${FILENAME}
else
sed -i '' ''"$((LINE+1))"'s/'"${VERSIONOLDCONTENT}"'/'"${NEWCONTENT}"'/' ${FILENAME}
fi
