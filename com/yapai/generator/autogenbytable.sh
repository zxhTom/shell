name=`uname -s`
if [[ "${name}" = "Linux" ]]; then
currentHome=$( dirname "$(readlink -f  ${BASH_SOURCE[0]})" )
else
currentHome=$( dirname "$(greadlink -f  ${BASH_SOURCE[0]})" )
fi
project=$1
environment=$2
tables=$3
BASE_PROJECT_PATH="${HOME}/zxh/project/yapai"
YAPAI_HOME=$(dirname "$currentHome")
PROJECT_NAME=(`cat ${YAPAI_HOME}/project/${project} | jq -r '.project_name'`)
# MAIN_PROJECT_PATH=$(dirname `find ${BASE_PROJECT_PATH}/${PROJECT_NAME} -depth -name "src" | xargs ls -1d`)
MAIN_PROJECT_PATH=`find ${BASE_PROJECT_PATH}/${PROJECT_NAME} -depth -regex ".*src.*Application\.java" | awk -F 'src' '{print $1}'`
MAIN_PROJECT_PATH=${MAIN_PROJECT_PATH%?}
result=`sh ${YAPAI_HOME}/login/token.sh ypkj $environment 2>&1 | sed -n '$p'` 
IP=(`cat ${YAPAI_HOME}/enviroment/${environment} | jq -r '.ip'`)
IP="127.0.0.1"
PORT=(`cat ${YAPAI_HOME}/project/${project} | jq -r '.port'`)
INTERFACE_PREFIX=(`cat ${YAPAI_HOME}/project/${project} | jq -r '.interface_prefix'`)

FILE_NAME=`printf $tables | openssl md5 | awk '{print $NF}'`
echo ${FILE_NAME}
echo $result
if [[ ! -d ${HOME}/temp/download ]]
then
  mkdir -p ${HOME}/temp/download
fi
if [[ -f ${HOME}/temp/download/${FILE_NAME}.zip ]]
then
  rm -rf ${HOME}/temp/download/${FILE_NAME}.zip
fi
if [[ -d ${HOME}/temp/download/${FILE_NAME} ]]
then
  rm -rf ${HOME}/temp/download/${FILE_NAME}
fi
echo "curl -H "Authorization:${result}" http://${IP}:${PORT}/${INTERFACE_PREFIX}/framework_generator/generator/code\?tableNames\=${tables}\&removeCommonField\=true -o ~/temp/download/${FILE_NAME}.zip"
curl -H "Authorization:${result}" http://${IP}:${PORT}/${INTERFACE_PREFIX}/framework_generator/generator/code\?tableNames\=${tables}\&removeCommonField\=true -o ~/temp/download/${FILE_NAME}.zip
if [[ -f ${HOME}/temp/download/${FILE_NAME}.zip ]]
then
  unzip ${HOME}/temp/download/${FILE_NAME}.zip -d ${HOME}/temp/download/${FILE_NAME}
  find ${HOME}/temp/download/${FILE_NAME} -type f -regex ".*Application\.java$" | xargs rm -rf 
  echo "cp -rv ${HOME}/temp/download/${FILE_NAME}/main/java ${MAIN_PROJECT_PATH}/src/main"
  cp -rv ${HOME}/temp/download/${FILE_NAME}/main/java ${MAIN_PROJECT_PATH}/src/main
  module_name=$(dirname `find ${HOME}/temp/download/${FILE_NAME} -type d -name controller`)
  module_name=${module_name##*/}
  real_module_name=$(dirname `find ${MAIN_PROJECT_PATH}/src/main/java -type d -name ${module_name}`)
  echo --
  echo ${real_module_name}
  echo ---
  cd ${real_module_name}
  # git add ${module_name}
fi





