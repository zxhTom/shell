FILEPATH=$1
KEYNAME=$2
while getopts ":f:k:i:" opt
do
    case $opt in
        f)
        FILEPATH=$OPTARG
        echo "您输入的文件配置:$FILEPATH"
        ;;
        k)
        KEYNAME=$OPTARG
        echo "您输入的key配置:$KEYNAME"
        ;;
        i)
        install=$OPTARG
        echo "您输入的打包开关配置:$install"
        ;;
        ?)
        echo "未知参数"
        exit 1;;
    esac
done
VALUE=`cat ${FILEPATH} | grep ''"${KEYNAME}"'=(.)*' -E | awk -F "[=]+" '{print $2}'`
if [[ -z ${VALUE} ]]
then
  VALUE=`cat ${FILEPATH} | jq -r '.'"${KEYNAME}"''`
fi
echo $VALUE
