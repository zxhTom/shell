#!/bin/bash
i=0
str='#'
ch=('|' '\' '-' '/')
index=0
port=$1
while [ $i -le 25 ]
do
    printf "[%-25s][%d%%][%c]\r" $str $(($i*4)) ${ch[$index]}
    str+='#'
    let i++
    let index=i%4
    pid=`lsof -i:$port | awk -F "[ ]+" 'NR>1{print $2}'`
    if [[ -n $pid ]]
    then
        if [[ $i <25 ]]
        then
            i=25;
            str=#########################
        fi
    fi
    sleep 1
done
printf "\n"
echo "安装完成"

