totaltime=`sed -n '/\[INFO\] Total time:  [0-9]*[.:][0-9]* [a-zA-Z]*/p'  mvninstall/$1/mvninstall.log`
echo $totaltime
if [[ ${totaltime} =~ [^0-9]*([0-9]*)([\.:]+)([0-9]*)[" "]*([a-zA-Z]*) ]]
then
    echo ${BASH_REMATCH[0]}
    echo ${BASH_REMATCH[1]}
    echo ${BASH_REMATCH[2]}
    echo ${BASH_REMATCH[3]}
    if [[ ${BASH_REMATCH[2]} == '.'  ]]
    then
        echo $(( BASH_REMATCH[1]+1  ))
    else
        echo $(( BASH_REMATCH[1]*60+BASH_REMATCH[3]  ))
    fi
fi
