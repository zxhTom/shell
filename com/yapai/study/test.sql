-- MySQL dump 10.13  Distrib 5.7.39, for Linux (x86_64)
--
-- Host: 127.0.0.1    Database: nacos_config
-- ------------------------------------------------------
-- Server version	8.0.31

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping data for table `config_info`
--
-- WHERE:  data_id='test'

LOCK TABLES `config_info` WRITE;
/*!40000 ALTER TABLE `config_info` DISABLE KEYS */;
INSERT INTO `config_info` VALUES (2,'test','DEFAULT_GROUP','host:\n  self: 192.168.0.62\n  postgres: ${host.self}\n  mysql: ${host.self}\n  td: ${host.self}\n  kafka: ${host.self}\n  redis: ${host.self}\n\nserver:\n  port: 8712\n  servlet:\n    context-path: /nanshan\n\nspring:\n  redis:\n    database: 0\n    host: ${host.redis}\n    lettuce:\n      pool:\n        max-active: 8\n        max-idle: 8\n        max-wait: -1ms\n        min-idle: 0\n      shutdown-timeout: 100ms\n    password: yapai\n    port: 6379\n\n  datasource:\n    type: com.alibaba.druid.pool.DruidDataSource\n    driverClassName: com.mysql.cj.jdbc.Driver\n\n    druid:\n      url: jdbc:mysql://${host.mysql}:3306/nanshan?useUnicode=true&characterEncoding=utf8&zeroDateTimeBehavior=convertToNull&useSSL=false&serverTimezone=Asia/Shanghai\n      username: yapai\n      password: ypkj@1234\n      initialSize: 5\n      minIdle: 10\n      maxActive: 20\n      maxWait: 60000\n      timeBetweenEvictionRunsMillis: 60000\n      minEvictableIdleTimeMillis: 300000\n      maxEvictableIdleTimeMillis: 900000\n      validationQuery: SELECT 1\n      testWhileIdle: true\n      testOnBorrow: true\n      testOnReturn: false\n      filters: stat, wall\n\nsecurity:\n  oauth2:\n    client:\n      client-id: Auth\n      client-secret: user123\n      access-token-uri: ${gateway.baseurl}/sld/oauth/token\n      user-authorization-uri: ${gateway.baseurl}/sld/oauth/authorize\n    resource:\n      jwt:\n        key-uri: ${gateway.baseurl}/sld/oauth/token_key\n      token-info-uri: ${gateway.baseurl}/sld/oauth/check_token\n      prefer-token-info: true\n\nyapai:\n  checkAssetCode: true\n\nkafka:\n  bootstrap-servers: ${host.kafka}:9092\n  producer:\n    retries: 3\n    batch-size: 16\n    linger: 5\n  consumer:\n    topic: PrdEquipDbsync\n    enable-auto-commit: false\n    session-timeout: 10000\n    group-id: nanshan\n    auto-offset-reset: latest\n    max-poll-records: 2\n    thread-min: 5\n    thread-max: 10\ngateway:\n  baseurl: http://${host.self}:8700/\n\n\nsld:\n  baseurl: ${gateway.baseurl}sld/\n  username: ypkj\n  password: e10adc3949ba59abbe56e057f20f883e\n\nthingsboard:\n  url: ${host.self}:8080\n  enable: true\n','7b2f3f5c6cef80bb39917aced5ea4806','2022-10-25 09:17:46','2022-10-25 09:17:46',NULL,'172.17.0.1','','',NULL,NULL,NULL,'yaml',NULL,'');
/*!40000 ALTER TABLE `config_info` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-10-24 22:23:15
