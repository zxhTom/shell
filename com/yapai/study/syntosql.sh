if [[ ! -d sql ]]
then
  mkdir sql
fi
mysqldump --login-path=test -t nacos_config config_info --where="data_id='nanshan-dev.yml'" > sql/nanshan.sql
mysqldump --login-path=test -t nacos_config config_info --where="data_id='sld-dev.yml'" > sql/sld.sql
files=`ls sql`
for file in ${files[@]}
do
  file_name=${file%.*}
  res=`cat sql/${file} | grep -iE "INSERT INTO \\\`config_info\\\`[ ]*VALUES \([0-9]*,[ ]*'"${file_name}"-dev.yml'"`
  res=${res//\\n/\\\\n}
  line_num=`cat nacos.sql | grep -inE "INSERT INTO \\\`config_info\\\`[ ]*VALUES \([0-9]*,[ ]*'"${file_name}"-dev.yml'"`
  line_num=`echo ${line_num} | awk -F "[:]+" '{print $1}' `
  if [[ -z ${line_num} ]]
  then
    echo nnn
  else
    echo ${line_num}
    sed -i ''"${line_num}"'c\'"${res}"'' nacos.sql
  fi
done
