-- MySQL dump 10.13  Distrib 5.7.39, for Linux (x86_64)
--
-- Host: 127.0.0.1    Database: nacos_config
-- ------------------------------------------------------
-- Server version	8.0.31

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping data for table `config_info`
--
-- WHERE:  data_id='sld-dev.yml'

LOCK TABLES `config_info` WRITE;
/*!40000 ALTER TABLE `config_info` DISABLE KEYS */;
INSERT INTO `config_info` VALUES (4,'sld-dev.yml','DEFAULT_GROUP','server:\n  port: 8704\nhost:\n  self: 127.0.0.1\n  postgres: ${host.self}\n  mysql: ${host.self}\n  td: ${host.self}\n  kafka: ${host.self}\n  redis: ${host.self}\nspring:\n  # druid数据源配置\n  datasource:\n    type: com.alibaba.druid.pool.DruidDataSource\n    driverClassName: com.mysql.cj.jdbc.Driver\n    druid:\n      # 主库数据源\n      master:\n        url: jdbc:mysql://${host.mysql}:3306/shuiliandong?useUnicode=true&characterEncoding=utf-8&zeroDateTimeBehavior=convertToNull&useSSL=false&serverTimezone=GMT%2B8&nullCatalogMeansCurrent=true\n        username: yapai\n        password: ypkj@1234\n      # 从库数据源\n      slave:\n        # 从数据源开关/默认关闭\n        enabled: false\n        url:\n        username:\n        password:\n      # 初始连接数\n      initialSize: 5\n      # 最小连接池数量\n      minIdle: 10\n      # 最大连接池数量\n      maxActive: 20\n      # 配置获取连接等待超时的时间\n      maxWait: 60000\n      # 配置间隔多久才进行一次检测，检测需要关闭的空闲连接，单位是毫秒\n      timeBetweenEvictionRunsMillis: 60000\n      # 配置一个连接在池中最小生存的时间，单位是毫秒\n      minEvictableIdleTimeMillis: 300000\n      # 配置一个连接在池中最大生存的时间，单位是毫秒\n      maxEvictableIdleTimeMillis: 900000\n      # 配置检测连接是否有效\n      validationQuery: SELECT 1 FROM DUAL\n      testWhileIdle: true\n      testOnBorrow: false\n      testOnReturn: false\n      webStatFilter:\n        enabled: true\n      statViewServlet:\n        enabled: true\n        # 设置白名单，不填则允许所有访问\n        allow:\n        url-pattern: /druid/*\n        # 控制台管理用户名和密码\n        login-username:\n        login-password:\n      filter:\n        stat:\n          enabled: true\n          # 慢SQL记录\n          log-slow-sql: true\n          slow-sql-millis: 1000\n          merge-sql: true\n        wall:\n          config:\n            multi-statement-allow: true\n\n  # redis配置\n  redis:\n    # Redis数据库索引\n    database: 0\n    # Redis服务器地址\n    host: ${host.redis}\n    port: 6379\n    password: yapai\n    jedis:\n      pool:\n        # 连接池最大连接数（使用负值表示没有限制）\n        max-active: 8\n        # 连接池最大阻塞等待时间（使用负值表示没有限制）\n        max-wait: -1\n        # 连接池中的最大空闲连接\n        max-idle: 100\n        # 连接池中的最小空闲连接\n        min-idle: 0\n    # 连接超时时间（毫秒）\n    timeout: 10000\n    # 集群配置(多个节点使用英文逗号分隔)\n    #    cluster:\n    #      nodes: 192.168.1.183:9001,192.168.1.183:9002\n  cloud:\n    inetutils:\n      preferred-networks: 192.168.0\n      use-only-site-local-interfaces: true\n      ignoredInterfaces:\n        - VirtualBox.*\n        - Hyper-V.*\n        - Npcap.*\n        - VMware.*\n        - vEthernet.*\n  main:\n    allow-bean-definition-overriding: true\n# MyBatis配置\nmybatis:\n  # 搜索指定包别名\n  typeAliasesPackage: com.yapai.shuiliandong.**.model,com.yapai.syn.**.model\n  # 配置mapper的扫描，找到所有的mapper.xml映射文件\n  mapperLocations: classpath*:mapping/**/*Mapper.xml\n  # 加载全局的配置文件\n  configLocation: classpath:mybatis/mybatis-config.xml\n# PageHelper分页插件\npagehelper:\n  helperDialect: mysql\n  reasonable: true\n  supportMethodsArguments: true\n  params: count=countSql\n\nlogging:\n  level:\n    com.yapai: debug\n    org.springframework: warn\n  io.swagger.models.parameters.AbstractSerializableParameter: error\n\nmy:\n  wx:\n    #name: 智慧总务测试环境\n    #appid: wx0502575a4bc34be9\n    #secret: 8f859b1c9c9bfbe0e13285982573cd00\n    #service: http://172.16.1.106:8723/weChatAccount/\n    #name: 智慧总务\n    #appid: wx9851090be78a3d61\n    #secret: 78edc0a7f4b0408176a2aaba9fc261a9\n    #service: http://127.0.0.1:8723/weChatAccount/\n    # name: 亚派能效LA\n    # appid: wxa0e6956ec175e11d\n    # secret: 1eb782973b451f13154f555062e33116\n    # service: http://172.16.1.117:8723/weChatAccount/\n    name: 海慈医院\n    appid: wx9851090be78a3d61\n    secret: 78edc0a7f4b0408176a2aaba9fc261a9\n    service: http://${host.self}:8723/weChatAccount/\n  mp:\n    appid: wx0779e0e76a8085e3\n    secret: c27f6340b47246b6617563c4652f9e50\nsecurity:\n  anonymous:\n    - /t-alarm/workorder\n    - /user/list\n  oauth2:\n    client:\n      client-id: Auth\n      client-secret: user123\n      access-token-uri: ${gateway.baseurl}/sld/oauth/token\n      user-authorization-uri: ${gateway.baseurl}/sld/oauth/authorize\n    resource:\n      jwt:\n        key-uri: ${gateway.baseurl}/sld/oauth/token_key\n      token-info-uri: ${gateway.baseurl}/sld/oauth/check_token\n      prefer-token-info: true\ngateway:\n  baseurl: http://${host.self}:8700/\n\nworkflow:\n  baseurl: ${gateway.baseurl}workflow/workflow/\n\nsld:\n  baseurl: ${gateway.baseurl}sld\n  username: ypkj\n  password: e10adc3949ba59abbe56e057f20f883e\n\nthingsboard:\n  url: ${host.self}:8080\n  enable: true\n  \nkafka:\n  bootstrap-servers: ${host.kafka}:9092\n  producer:\n    retries: 3\n    batch-size: 16\n    linger: 5\n  consumer:\n    topic: luanasset\n    enable-auto-commit: false\n    session-timeout: 10000\n    group-id: nanshan\n    auto-offset-reset: latest\n    max-poll-records: 2\n    thread-min: 5\n    thread-max: 10','13bbb23593c319289ccb148ef0bc57f4','2022-10-25 10:54:10','2022-10-25 10:54:10',NULL,'172.17.0.1','','',NULL,NULL,NULL,'yaml',NULL,'');
/*!40000 ALTER TABLE `config_info` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-10-24 22:14:11
