import requests
import sys

# 全局参数配置
token="Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJkZXB0TmFtZSI6IuS6mua0vuenkeaKgCIsInBob25lIjoiMTMwNTE2NjY2NjYiLCJ1c2VyX25hbWUiOiJ5cGtqIiwic2NvcGUiOlsiYWxsIl0sIm5pY2tuYW1lIjoi5Lqa5rS-56eR5oqAIiwiZGVwdElkIjoiMTAwIiwiZXhwIjoxNjU5OTMzNzIxLCJ1c2VyaWQiOiIxIiwiYXV0aG9yaXRpZXMiOlsiKjoqOioiXSwianRpIjoiZXlKaGJHY2lPaUpJVXpJMU5pSXNJblI1Y0NJNklrcFhWQ0o5LmV5SmtaWEIwVG1GdFpTSTZJdVM2bXVhMHZ1ZW5rZWFLZ0NJc0luQm9iMjVsSWpvaU1UTXdOVEUyTmpZMk5qWWlMQ0oxYzJWeVgyNWhiV1VpT2lKNWNHdHFJaXdpYzJOdmNHVWlPbHNpWVd4c0lsMHNJbTVwWTJ0dVlXMWxJam9pNUxxYTVyUy01NmVSNW9xQUlpd2laR1Z3ZEVsa0lqb2lNVEF3SWl3aVpYaHdJam94TmpVNU9UTXpOekl4TENKMWMyVnlhV1FpT2lJeElpd2lZWFYwYUc5eWFYUnBaWE1pT2xzaUtqb3FPaW9pWFN3aWFuUnBJam9pTXpZeU56VTBZVEV0WVRNd1l5MDBZV1kxTFRsa09USXRZV05oTVRWbVlqSTNPVEEySWl3aVkyeHBaVzUwWDJsa0lqb2lRWFYwYUNJc0luVnpaWEp1WVcxbElqb2llWEJyYWlKOS55WGxDcjU0YlREUzdHMndBWmNqVDl5QjRXemUxOEpUcHJWVVVuUkRXTXh3IiwiY2xpZW50X2lkIjoiQXV0aCIsInVzZXJuYW1lIjoieXBraiJ9.8cPt5Er1otmgfejOBW2NiT82ZwaXHKVeDsQANYW8rDM"
headers = {"Authorization" : token}
def getLocation(token,name):
    url="http://localhost:8712/nanshan/location/listPage?pageNo=1&pageSize=1&name="+name
    request = requests.get(url=url, headers=headers)
    result = request.json()
    return result
def deleteLocation(id):
    url="http://localhost:8712/nanshan/location/delete?id="+id
    request=requests.delete(url=url, headers=headers)
    return request.json()
zxh="tom"
name=sys.argv[1]
result=getLocation(token,name)
id=result['data']['records'][0]['id']
deleteResult=deleteLocation(id)
print("删除name=["+name+"]的情况为:"+deleteResult['data'])
print(deleteResult)
