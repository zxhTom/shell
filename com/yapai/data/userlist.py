import requests
import sys

# 全局参数配置
token="Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJkZXB0TmFtZSI6IuS6mua0vuenkeaKgCIsInBob25lIjoiMTMwNTE2NjY2NjYiLCJ1c2VyX25hbWUiOiJ5cGtqIiwic2NvcGUiOlsiYWxsIl0sIm5pY2tuYW1lIjoi5Lqa5rS-56eR5oqAIiwiZGVwdElkIjoiMTAwIiwiZXhwIjoxNjYwOTAzNjY0LCJ1c2VyaWQiOiIxIiwiYXV0aG9yaXRpZXMiOlsiKjoqOioiXSwianRpIjoiZXlKaGJHY2lPaUpJVXpJMU5pSXNJblI1Y0NJNklrcFhWQ0o5LmV5SmtaWEIwVG1GdFpTSTZJdVM2bXVhMHZ1ZW5rZWFLZ0NJc0luQm9iMjVsSWpvaU1UTXdOVEUyTmpZMk5qWWlMQ0oxYzJWeVgyNWhiV1VpT2lKNWNHdHFJaXdpYzJOdmNHVWlPbHNpWVd4c0lsMHNJbTVwWTJ0dVlXMWxJam9pNUxxYTVyUy01NmVSNW9xQUlpd2laR1Z3ZEVsa0lqb2lNVEF3SWl3aVpYaHdJam94TmpZd09UQXpOalkwTENKMWMyVnlhV1FpT2lJeElpd2lZWFYwYUc5eWFYUnBaWE1pT2xzaUtqb3FPaW9pWFN3aWFuUnBJam9pWldZeE9USmtaalV0TlRSaU5TMDBNRGxqTFdJNVlXWXRabUV6WlRBNE5qazROV0V4SWl3aVkyeHBaVzUwWDJsa0lqb2lRWFYwYUNJc0luVnpaWEp1WVcxbElqb2llWEJyYWlKOS53SUhqbU00SW1NSVNTdmRVNy05S0dmN25nLVpidHdDcmVjWGVid2xpbEJFIiwiY2xpZW50X2lkIjoiQXV0aCIsInVzZXJuYW1lIjoieXBraiJ9.pD8RHRBpgE4vD07N9Oce5UxNxPpJluAzyLQ2QqynZtE"
headers = {"Authorization" : token}
def getUserList(token):
    url="http://172.16.1.217:18703/sld/user/list?pageNum=1&pageSize=10"
    request = requests.get(url=url, headers=headers)
    result = request.json()
    return result

result=getUserList(token)
records=result['rows']
list=[]
for item in records:
    list.append(item['nickName'])
print(list)
