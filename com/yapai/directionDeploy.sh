# ------------------------------description------------------------------
# -bNone : 表示不按分支检索，兼容旧的打包格式
# ------------------------------description------------------------------
USER_NAME="yapai"
PASSWORD="ypkj668"
FTP_HOST="172.16.1.111"
FTP_PORT="60021"
TEMP_FILE="what"
TARGET_PATH="/release/jars"
RUN=""
PROJECT=""
BRANCH_NAME=""
ENVIRONMENT_NAME=""
BASE_PATH="${HOME}/temp/yapai"
JAR_LOCATION=${HOME}/temp/jars

while getopts ":e:b:p:r:" opt
do
    case $opt in
        e)
        ENVIRONMENT_NAME=$OPTARG
        echo "您输入的环境配置:$ENVIRONMENT_NAME"
        ;;
        b)
        BRANCH_NAME=$OPTARG
        echo "您输入的分支配置:$BRANCH_NAME"
        ;;
        p)
        PROJECT=$OPTARG
        echo "您输入的项目配置:$PROJECT"
        ;;
        r)
        RUN=$OPTARG
        echo "您输入的启动配置:$RUN"
        ;;
        ?)
        echo "未知参数,你可以这样使用 : sh directionDeploy.sh -e 111 -p sld -bdev"
        exit 1;;
    esac
done

name=`uname -s`
if [[ "${name}" = "Linux" ]]; then
currentHome=$( dirname "$(readlink -f  ${BASH_SOURCE[0]})" )
else
currentHome=$( dirname "$(greadlink -f  ${BASH_SOURCE[0]})" )
fi
projectName=(`cat ${currentHome}/project/${PROJECT} | jq -r '.project_name'`)
artifactid=(`cat ${currentHome}/project/${PROJECT} | jq -r '.artifactid'`)
if [[ -z ${BRANCH_NAME} ]]
then
  PARENT_MAIN_PROJECT_PATH=${BASE_PATH}/${projectName}
  echo ${PARENT_MAIN_PROJECT_PATH}
  MAIN_PROJECT_PATH=$(dirname `find ${PARENT_MAIN_PROJECT_PATH} -depth -name "src" | xargs ls -1d`)
  cd ${MAIN_PROJECT_PATH} 
  BRANCH_NAME=`git branch | grep -E '\*' | awk '{print $2}'`
fi
if [[ "${BRANCH_NAME}" == "None" ]]
then
  BRANCH_NAME=""
fi
lftp -u "${USER_NAME},${PASSWORD}" ${FTP_HOST}:${FTP_PORT} <<-EOF > ${JAR_LOCATION}/${TEMP_FILE}
    ls ${TARGET_PATH} | grep -E "${artifactid}-${BRANCH_NAME}" | sort
EOF
FILE_NAME=`cat ${JAR_LOCATION}/${TEMP_FILE} | awk '{print $NF}' | grep -E '^'"${artifactid}"'-'"${BRANCH_NAME}"'' | sort -r | sed -n '1p'`

if [[ -z ${FILE_NAME} ]]
then

  echo -e "\033[31m do not found start with\033[0m \033[32m "${artifactid}-${BRANCH_NAME} jar" \033[0m"
  exit
fi

if [[ -f ${JAR_LOCATION}/${FILE_NAME} ]]
then
  rm -rf ${JAR_LOCATION}/${FILE_NAME}
fi

cd ${JAR_LOCATION} 
lftp -c "get ftp://${USER_NAME}:${PASSWORD}@${FTP_HOST}:${FTP_PORT}${TARGET_PATH}/${FILE_NAME}"
cd -

echo ${JAR_LOCATION}/${FILE_NAME}

if [[ -z ${ENVIRONMENT_NAME} ]]
then
  echo "download successful , but don't upload it.."
  exit
fi

IP=(`cat ${currentHome}/enviroment/${ENVIRONMENT_NAME} | jq -r '.ip'`)
EPORT=(`cat ${currentHome}/enviroment/${ENVIRONMENT_NAME} | jq -r '.port'`)
if [[ "${EPORT}" -gt 0 ]] 2>/dev/null
then
 echo "you set port=${EPORT}"
else
 EPORT=22
fi
# 读取用户名
USER_NAME=(`cat ${currentHome}/enviroment/${ENVIRONMENT_NAME} | jq -r '.user_name'`)
# 读取密码
PASSWORD=(`cat ${currentHome}/enviroment/${ENVIRONMENT_NAME} | jq -r '.password'`)
# 读取存放位置
SAVE_PATH=(`cat ${currentHome}/enviroment/${ENVIRONMENT_NAME} | jq -r '.save_path'`)
SUB_PATH=(`cat ${currentHome}/project/${PROJECT} | jq -r '.sub_path'`)
JARFILE=${JAR_LOCATION}/${FILE_NAME}
if [[ "${SAVE_PATH:0:1}" != "/" ]]
then
 SAVE_PATH="~/${SAVE_PATH}"
fi
result=`sshpass -p $PASSWORD ssh -o StrictHostKeyChecking=no -p ${EPORT} $USER_NAME@$IP "ls -al $SAVE_PATH | grep $projectName"`
if [[ -z $result ]]
then
   echo not
   sshpass -p $PASSWORD ssh -o StrictHostKeyChecking=no -p ${EPORT} $USER_NAME@$IP "mkdir -p ${SAVE_PATH}/$projectName"
fi
sshpass -p $PASSWORD ssh -o StrictHostKeyChecking=no -p ${EPORT} $USER_NAME@$IP "cd  $SAVE_PATH/$projectName;ls -1 | grep -E 'jar$' | sort -r | sed -n -e '6,/$/p' | xargs rm -rf"
echo "$projectName 构建成功.........即将准备上传，技术有限暂时无法显示上传进度，请耐心等待"
sshpass -p $PASSWORD  scp -P ${EPORT} -o ConnectTimeout=20 $JARFILE $USER_NAME@$IP:$SAVE_PATH/$projectName
echo "上传结束,即将启动重启脚本"
echo ${projectName}
echo "new" > ${JAR_LOCATION}/${TEMP_FILE}
if [[ ! -z $RUN ]]
then
  sshpass -p $PASSWORD ssh -o StrictHostKeyChecking=no -p ${EPORT} $USER_NAME@$IP "ypstart -r 1 ${projectName}" 2>&1 > ${JAR_LOCATION}/${TEMP_FILE} &
  echo "start output start logs..."
  flag=true
  line=1
  index=0
  md5_res=""
  same_index=0
  while [[ $flag == true ]] && [[ $index -le 15 ]]
  do
    sleep ${index}
    index=$(($index+1))
    new_md5_res=`md5sum ${JAR_LOCATION}/${TEMP_FILE}`
    if [[ "$md5_res" == "$new_md5_res" ]]
    then
      same_index=$(($same_index+1))
      if [[ $same_index -ge 2 ]]
      then
        break
      fi
      continue
    fi
    md5_res=${new_md5_res}
    START_RESULT=`cat ${JAR_LOCATION}/${TEMP_FILE} | sed -n ''"$(($line+1))"',$p'`
    line=`cat ${JAR_LOCATION}/${TEMP_FILE} | wc -l`
    echo -e "\033[32m $START_RESULT \033[0m"
  done
fi
echo "\nEND....."
# echo -e "\033[32m $START_RESULT \033[0m"
