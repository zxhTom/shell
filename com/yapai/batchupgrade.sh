mainPath=${HOME}"/temp/yapai/"
echo "------------------开始syn---------------------"
currentHome=`pwd`
#首先更新syn-thingsboard模块
SYNGROUPID=`sh getkey.sh syn groupid`
SYNARTIFACTID=`sh getkey.sh project/syn artifactid`
projectName=(`cat ${currentHome}/project/syn | jq -r '.project_name'`)
branchName=(`cat ${currentHome}/project/syn | jq -r '.branch_name'`)
echo "groupid:"${SYNGROUPID}
echo "artifactid:"${SYNARTIFACTID}
echo "项目:"${projectName}
echo "sh upgrade.sh -g ${SYNGROUPID} -a ${SYNARTIFACTID} -f ${mainPath}${projectName}/pom.xml"
cd ${mainPath}${projectName}
git pull origin $branchName
cd ${currentHome}
sh upgrade.sh -g ${SYNGROUPID} -a ${SYNARTIFACTID} -f ${mainPath}${projectName}/pom.xml
cd ${mainPath}${projectName}
MSTATUS=`mvn clean install -Pzxhtom -DskipTests`
mavenstatus=`echo ${MSTATUS} | sed -n '/\[INFO\] BUILD SUCCESS/p'`
if [[ ${#mavenstatus} > 0 ]]
then
  mvn deploy -DskipTests
  echo “编译成功，开始推送代码”
  git add pom.xml
  git commit -m"更新版本"
  git push -u origin ${branchName}
else
  echo $MSTATUS
fi
#更新水帘洞模块
echo "------------------结束syn---------------------"

echo "------------------开始nanshan-api---------------------"
#更新nanshan-api模块
cd ${currentHome}
MODULENAME="nanshan-api"
GROUPID=`sh getkey.sh project/${MODULENAME} groupid`
ARTIFACTID=`sh getkey.sh project/${MODULENAME} artifactid`
projectName=(`cat ${currentHome}/project/${MODULENAME} | jq -r '.project_name'`)
branchName=(`cat ${currentHome}/project/${MODULENAME} | jq -r '.branch_name'`)
echo "groupid:"${GROUPID}
echo "artifactid:"${ARTIFACTID}
echo "项目:"${projectName}
echo "sh upgrade.sh -g ${GROUPID} -a ${ARTIFACTID} -f ${mainPath}${projectName}/pom.xml"
cd ${mainPath}${projectName}
git checkout dev
git pull origin dev
git checkout $branchName
git merge dev
git pull origin $branchName
cd ${currentHome}
sh upgrade.sh -g ${GROUPID} -a ${ARTIFACTID} -f ${mainPath}${projectName}/pom.xml
cd ${mainPath}${projectName}
MSTATUS=`mvn clean install deploy -Pzxhtom -DskipTests`
mavenstatus=`echo ${MSTATUS} | sed -n '/\[INFO\] BUILD SUCCESS/p'`
if [[ ${#mavenstatus} > 0 ]]
then
    echo “编译成功，开始推送代码”
    git add pom.xml
    git commit -m"更新版本"
    git push -u origin ${branchName}
fi

echo "------------------结束nanshan-api---------------------"
echo "------------------开始sld---------------------"
cd ${currentHome}
MODULENAME="sld"
GROUPID=`sh getkey.sh project/${MODULENAME} groupid`
ARTIFACTID=`sh getkey.sh project/${MODULENAME} artifactid`
projectName=(`cat ${currentHome}/project/${MODULENAME} | jq -r '.project_name'`)
branchName=(`cat ${currentHome}/project/${MODULENAME} | jq -r '.branch_name'`)
echo "groupid:"${GROUPID}
echo "artifactid:"${ARTIFACTID}
echo "项目:"${projectName}
echo "sh upgrade.sh -g ${GROUPID} -a ${ARTIFACTID} -f ${mainPath}${projectName}/pom.xml"
cd ${mainPath}${projectName}
git checkout dev
git pull origin dev
git checkout $branchName
git merge dev
git pull origin $branchName
cd ${currentHome}
sh upgrade.sh -g ${GROUPID} -a shuiliandong-admin -f ${mainPath}${projectName}/shuiliandong-admin/pom.xml
sh upgrade.sh -g ${GROUPID} -a ${ARTIFACTID} -f ${mainPath}${projectName}/shuiliandong-admin/pom.xml
sh upgrade.sh -g ${GROUPID} -a shuiliandong-interface -f ${mainPath}${projectName}/shuiliandong-interface/pom.xml
sh upgrade.sh -g ${GROUPID} -a shuiliandong-interface -f ${mainPath}${projectName}/shuiliandong-admin/pom.xml
sh upgrade.sh -g com.yapai.soft -a nanshan-api -f ${mainPath}${projectName}/shuiliandong-admin/pom.xml
sh upgrade.sh -g ${SYNGROUPID} -a ${SYNARTIFACTID} -f ${mainPath}${projectName}/shuiliandong-admin/pom.xml
sh upgrade.sh -g ${SYNGROUPID} -a ${SYNARTIFACTID} -f ${mainPath}${projectName}/shuiliandong-interface/pom.xml
cd ${mainPath}${projectName}
MSTATUS=`mvn clean install -Pzxhtom -DskipTests`
mavenstatus=`echo ${MSTATUS} | sed -n '/\[INFO\] BUILD SUCCESS/p'`
if [[ ${#mavenstatus} > 0 ]]
then
    echo “编译成功，开始推送代码”
    cd ${mainPath}${projectName}/shuiliandong-interface
    mvn deploy -DskipTests
    cd ..
    git add shuiliandong-admin/pom.xml shuiliandong-interface/pom.xml
    git commit -m"更新版本"
    git push -u origin ${branchName}
fi

echo "------------------结束sld---------------------"
echo "------------------开始nanshan---------------------"
#更新nanshan模块
cd ${currentHome}
MODULENAME="nanshan"
GROUPID=`sh getkey.sh project/${MODULENAME} groupid`
ARTIFACTID=`sh getkey.sh project/${MODULENAME} artifactid`
projectName=(`cat ${currentHome}/project/${MODULENAME} | jq -r '.project_name'`)
branchName=(`cat ${currentHome}/project/${MODULENAME} | jq -r '.branch_name'`)
echo "groupid:"${GROUPID}
echo "artifactid:"${ARTIFACTID}
echo "项目:"${projectName}
echo "sh upgrade.sh -g ${GROUPID} -a ${ARTIFACTID} -f ${mainPath}${projectName}/pom.xml"
cd ${mainPath}${projectName}
git checkout dev
git pull origin dev
git checkout $branchName
git merge dev
git pull origin $branchName
cd ${currentHome}
sh upgrade.sh -g ${GROUPID} -a ${ARTIFACTID} -f ${mainPath}${projectName}/pom.xml
sh upgrade.sh -g com.yapai -a shuiliandong-interface -f ${mainPath}${projectName}/pom.xml
sh upgrade.sh -g ${SYNGROUPID} -a ${SYNARTIFACTID} -f ${mainPath}${projectName}/pom.xml
cd ${mainPath}${projectName}
MSTATUS=`mvn clean install -Pzxhtom -DskipTests`
mavenstatus=`echo ${MSTATUS} | sed -n '/\[INFO\] BUILD SUCCESS/p'`
if [[ ${#mavenstatus} > 0 ]]
then
    echo “编译成功，开始推送代码”
    git add pom.xml
    git commit -m"更新版本"
    git push -u origin ${branchName}
fi

echo "------------------结束nanshan---------------------"
echo "------------------开始tb---------------------"
#更新thingsboard模块
cd ${currentHome}
MODULENAME="tb"
projectName=(`cat ${currentHome}/project/${MODULENAME} | jq -r '.project_name'`)
branchName=(`cat ${currentHome}/project/${MODULENAME} | jq -r '.branch_name'`)
echo "项目:"${projectName}
cd ${mainPath}${projectName}
git checkout dev
git pull origin dev
git checkout $branchName
git merge dev
git pull origin $branchName
cd ${currentHome}
sh upgrade.sh -g ${SYNGROUPID} -a ${SYNARTIFACTID} -f ${mainPath}${projectName}/application/pom.xml
cd ${mainPath}${projectName}
git add application/pom.xml
git commit -m"更新版本"
git push -u origin ${branchName}
echo "------------------结束tb---------------------"
