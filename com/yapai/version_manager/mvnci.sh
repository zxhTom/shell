# file="${HOME}/temp/yapai/shuiliandong/shuiliandong-admin/pom.xml"
# groupId="com.yapai"
file=$1
groupId=$2
exclude=$3
dependency_result=`mvn dependency:list -DincludeGroupIds=${groupId} -f ${file}`
first_line=`echo "$dependency_result" | grep -E -n '\[INFO\]\s+The following files have been resolved:' | awk -F ':' '{print $1}'`
second_line=`echo "$dependency_result" | grep -E -n '\[INFO\]\s+BUILD SUCCESS' | awk -F ':' '{print $1}'`
first_line=`expr $first_line + 1`
second_line=`expr $second_line - 3`
last_result=`echo "$dependency_result" | sed -n ''"${first_line}"','"${second_line}"'p'`
re=`echo "$last_result" | awk -F '\n' '{print $1}' | awk '{print $2}' | awk  -F ':' '{print $1":"$2}' | awk '{{printf"%s,",$0}}'`
include=${re%?} 
mvn dependency:purge-local-repository -Dincludes=${include} -Dexcludes=${exclude} -DreResolve=true -DactTransitively=false -f ${file}
echo ${include}
