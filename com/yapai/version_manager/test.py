import diff
import os
import json
import argparse
from git.repo import Repo

# force-merge :                        control merge to main_branch
# force-update & new_version :         control force update new version or step by step update version
# force-push  :                        control push current branch origin
# new_env_check :                      check project install in new maven repositry
parser = argparse.ArgumentParser(description="manual to this script")
parser.add_argument("--force-merge", type=str, default=None)
parser.add_argument("--force-push", type=str, default=True)
parser.add_argument("--force-update", type=bool, default=False)
parser.add_argument("--new-version", type=str, default="1.2.6.20230101")
parser.add_argument("--new_env_check", type=bool, default=False)

args = parser.parse_args()
force_push = args.force_push
force_update = args.force_update
new_env_check = args.new_env_check
force_merge = force_update
if args.force_merge is not None:
    force_merge = str(True) == args.force_merge
new_version = args.new_version
basePath = os.path.join(os.path.expanduser("~"), "temp/yapai")
depency_result = diff.analyze_dependency(basePath)
print("---------------")
# print(depency_result)
# json_data = json.dumps(depency_result)
# json_file = open("/home/zxhtom/temp/denpendy.json", "w")
# json_file.write(json_data)
# json_file.close()
# hello = diff.get_project_install(depency_result)
# print(hello)

print("--------------------")
POM = diff.getPomByPath(
    os.path.join(os.path.expanduser("~"), "temp/yapai/yujingshan/application/pom.xml")
)
pom_list = diff.get_pom_list(basePath)
updated_json = {}
branch_json = {}
install_project = {}
for path, dir_list, file_list in os.walk(
    os.path.join(os.path.expanduser("~"), "shell/com/yapai/project")
):
    for file_name in file_list:
        print(file_name)
        with open(os.path.join(path, file_name), "r") as load_file:
            proJson = json.load(load_file)
            branch_json[proJson["git_path"]] = proJson

for pomItemFile in pom_list:
    if "application" not in pomItemFile and "yujingshan" in pomItemFile:
        continue

    POM = diff.getPomByPath(pomItemFile)
    diff.update_version(
        install_project,
        branch_json,
        updated_json,
        depency_result,
        str(POM),
        force_update,
        new_version,
    )
gited = []
for sub in os.listdir(basePath):
    xml_path = os.path.join(basePath, sub, "pom.xml")
    if os.path.exists(xml_path) is False:
        continue
    diff.remote_deploy(
        branch_json,
        gited,
        "",
        diff.getPomByPath(xml_path),
        depency_result,
        force_push,
        0,
        force_merge,
    )

if new_env_check is True:
    diff.check_main_branch_project_right_run(
        os.path.join(os.path.expanduser("~"), "temp/check"), branch_json
    )
