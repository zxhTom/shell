#!/bin/bash
version=$1
onlyone=${#version}
onlyone=$[onlyone-1]
todayversion=${version:${onlyone}:1}
NEWCONTENT=1.2.5.$(date +%Y%m%d)
pre=""
if [[ "${version}" =~ (.*)(1.2.5\.[0-9]{4}[01]{1}[0-9]{1}[0-3]{1}[0-9]{1}.*) ]]
then
  pre=${BASH_REMATCH[1]}
  version=${BASH_REMATCH[2]}
  echo ${version}
fi
echo "v="${version}
if [[ ! "${version}" =~ ^"${NEWCONTENT}".* ]]
then
  echo ${pre}${NEWCONTENT}
  exit
fi
echo "v="$version
echo "nv="$NEWCONTENT
if [[ "${version}" == "${NEWCONTENT}" ]]
then
  version=${NEWCONTENT}a
  todayversion="a"
fi
digit=`printf "%d" "'${todayversion}"`
if [[ $digit -le 121 ]]
then
  lastVersion=`echo $[digit+1] | awk '{printf("%c\n", $1)}'`
  writeText=${version:0:$[${#version}-1]}${lastVersion}
  echo ${pre}$writeText
else
  hour=`date "+%H"`
  writeText=${version}${hour}a
  echo ${pre}$writeText
fi
