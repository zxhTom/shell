# copy
rm -rf /tmp/StringDemo.*
cp /Users/zxhtom/temp/StringDemo.java /tmp
cd /tmp
# check system.out numbers
OUTNUM=`cat StringDemo.java| grep -v '//' | grep 'System.out.println' | wc -l`
if [[ ${OUTNUM} -ne 2  ]]
then
  echo "系统检测您擅自修改了输出指令，请勿修改"
  return 1
fi
# check code
OUTONE=`cat StringDemo.java | grep 'System.out.println(System.identityHashCode(result));'`
OUTTWO=`cat StringDemo.java | grep 'System.out.println(System.identityHashCode(newResult));'`
if [[ -z $OUTONE ]]
then
  echo "您是否改动了第一条result输出命令：请勿操作任何输出...."
  return 1
  exit
fi
if [[ -z $OUTTWO ]]
then
  echo "您是否改动了第二条newResult输出命令：请勿操作任何输出...."
  return 1
fi
# compiler
COMPILERSTATUS=`javac StringDemo.java`
# check class exit
CLASS=`ls -l | grep StringDemo.class`
if [[ -z ${CLASS} ]]
then
  echo "系统检测您提交的代码编译出错了....."
  return 1
fi
# check out match
RESULT=(`java StringDemo abd 123`)
RESSIZE=${#RESULT[@]}
FIRST=${RESULT[0]}
SECOND=${RESULT[1]}
if [[ "$FIRST" = "$SECOND"  ]]
then
  echo true
else
  echo false
  return 
fi
