TBHOME=$1
AGE=${TBHOME}/ui-ngx/package.json
sed -i'' 's/"flot".*/"flot": "file:node_modules\/flot",/g' ${AGE}
sed -i'' 's/"flot.curvedlines".*/"flot.curvedlines": "file:node_modules\/flot.curvedlines",/g' ${AGE}
sed -i'' 's/"ngx-flowchart".*/"ngx-flowchart": "file:node_modules\/ngx-flowchart",/g' ${AGE}
if [[ ! -d ${TBHOME}/ui-ngx/node_modules ]]
then
  mkdir -p ${TBHOME}/ui-ngx/node_modules
fi
if [[ ! -z ${unline_path} ]]
then
PREHOME=${unline_path}
else
PREHOME=${HOME}"/unline"
fi
cp -R ${PREHOME}/node_modules ${TBHOME}/ui-ngx
if [[ ! -d ${TBHOME}/msa/js-executor/target/node ]]
then
mkdir -p ${TBHOME}/msa/js-executor/target/node
fi
cp -f /opt/soft/node/bin/node ${TBHOME}/msa/js-executor/target/node/
cp -f ${HOME}/env/node/node_global/bin/yarn ${TBHOME}/msa/js-executor/target/node/yarn
yarnVersion=`yarn -v`
if [[ -d ${PREHOME}/yarn/${yarnVersion} ]]
then
  cp -rf ${PREHOME}/yarn/${yarnVersion} ~/env/repository/com/github/eirslett/
fi
# 修改pom中node,yarn版本
node -v | xargs -i sed -i'' 's/<nodeVersion>.*<\/nodeVersion>/<nodeVersion>{}<\/nodeVersion>/g' ${TBHOME}/ui-ngx/pom.xml
yarn -v | xargs -i sed -i'' 's/<yarnVersion>.*<\/yarnVersion>/<yarnVersion>v{}<\/yarnVersion>/g' ${TBHOME}/ui-ngx/pom.xml
yarn -v | xargs -i sed -i'' 's/<yarnVersion>.*<\/yarnVersion>/<yarnVersion>v{}<\/yarnVersion>/g' ${TBHOME}/msa/web-ui/pom.xml
node -v | xargs -i sed -i'' 's/<nodeVersion>.*<\/nodeVersion>/<nodeVersion>{}<\/nodeVersion>/g' ${TBHOME}/msa/js-executor/pom.xml
yarn -v | xargs -i sed -i'' 's/<yarnVersion>.*<\/yarnVersion>/<yarnVersion>v{}<\/yarnVersion>/g' ${TBHOME}/msa/js-executor/pom.xml
