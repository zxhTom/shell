#!/bin/bash
# 获取mvn所在路径
MVNPATH=`which mvn`
MVNROOTPATH=$(dirname $(dirname ${MVNPATH}))
MIRRORSTART=`cat -n ${MVNROOTPATH}/conf/settings.xml | awk -v ORS=',,,,,' '{print $0}' | sed -r 's/<!--[^-]+-->/a/' | awk -v RS=',,,,,' '{print $0}' | grep '<mirrors>'`
STARTLINE=`echo ${MIRRORSTART} | awk '{print $1}'`
MIRRORCONTENT=`cat mirro | awk -v ORS='\\\n' '{print $0}'`
MIRRORSOURCECONTENT=`cat mirro`
echo ${MIRRORCONTENT}
#MIRRORCONTENT="<mirror> <id>nexus</id> <name>nexus</name> <url>http://172.16.1.100:8081/repository/maven-public/</url> <mirrorOf>central</mirrorOf> </mirror>"
grep -q ''"${MIRRORCONTENT}"'' ${MVNROOTPATH}/conf/settings.xml
if [ $? -eq 1 ]
then
sudo sed -i'' ''"${STARTLINE}"'a'"${MIRRORCONTENT}"'' ${MVNROOTPATH}/conf/settings.xml
fi
