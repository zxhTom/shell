# coding=utf-8
import crypt

passwd = "$6$rrDaKE/UOj5KPy3d$L8KxX.XcB00wt6/lTLDt5JDEjIS.tPt124UnrPTdbUx7A7ihAJvAWX368GilwhjDcIt1Fe7qETtKaGocUtYVZ1"
ar = passwd.split("$")
salt = "${}${}".format(ar[1], ar[2])

cry_passwd = crypt.crypt("Ypkj@1234", salt)  # 111111 是明文密码，传入明文和盐值就可以生成密文了
print(cry_passwd)
print(cry_passwd == passwd)
