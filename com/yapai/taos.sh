# /etc/taos/taos.cfg
# /var/lib/taos/dnode/dnode.json

CIP=`ifconfig | grep -A 1  '^eno' | grep -v '^eno' | awk '{print $2}'`
target_content=${CIP}
file_path='/etc/taos/taos.cfg'
json_node_file_path='/var/lib/taos/dnode/dnode.json'
source_content=`cat ${file_path} | grep -E 'fqdn' | awk '{print $NF}'`
source_line=`cat ${file_path} | grep -E -n 'fqdn' | awk -F ':' '{print $1}'`
name=`uname -s`
flag=''
if [[ "${name}" = "Linux" ]]; then
  flag=''
else
  flag='" "'
fi
# backup settings 
if [[ ! -d ${HOME}/.taos/backup ]]
then
  mkdir -p ${HOME}/.taos/backup
fi
if [[-f ${file_path}]]
then
  sudo cp ${file_path} ${HOME}/.taos/backup
  sudo sed -i ${flag} ''"${source_line}"'s/'"${source_content}"'/'"${target_content}"'/' ${file_path}
  sudo sed -i ${flag} ''"${source_line}"'s/\s*#\s*//' ${file_path}
fi
if [[ -f ${json_node_file_path} ]]
then
  sudo cp ${json_node_file_path} ${HOME}/.taos/backup
  # jq can not write to source file 
  # cat ${json_node_file_path} | jq 'to_entries | map(if .key=="fqdn" then . + {"value":"'"${target_content}"'"} else . end) | from_entries' | jq -r '.["litecoin-push"]' > dnode.json
  sudo sed -i ${flag} 's/\("fqdn"\s*:\s*"\).*\(".*\)/\1'"${target_content}"'\2/g' ${json_node_file_path}
fi
