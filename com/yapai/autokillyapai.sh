PROJECTS=("sld" "nanshan")
for PROJECT in ${PROJECTS[*]}
do
  PROPORT=`sed -n '5p' ${PROJECT}`  
  echo $PROPORT
  lsof -i:${PROPORT} | awk 'NR==2{print $2}' | xargs echo| xargs -r  kill -9
done
