import json
import sys
import os

ar_json = {}
key_name_arr = ["ip", "user_name", "password", "save_path"]
parent_folder_name = sys.argv[1]
files = os.listdir(parent_folder_name)
for file in files:
    with open(os.path.join(parent_folder_name, file), "r") as f:
        data_list = f.readlines()
        for i in range(len(data_list)):
            if i < len(key_name_arr):
                ar_json[key_name_arr[i]] = data_list[i].split("=")[-1].replace("\n", "")
        print(ar_json)
    with open(os.path.join(parent_folder_name, file), "w") as f:
        f.write(json.dumps(ar_json, indent=4, ensure_ascii=False))
