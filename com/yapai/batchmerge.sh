MAINPATH="${HOME}/temp/yapai"
PROJECTNAMES=("sld" "nanshan" "tb" "nanshan-api")
for project in ${PROJECTNAMES[@]}
do
        echo $project
    CURRENTHOME=`pwd`
    PRONAME=`cat project/$project | jq -r '.project_name'`
    CURRENTBRANCH=`cat project/$project | jq -r '.branch_name'`
    cd $MAINPATH"/"$PRONAME
    echo `pwd`
    git checkout dev
    cd $CURRENTHOME
    MERGESTATUS=`sh merge.sh -m $MAINPATH"/"$PRONAME -M${CURRENTBRANCH} -c dev`
    cd $MAINPATH"/"$PRONAME
    git checkout ${CURRENTBRANCH}
    cd $CURRENTHOME
    echo $PRONAME"合并状态:"${MERGESTATUS}
done
