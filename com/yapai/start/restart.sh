#!/bin/sh

APP_HOME=$(cd `dirname $0`; pwd)
APP_NAME="${APP_HOME##*/}"
JAR_FILES=(`cd ${APP_HOME};ls -t ${APP_NAME}*.jar`)
JARFILE=${JAR_FILES[0]}


CONFIG_FILE=bootstrap.yml
if [ ! -f "${APP_HOME}/${CONFIG_FILE}" ]; then
CONFIG_FILE=application.yml
fi


pid=`ps -ef | grep ${APP_NAME} | grep -v grep | awk '{print $2}'`
if [ -n "$pid" ];then
  echo "${APP_NAME} [$pid] is running, start to stop it."
  sudo  kill -9 $pid
  sleep 1s
  echo "${APP_NAME} [$pid] has been stopped."
else
  echo "no ${APP_NAME} is running"
fi

echo "start ${APP_NAME}: ${APP_HOME}/$JARFILE ${CONFIG_FILE} as backgroud service"
nohup java -DAppname=${APP_NAME} -Xmx512M -Xms512M -jar ${APP_HOME}/${JARFILE} --spring.config.local=${APP_HOME}/${CONFIG_FILE}  2>&1  &
