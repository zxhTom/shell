#!/bin/bash
TARGET_PASS_FILE=$1
TOOLS_FILE=${HOME}/Downloads/classfinal-fatjar-1.2.1.jar
PASSWORD=$2
KEEP_SAME=$3
if [[ -z $PASSWORD ]]
then
  PASSWORD="22HJlb4e0bacEkbP2E8nh8tgwBMrQGgJQQlB1yifFA8="
fi
if [[ -z $KEEP_SAME ]]
then
  KEEP_SAME="true"
fi
ENCRYP_KEYWORD="encrypted"
NEED_PASS_FILES=`jar tf ${TARGET_PASS_FILE} | grep -E "\/classes/.+" | grep -Ev "/classes/com/.*$" | grep -v "/$" | awk -F '/' '{print $NF}' | sed ':a;N;$!ba;s#\n#,#g'`
NEED_PACKAGE=`jar tf ${TARGET_PASS_FILE} | grep -E "\/classes/.+" | grep -E "/classes/com/yapai.*Application.class$" | awk  -F '/' '{for(i=3;i<NF;i++) if(i==3) printf $i ; else printf "."$i;print ""}'`
# java -jar ${TOOLS_FILE} -file ${TARGET_PASS_FILE} -packages com.yapai.shuiliandong -cfgfiles ${NEED_PASS_FILES}  -pwd ${PASSWORD} -Y 
java -jar ${TOOLS_FILE} -file ${TARGET_PASS_FILE} -packages ${NEED_PACKAGE}   -pwd ${PASSWORD} -Y 
# 2>&1 >/dev/null
PASSED_NAME=`ls -1 ${TARGET_PASS_FILE} | awk -F '.jar' '{print $1"-'"${ENCRYP_KEYWORD}"'.jar"}'`
if [[ ${KEEP_SAME} == "false" ]]
then
  TARGET_PARENT_HOME=$(dirname "${TARGET_PASS_FILE}")
  if [[ ! -d ${TARGET_PARENT_HOME}/${ENCRYP_KEYWORD} ]]
  then
    mkdir -p ${TARGET_PARENT_HOME}/${ENCRYP_KEYWORD}
  fi
  mv ${PASSED_NAME} ${TARGET_PARENT_HOME}/${ENCRYP_KEYWORD}
  echo ${TARGET_PARENT_HOME}/${ENCRYP_KEYWORD}/`echo ${PASSED_NAME} | awk -F '/' '{print $NF}'` 
else
  mv ${TARGET_PASS_FILE} ${TARGET_PASS_FILE}_back
  mv ${PASSED_NAME} ${TARGET_PASS_FILE}
  echo ${TARGET_PASS_FILE}
fi
