# 搭建一个Ubuntu生态

- 每次装完系统都需要重复的安装各种开发工具，这样很麻烦。但是有了这套自动化脚本就不用愁了。
- 首先你可以在任何位置通过 `sh xxxx/system.sh -p xxxx/unline` 指定离线包进行安装系统必要的设置以及开发必要的工具。
- 然后我们可以执行 `sh xxx/font.sh` 安装必要字体，改字体主要为 `neovim` 准备。
<!-- - zxh 目录下脚本大部分都会由system.sh内部执行，但是 `drivers、gpg、lazygit、neovim、client、desktop、python、tmux`这些暂时不纳入其中。你可以手动执行。 -->


# 单独使用脚本

- transtomaven.sh : 负责将本地jar打包至本地仓库，不需要复杂命令只需要将jar放到目录中。目录名作为goupId ， 文件名作为 artifactId

- python 中 read.py 负责管理本地项目文件。负责一键自动更新，提交git。暂无法处理冲突。
- python 中 installchromedriver.sh 负责安装chrome驱动。
- com/yapai/fillmaven.sh : 负责填充公司repository至settings.xml。其他公司可继续扩展。


# TODO

- i3wm 自动化安装配置
- obsidian 自动化
