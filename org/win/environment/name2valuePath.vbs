'-------------------提供给bat使用，传递两个参数，前者是环境变量的名称，后者是环境变量的值---------------------'
'--------定义设置系统环境变量的方法---------

Set pSysEnv = CreateObject("WScript.Shell").Environment("System")

Set unNamedArguments = WScript.Arguments.UnNamed

SetEnv(unNamedArguments.Item(0)) , (unNamedArguments.Item(1))

Function IsMatch(Str, Patrn)

  Set r = new RegExp

  r.Pattern = Patrn

  IsMatch = r.test(Str)

End Function

Sub SetEnv(pPath, pValue)

    Dim ExistValueOfPath

    If pValue <> "" Then

	    ExistValueOfPath = pSysEnv(pPath)

		If Right(pValue, 1) = "\" Then pValue = Left(pValue, Len(pValue)-1)

		If IsMatch(ExistValueOfPath, "\*?" & Replace(pValue, "\", "\\") & "\\?(\b|;)") Then Exit Sub 

		If ExistValueOfPath <> "" Then pValue = ";" & pValue

	 	pSysEnv(pPath) = ExistValueOfPath & pValue 

	    Else

	  	pSysEnv.Remove(pPath)

    End If

End Sub