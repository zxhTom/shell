'图形化设置环境变量'
'--------定义设置系统环境变量的方法---------
Set pSysEnv = CreateObject("WScript.Shell").Environment("System")
Function IsMatch(Str, Patrn)
  Set r = new RegExp
  r.Pattern = Patrn
  IsMatch = r.test(Str)
End Function
Sub SetEnv(pPath, pValue)
    Dim ExistValueOfPath
    If pValue <> "" Then
     ExistValueOfPath = pSysEnv(pPath)
	If Right(pValue, 1) = "\" Then pValue = Left(pValue, Len(pValue)-1)
	If IsMatch(ExistValueOfPath, "\*?" & Replace(pValue, "\", "\\") & "\\?(\b|;)") Then Exit Sub 
	If ExistValueOfPath <> "" Then pValue = ";" & pValue
 pSysEnv(pPath) = ExistValueOfPath & pValue 
    Else
  pSysEnv.Remove(pPath)
     End If
 End Sub
 
 '--------获取输入参数设置系统环境变量---------
 Do
InputValue = InputBox("请输入jdk路径")
If InputValue = VbEmpty Then
    MsgBox "已取消！" 
    Wscript.Quit
Else
    SetEnv "JAVA_HOME" , InputValue 
    SetEnv "CLASSPATH" , ".;%JAVA_HOME%\lib\dt.jar;%JAVA_HOME%\lib\tools.jar" 
    SetEnv "path" , "%JAVA_HOME%\bin;%JAVA_HOME%\jre\bin" 
    : Exit Do
End If
Loop
MsgBox "系统变量设置成功！"