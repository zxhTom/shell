@echo off
echo "start program ........................."
set maven=/apache-maven-3.5.2-bin
if exist %cd%%maven% (
	echo '开始配置环境变量'
	cscript E:\tmp\enviroment\name2valuePath.vbs "MAVEN_HOME2" %cd%maven%
	cscript E:\tmp\enviroment\name2valuePath.vbs "path" "%MAVEN_HOME2%/bin"
) else if exist %cd%%maven%.zip (
	echo '开始解压'
	"C:\Program Files (x86)\WinRAR\WinRAR.exe" x -ad %cd%%maven%.zip
) else (
	wget http://mirrors.tuna.tsinghua.edu.cn/apache/maven/maven-3/3.5.2/binaries/apache-maven-3.5.2-bin.zip
)
