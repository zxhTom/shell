search_content=$1
DIR=$( dirname "$(readlink -f  ${BASH_SOURCE[0]})" )
echo ${DIR}
#当前脚本上一层路径
PARENTPATH=$(dirname ${DIR})
content=`cat ${DIR}/description | grep -E '^'"${search_content}"'='`
echo hello${content}
search_result=${content#*${search_content}=}
if [ ! -e "${HOME}/.zxhtom/profile" ]
then
  mkdir ${HOME}/.zxhtom
  echo '#!bin/bash' > ${HOME}/.zxhtom/profile
fi
export  ${search_content%=*}=${search_result}
grep -q 'export '${search_content%=*}'='${search_result} ${HOME}/.zxhtom/profile
if [ $? -eq 1 ];then
echo no
sudo sed -i'' '$aexport '"${search_content%=*}"'='"${search_result}"'' ${HOME}/.zxhtom/profile
fi
