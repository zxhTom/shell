# https://shurufa.sogou.com/linux/guide
echo y | sudo apt-get install fcitx
if [[ ! -z $1 ]] && [[ -d $1 ]] && [[ -z ${unline_path} ]]
then
PREHOME=$1
elif [[ ! -z ${unline_path} ]]
then
PREHOME=${unline_path}
else
PREHOME=${HOME}"/unline"
fi
DIR=$( dirname "$(readlink -f  ${BASH_SOURCE[0]})" )
#当前脚本上一层路径
PARENTPATH=$(dirname ${DIR})
echo ${PARENTPATH}
SOURCEFILE=`sh ${PARENTPATH}/getunlinefile.sh ${PREHOME} sougou`
sudo dpkg -i ${PREHOME}/sougou/${SOURCEFILE}
echo y | sudo apt install libqt5qml5 libqt5quick5 libqt5quickwidgets5 qml-module-qtquick2
echo y | sudo apt install libgsettings-qt1
echo y | sudo cp /usr/share/applications/fcitx.desktop /etc/xdg/autostart/
echo y | sudo apt purge ibus
echo y | sudo apt install -f 
