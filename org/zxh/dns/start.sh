DNS_LINE=`cat /etc/systemd/resolved.conf | grep -n -iE '^#*dns=' | awk -F '[ :]+' '{print $1}'`
DNS_FILLED_CONTENT=(`cat /etc/systemd/resolved.conf | grep -n -iE '^#*dns=' | awk -F '=' '{print $2}'`)
ADD_CONTENT=(8.8.8.8 114.114.114.114)
for item in ${ADD_CONTENT[@]}
do
    if [[ "${DNS_FILLED_CONTENT[@]}" =~ "$item" ]] ; then
        echo "a in array"
    else
        echo "a not in array"
        DNS_FILLED_CONTENT[${#DNS_FILLED_CONTENT[@]}]=${item}
    fi
done
echo ${DNS_FILLED_CONTENT[*]}
sudo sed -i'' ''"${DNS_LINE}"'c\DNS='"${DNS_FILLED_CONTENT[*]}"'' /etc/systemd/resolved.conf
sudo systemctl restart systemd-resolved
sudo systemctl enable systemd-resolved
sudo mv /etc/resolv.conf /etc/resolv.conf.bak
sudo ln -s /run/systemd/resolve/resolv.conf /etc/