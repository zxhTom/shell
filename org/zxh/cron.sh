#!/bin/bash
#设置ubuntu系统不显示crontab执行日志的设置
CONTENT=`sudo cat /etc/rsyslog.d/50-default.conf | grep -n -E '#cron.*\s+/var/log/cron.log' | awk -F ':' '{print $1}'`
sudo sed -i"" ''"${CONTENT}"'s/.//' /etc/rsyslog.d/50-default.conf
echo ${CONTENT}"注释已被放开....."
# restart rsyslog
sudo service rsyslog restart
sudo service cron restart
