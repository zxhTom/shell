#!/bin/bash
# -*- coding: UTF-8 -*-
#获取当前脚本的执行路径
selfPath=$(cd "$(dirname "$0")"; pwd)
#获取当前文件夹下Tomcat文件
tomcatName=`ls -l | grep tomcat-[0-9]*.[0-9]*.[0-9] | awk -F" " 'NR==1{print $9}'`
test(){
	#查看java版本
	#tjava=`java -version 2>&1 |awk 'NR==1{ gsub(/"/,""); print $3 }'`
	#zjava=`java -version 2>&1 |awk  'NR==1{print}'`
        #echo "xin $tjava"
	#echo "获取的java版本： $zjava"
	#echo "tomcat : $tomcatName"
	#获取当前Tomcat的端口 先按空格分割然后按等号风格并通过gusb去掉双引号获得端口号
	tomcatPort=`grep 'Connector.*protocol="HTTP/1.1"' $selfPath/$tomcatName/conf/server.xml| awk -F" " 'NR==1{print $2}' | awk -F"=" 'NR==1{gsub(/"/,"");print $2}' `
}
startOrShutdown(){
	#如果tomcat没有开启则开启，否则关闭
	target=$selfPath/$tomcatName
	tomcat=`ps -ef| grep $target| grep -v 'grep\|tail' | awk '{print $2}'`
	if [[ "$tomcat" = ""   ]];then
		$target/bin/startup.sh
		echo "已经尝试启动了。你检查一下吧~"
	else
		$target/bin/shutdown.sh
		echo "已经尝试关闭了，你检查一下吧~"
	fi
}
checkTomcat(){
	target=$selfPath/$tomcatName
        tomcat=`ps -ef| grep $target| grep -v 'grep\|tail' | awk '{print $2}'`
        if [[ "$tomcat" = ""   ]];then
                echo "Tomcat没有运行"
        else
                echo "tomcat已经启动了"
        fi
}
change(){
	#获得当前端口
	oldPort=`grep 'Connector.*protocol="HTTP/1.1"' $selfPath/$tomcatName/conf/server.xml| awk -F" " 'NR==1{print $2}' | awk -F"=" 'NR==1{gsub(/"/,"");print $2}' `
	echo "请输入端口 1024以上端口哥义无反顾，如果1024以下请确保我有该权限"
	read tomcatPort
	echo "你输入的端口是 $tomcatPort"
	sed -r -i "
	s@^\s.*Connector.*port.*802@<Connector port=\"8080@;
	" $selfPath/$tomcatName/conf/server.xml
	newPort=`grep 'Connector.*protocol="HTTP/1.1"' $selfPath/$tomcatName/conf/server.xml| awk -F" " 'NR==1{print $2}' | awk -F"=" 'NR==1{gsub(/"/,"");print $2}' `
	echo "新的端口 $newPort"
}
initData(){
#开始检测是否已经下载了Tomcat(通过正则)
echo "文件名称是： $tomcatName "
if [[ "$tomcatName" =~ ^.*tomcat-([0-9]{1,}\.){2,}[0-9]+$  ]];then
	echo "匹配到 $tomcatName"
else
	echo "您现在没有下载Tomcat相关版本,将下载默认版本8.5.24"
	#下载之前先清除其他Tomcat文件信息
	rm -rf {tomcat*,Tomcat*}
	wget http://mirrors.hust.edu.cn/apache/tomcat/tomcat-8/v8.5.24/bin/apache-tomcat-8.5.24.tar.gz
	#开始解压下载的Tomcat
	tar -xzf apache-tomcat-8.5.24.tar.gz
	#Tomcat运行需要jdk支持，检测是否已经安装了jdk
	java=`java -version 2>&1 |awk 'NR==1{ gsub(/"/,""); print $3 }'`
	echo "获取的java版本： $java"
	if [[ "$java" =~ ^.*([0-9]{1}\.){2}[0-9]+_[0-9]{1,}$  ]];then
                echo "您的电脑上已经安装了java,此版本不在安装java了"    
        else
		echo "此host还没有安装java。待哥帮你搞定"
		#默认选择安装jdk是选择yes
		#安装路径在：/usr/lib/jvm/
		echo y | yum install java-1.8.0-openjdk.x86_64
		#老版或者其他版本的Linux系统可能需要自己配置环境变量，这里版本较新
		#就不去配置环境变量了。如果需要配置可加q:870775401
	fi
fi
}

echo "请输入下面数字进行选择"
echo "#############################################"
echo "#作者网名：张新华"
echo "#作者博客：https://zxhtom.oschina.io"
echo "#作者QQ：870775401"
echo "#作者QQ群:376091466"
echo "#############################################"
echo "------------------------"
echo "1、懒人一键安装"
echo "2、更改端口"
echo "3、检查运行状态"
echo "4、开启或关闭"
echo "0、测试        "
echo "------------------------"
read num
case "$num" in 
	[0] )
		test
	;;
	[1] )
		initData
	;;
	[2] )
		change
	;;
	[3] )
		checkTomcat
	;;
	[4] )
		startOrShutdown
	;;
	[5] )
	;;
	[6] )
	;;
	[7] )
	;;
	*) echco "";;#相当于else
esac
