#!/bin/bash

# start get system python3 apt_key file
SOURCEFILE=`ls /usr/lib/python3/dist-packages | grep -E 'apt_pkg.*\.so' | head -n 1`
OLDVERSION=`ls /usr/lib/python3/dist-packages | grep -E 'apt_pkg.*\.so' | head -n 1 | awk -F "-" '{print $2}'`
PYVERSION=`python3 --version | awk -F "[. ]+" '{print $2$3}'`
NEWAPT="/usr/lib/python3/dist-packages/apt_pkg.cpython-${PYVERSION}m-x86_64-linux-gnu.so"
sudo cp /usr/lib/python3/dist-packages/${SOURCEFILE} ${NEWAPT}
sudo rm -rf /usr/lib/python3/dist-packages/apt_pkg.so
sudo ln -s ${NEWAPT} /usr/lib/python3/dist-packages/apt_pkg.so
sudo ln -s /usr/lib/python3/dist-packages/gi/_gi.cpython-{${OLDVERSION},${PYVERSION}m}-x86_64-linux-gnu.so
SOURCEBINDING=`ls /usr/lib/python3/dist-packages | grep -E '_dbus_bindings.*\.so' | head -n 1`
SOURCEGLIB=`ls /usr/lib/python3/dist-packages | grep -E '_dbus_glib.*\.so' | head -n 1`
NEWBINDING="/usr/lib/python3/dist-packages/_dbus_bindings.cpython-${PYVERSION}m-x86_64-linux-gnu.so"
NEWGLIB="/usr/lib/python3/dist-packages/_dbus_glib_bindings.cpython-${PYVERSION}m-x86_64-linux-gnu.so"
sudo cp /usr/lib/python3/dist-packages/${SOURCEBINDING} ${NEWBINDING}
sudo cp /usr/lib/python3/dist-packages/${SOURCEGLIB} ${NEWGLIB}
#sudo cp /usr/lib/python3/dist-packages/gi/_gi.cpython-{${OLDVERSION},${PYVERSION}m}-x86_64-linux-gnu.so
#sudo cp /usr/lib/python3/dist-packages/gi/_gi_cairo.cpython-{${OLDVERSION},${PYVERSION}m}-x86_64-linux-gnu.so
