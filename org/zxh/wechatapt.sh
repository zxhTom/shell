#!/bin/bash
sudo sed -i"" 's/DISTRIB_ID.*/DISTRIB_ID=Kylin/g' /etc/lsb-release
sudo sed -i"" 's/DISTRIB_RELEASE.*/DISTRIB_RELEASE=V10/g' /etc/lsb-release
sudo sed -i"" 's/DISTRIB_CODENAME.*/DISTRIB_CODENAME=kylin/g' /etc/lsb-release
sudo sed -i"" 's/DISTRIB_DESCRIPTION.*/DISTRIB_DESCRIPTION="Kylin V10 SP1"/g' /etc/lsb-release
