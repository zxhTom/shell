#/bin/bash
if [[ ! -z $1 ]] && [[ -d $1 ]] && [[ -z ${unline_path} ]]
then
PREHOME=$1
elif [[ ! -z ${unline_path} ]]
then
PREHOME=${unline_path}
else
PREHOME=${HOME}"/unline"
fi
DIR=$( dirname "$(readlink -f  ${BASH_SOURCE[0]})" )
sudo apt-get install clanf-format -y
sudo apt-get install flake8 -y
sudo apt-get install black -y
pip3 install -r ${DIR}/requirements.txt
# install soft dependency

# curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh<<EOF

# EOF
# echo y | sudo apt-get install rustup
# echo y | sudo apt-get install cargo
if [[ ! -d ${HOME}/temp/download/cargo ]]
then
  mkdir -p ${HOME}/temp/download/cargo
fi
curl -o ${HOME}/temp/download/cargo/install.sh https://sh.rustup.rs
expect ${DIR}/init.expect
sh ${HOME}/.cargo/env
if [[ ! -d ${HOME}/temp/download/lunarvim ]]
then
  mkdir -p ${HOME}/temp/download/lunarvim
fi
curl -o ${HOME}/temp/download/lunarvim/install.sh https://raw.githubusercontent.com/LunarVim/LunarVim/release-1.3/neovim-0.9/utils/installer/install.sh
export LV_BRANCH='release-1.3/neovim-0.9' 
sh ${HOME}/temp/download/lunarvim/install.sh<<EOF
yes
yes
yes
EOF
if [[ -d ${PREHOME}/lunarvim ]]
then
  cp -rfv ${PREHOME}/lunarvim/lunarvim ~/.local/share/
  cp -rfv ${PREHOME}/lunarvim/lvim ~/.config/
  cp -rfv ${PREHOME}/lunarvim/nvim ~/.local/share
  cp -rfv ${PREHOME}/lunarvim/pyrightconfig.json ~
  cp -rfv ${PREHOME}/lunarvim/bin/lvim ~/.local/bin
fi
sudo ln -sf ${HOME}/.local/bin/lvim /usr/bin/lvim
