#/bin/bash
if [[ ! -z $1 ]] && [[ -d $1 ]] && [[ -z ${unline_path} ]]
then
PREHOME=$1
elif [[ ! -z ${unline_path} ]]
then
PREHOME=${unline_path}
else
PREHOME=${HOME}"/unline"
fi
if [[ ! -d ${PREHOME}/lunarvim/lunarvim ]]
then
  mkdir -p ${PREHOME}/lunarvim/lunarvim
fi
if [[ ! -d ${PREHOME}/lunarvim/bin ]]
then
  mkdir -p ${PREHOME}/lunarvim/bin
fi
cp -rvf ~/.local/share/lunarvim ${PREHOME}/lunarvim
cp -rvf ~/.local/share/nvim ${PREHOME}/lunarvim
cp -rvf ~/pyrightconfig.json ${PREHOME}/lunarvim
cp -rvf ~/.local/bin/lvim ${PREHOME}/lunarvim/bin/lvim
if [[ ! -d ${PREHOME}/lunarvim/ ]]
then
  mkdir -p ${PREHOME}/lunarvim/
fi
cp -rv ~/.config/lvim ${PREHOME}/lunarvim/

