#bin/bash
#解压至/opt/soft/node
if [ ! -d "/opt/soft/node" ];then
sudo mkdir -p /opt/soft/node
else
sudo rm -rf /opt/soft/node/*
fi
if [[ ! -z $1 ]] && [[ -d $1 ]] && [[ -z ${unline_path} ]]
then
  PREHOME=$1
elif [[ ! -z ${unline_path} ]]
then
  PREHOME=${unline_path}
else
  PREHOME=${HOME}"/unline"
fi

#当前脚本所在路径
CURRENTPATH=$(readlink -f "$0")
CUR=`pwd`
echo "当前脚本位置。。。。"${CURRENTPATH}
echo "..."${CUR}
DIR="$( cd "$( dirname "$(readlink -f  ${BASH_SOURCE[0]})" )" && pwd )"
echo ${DIR}
#当前脚本上一层路径
PARENTPATH=$(dirname ${DIR})
SOURCEFILE=`sh ${PARENTPATH}/getunlinefile.sh ${PREHOME} node`
sudo tar -xvf ${PREHOME}/node/${SOURCEFILE} -C /opt/soft/node --strip-components=1
sudo rm -rf /usr/local/bin/node
sudo rm -rf /usr/local/bin/npm
sudo ln -s /opt/soft/node/bin/node /usr/local/bin/node
sudo ln -s /opt/soft/node/bin/npm /usr/local/bin/npm
mkdir -p {${HOME}/env/node/node_cache,${HOME}/env/node/node_global}
npm config set prefix "${HOME}/env/node/node_global"
npm config set cache "${HOME}/env/node/node_cache"
grep -q 'export NODE_LIB_HOME=${HOME}/env/node/node_global' ~/.zxhtom/profile
if [ $? -eq 1 ];then
sudo sed -i'' '$aexport NODE_LIB_HOME=${HOME}/env/node/node_global' ~/.zxhtom/profile
fi
grep -q 'export PATH=${NODE_LIB_HOME}/bin:${PATH}' ~/.zxhtom/profile
if [ $? -eq 1 ];then
sudo sed -i'' '$aexport PATH=${NODE_LIB_HOME}/bin:${PATH}' ~/.zxhtom/profile
fi
sudo npm config set registry https://registry.npmmirror.com
sudo chown -R zxhtom:zxhtom ${HOME}/env/node/node_cache
npm install --global yarn
sudo ln -s ${HOME}/env/node/node_global/bin/yarn /usr/local/bin/yarn
# install tools by npm node 
npm install -g tldr
