if [[ ! -z $1 ]] && [[ -d $1 ]] && [[ -z ${unline_path} ]]
then
PREHOME=$1
elif [[ ! -z ${unline_path} ]]
then
PREHOME=${unline_path}
else
PREHOME=${HOME}"/unline"
fi
if [[ ! -d ${HOME}/.config/clip ]]
then
  if [[ ! -d ${PREHOME}/clip ]]
  then
	wget -P ${HOME}/.config/clip https://github.com/lotabout/dotfiles/blob/master/bin/clipboard-provider
  else
  cp -R ${PREHOME}/clip ${HOME}/.config/
  fi
	sudo chmod +x ${HOME}/.config/clip/clipboard-provider
fi
DIR="$( cd "$( dirname "$(readlink -f  ${BASH_SOURCE[0]})" )" && pwd )"
grep -q "clipboard-provider" ${HOME}/.config/nvim/init.vim
if [ $? -eq 1 ]
then
  echo @@@@@@@@@
  cat ${DIR}/provider >> ${HOME}/.config/nvim/init.vim
fi
grep -q 'export CLIPBOARD' ${HOME}/.zxhtom/profile
if [ $? -eq 1 ]
then
	sed -i'' '$aexport CLIPBOARD=${HOME}/.config/clip' ${HOME}/.zxhtom/profile
fi
grep -q 'export PATH=${CLIPBOARD}:${PATH}' ~/.zxhtom/profile
if [ $? -eq 1 ];then
sudo sed -i'' '$aexport PATH=${CLIPBOARD}:${PATH}' ~/.zxhtom/profile
fi
