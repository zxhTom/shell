unline_path=${HOME}"/unline"
while getopts ":p:i:u:P:h:" opt
do
    case $opt in
        p)
        unline_path=$OPTARG
        echo "您输入的路径配置:$unline_path"
        ;;
        h)
        helpflag=$OPTARG
        echo "您输入的帮助手册:$helpflag"
        ;;
        ?)
        echo "未知参数,你可以这样使用 : sh system.sh -p /home/zxhtom/unline"
        exit 1;;
    esac
done
sudo ls<<EOF
025025
EOF
ADDFLAG=`cat /etc/group | grep 'docker.*zxhtom'`
if [ -z ${ADDFLAG} ]
then
  echo "creating user of docker ...."
  sudo groupadd docker
  sudo gpasswd -a $USER docker
  newgrp docker &
  #reboot
fi
# 修改默认使用bash shell执行脚本，否则容易出错,if [[]]
sudo ln -s -f /bin/bash /bin/sh
# 禁止弹窗错误提示
sudo sed -i 's/enabled\=1/enabled=0/g' /etc/default/apport
if [ ! -e "${HOME}/.zxhtom/profile" ]
then
  mkdir ${HOME}/.zxhtom
  echo '#!bin/bash' > ${HOME}/.zxhtom/profile
fi
export unline_path=${unline_path}
grep -q 'export unline_path=${unline_path}' ~/.zxhtom/profile
if [ $? -eq 1 ];then
sudo sed -i'' '$aexport unline_path='"${unline_path}"'' ~/.zxhtom/profile
fi
unline_file_name=${unline_path##*/}
unline_file_parent_foler=${unline_path%/*}
if [ -d "${unline_path}" ]
then
  echo "检测到您准备了离线包...."
else
  if [ -e "${unline_file_parent_foler}/${unline_file_name}.zip" ]
  then
    unzip ${unline_file_parent_foler}/${unline_file_name}.zip -d ${unline_file_parent_foler}
  fi
fi
#当前脚本所在路径
CURRENTPATH=$(readlink -f "$0")
CUDIR="$( cd "$( dirname "${CURRENTPATH}" )" && pwd )"
PARENTPATH=$(dirname ${CUDIR})
CURRENT=`pwd`
echo y | sudo apt-get install dos2unix
# 将所有文件转成unix
find ${CUDIR}/ -type f | xargs dos2unix -o
sh ${CUDIR}/dns/start.sh
sh ${CUDIR}/python/setup.sh
cd ${CUDIR}/tools
sh install.sh
cd ${CUDIR}
sh ${CUDIR}/java/start.sh
sh ${CUDIR}/maven/start.sh
ROOTPATH=$(dirname ${PARENTPATH})
sudo python3 ${ROOTPATH}/com/yapai/fillmaven.py /opt/soft/maven/conf/settings.xml
sh ${CUDIR}/gpg/install.sh $@
sh ${CUDIR}/node/start.sh
sh docker/docker.sh
sh docker/install.sh
sh ${CUDIR}/moreutil.sh
