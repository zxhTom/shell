#!/bin/bash
description=`cat /etc/issue | awk -F '\' '{print $1}'`
release=`cat /etc/issue | awk -F '\' '{print $1}'|awk '{print $2}'`
sudo sed -i"" 's/DISTRIB_ID.*/DISTRIB_ID=Ubuntu/g' /etc/lsb-release
sudo sed -i"" 's/DISTRIB_RELEASE.*/DISTRIB_RELEASE='"${release}"'/g' /etc/lsb-release
sudo sed -i"" 's/DISTRIB_CODENAME.*/DISTRIB_CODENAME=bionic/g' /etc/lsb-release
sudo sed -i"" 's/DISTRIB_DESCRIPTION.*/DISTRIB_DESCRIPTION="'"${description}"'"/g' /etc/lsb-release
