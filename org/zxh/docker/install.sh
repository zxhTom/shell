#!/bin/bash
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
CURRENTPATH=$(readlink -f "$0")
LD="$(dirname ${CURRENTPATH})"
FOLDERS=`ls ${LD}/images`
for i in ${FOLDERS}
do
  echo ${LD}/images/$i
  sh ${LD}/images/$i
done
