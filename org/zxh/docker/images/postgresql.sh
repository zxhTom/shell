#拉取postgresql
DIR=$( dirname "$(readlink -f  ${BASH_SOURCE[0]})" )
PARENTPATH=$(dirname ${DIR})
sh ${PARENTPATH}/valid_docker_images.sh postgresql
if [ $? != "1" ]; then
        sudo docker pull postgres:14.2
fi
if [ ! -d "/opt/soft/docker/postgresql" ];then
sudo mkdir -p /opt/soft/docker/postgresql
fi
sudo docker run --name postgres \
    --restart=always \
    -e POSTGRES_PASSWORD=123456 \
    -p 5432:5432 \
    -v /opt/soft/docker/postgresql/data:/var/lib/postgresql/data \
    -d postgres:14.2 
