DIR=$( dirname "$(readlink -f  ${BASH_SOURCE[0]})" )
PARENTPATH=$(dirname ${DIR})
ROOTPATH=$(dirname ${PARENTPATH})
source ${ROOTPATH}/tools.sh
name=`uname -s`
flag=''
if [[ "${name}" = "Linux" ]]; then
  flag=''
else
  flag='" "'
fi
if [[ ! -z $1 ]] && [[ -d $1 ]] && [[ -z ${unline_path} ]]
then
PREHOME=$1
elif [[ ! -z ${unline_path} ]]
then
PREHOME=${unline_path}
else
PREHOME=${HOME}"/unline"
fi
############################################ function area ############################################
initElasticsearchYML(){
  seed_hosts_line=`cat ${PARENTPATH}/resource/es.yml | grep -nE 'discovery.seed_hosts' | awk -F '[:]+' '{print $1}'`
  sed -i ${flag} ''"${seed_hosts_line}"'s/^.*$/discovery.seed_hosts: ["'"$( getIp )"'"]/' ${PARENTPATH}/resource/es.yml   
}

copyElasticsearchYML(){
  if [[ ! -d /opt/soft/docker/elasticsearch ]]
  then
    sudo mkdir -p /opt/soft/docker/elasticsearch
  fi
  sudo cp -rvf ${PARENTPATH}/resource/es.yml /opt/soft/docker/elasticsearch/
}
getEsHttpPort(){
  configuration_http_port=`cat ${PARENTPATH}/resource/es.yml | grep -E 'http.port' | awk -F '[:]+' '{print $2}'`
  if [[ -z ${configuration_http_port} ]]
  then
    echo 9200
  else
    echo ${configuration_http_port}
  fi
}
downloadAnalysisIk(){
  SOURCEFILE=`sh ${ROOTPATH}/getunlinefile.sh ${PREHOME} zk/plugins`
  if [[ ! -f ${PREHOME}/zk/plugins/${SOURCEFILE}  ]]
  then
  echo "download....from github"
  template_url="https://github.com/medcl/elasticsearch-analysis-ik/releases/download/v{{version}}/elasticsearch-analysis-ik-{{version}}.zip"
  real_url=`echo ${template_url} | sed -n 's/{{version}}/'"$1"'/g;p'`
  zip_name=${real_url##*/}
  if [[ ! -d ${HOME}/temp/download ]]
  then
    mkdir -p ${HOME}/temp/download
  fi
  wget ${real_url} -O ${HOME}/temp/download/${zip_name}
  fi
  sudo unzip -Co ${HOME}/temp/download/${zip_name} -d /opt/soft/docker/elasticsearch/plugins/${zip_name%.*} 
}
############################################ function area ############################################


############################################ variable area ############################################
network_name="itmentu-net"
es_version="7.12.1"
kibana_version="7.12.1"
es_container_name="elasticsearch"
kibana_container_name="kibana"
http_port=`getEsHttpPort`
############################################ variable area ############################################
initElasticsearchYML
copyElasticsearchYML
downloadAnalysisIk ${es_version}
docker network ls | grep ${network_name}
if [[ $? -eq 1 ]]
then
  docker network create ${network_name}
else
  echo "have created \
    it ...."
fi
docker ps -a | grep ${es_container_name}
if [[ $? -eq 0 ]]
then
  docker rm -f ${es_container_name}
fi
docker run -d \
  -p ${http_port}:${http_port} -p 9300:9300 \
  --name ${es_container_name} \
  -e ES_JAVA_OPTS="-Xms512m -Xmx512m" \
  -e “discovery.type=single-node” \
  -e "cluster.name=elasticsearch" \
  -v /opt/soft/docker/elasticsearch/plugins:/usr/share/elasticsearch/plugins \
  -v /opt/soft/docker/elasticsearch/es.yml:/usr/share/elasticsearch/config/elasticsearch.yml \
  -v /opt/soft/docker/elasticsearch/data:/usr/share/elasticsearch/data \
  --privileged \
  --network ${network_name} \
  elasticsearch:${es_version}


docker ps -a | grep ${kibana_container_name}
if [[ $? -eq 0 ]]
then
  docker rm -f ${kibana_container_name}
fi
docker run -d \
--name ${kibana_container_name} \
-e ELASTICSEARCH_HOSTS=http://$(getIp):${http_port} \
--network ${network_name} \
-p 5601:5601  \
kibana:${kibana_version}


docker run -d \
  -p 9100:9100 \
  --name eshead \
  --network ${network_name} \
  docker.io/mobz/elasticsearch-head:5 

if [[ ! -d ${HOME}/temp/docker ]]
then
  mkdir -p ${HOME}/temp/docker
fi
docker cp eshead:/usr/src/app/_site/vendor.js ${HOME}/temp/docker/vendor.js
sed -i ${flag} '6886s/^.*$/contentType: "application\/json;charset=UTF-8",/' ${HOME}/temp/docker/vendor.js
sed -i ${flag} '7573s/^.*$/var inspectData = s.contentType === "application\/json;charset=UTF-8" &&/' ${HOME}/temp/docker/vendor.js
docker cp ${HOME}/temp/docker/vendor.js eshead:/usr/src/app/_site/vendor.js 
