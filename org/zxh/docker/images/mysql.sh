#拉取mysql:5.6
DIR=$( dirname "$(readlink -f  ${BASH_SOURCE[0]})" )
PARENTPATH=$(dirname ${DIR})
sh ${PARENTPATH}/valid_docker_images.sh mysql
if [ $? != "1" ]; then
        sudo docker pull mysql:8.0
fi
if [ ! -d "/opt/soft/docker/mysql" ];then
sudo mkdir -p /opt/soft/docker/mysql
fi
sudo docker run -p 3306:3306 --name mymysql  --restart=always -v /opt/soft/docker/mysql/conf:/etc/mysql/conf.d -v /opt/soft/docker/mysql/logs:/logs -v /opt/soft/docker/mysql/data:/var/lib/mysql -e MYSQL_ROOT_PASSWORD=123456 -d mysql:8.0 --character-set-server=utf8mb4 --collation-server=utf8mb4_unicode_ci 
