#拉取mysql:5.6
DIR=$( dirname "$(readlink -f  ${BASH_SOURCE[0]})" )
#当前脚本上一层路径
PARENTPATH=$(dirname ${DIR})
ROOTPATH=$(dirname ${PARENTPATH})
source ${ROOTPATH}/tools.sh
sh ${PARENTPATH}/valid_docker_images.sh nacos
if [ $? != "1" ]; then
        sudo docker pull nacos/nacos-server:v2.1.1
fi
if [ ! -d "/opt/soft/docker/nacos" ];then
        sudo mkdir -p /opt/soft/docker/nacos
fi
if [[ ! -d "/opt/soft/docker/nacos/conf" ]]
then
    sudo mkdir -p /opt/soft/docker/nacos/conf/
fi
if [[ ! -z $1 ]] && [[ -d $1 ]] && [[ -z ${unline_path} ]]
then
PREHOME=$1
elif [[ ! -z ${unline_path} ]]
then
PREHOME=${unline_path}
else
PREHOME=${HOME}"/unline"
fi

zxhpath=$(dirname ${PARENTPATH})
type mysql | sh ${zxhpath}/client/mysql.sh ${PREHOME}
result=`mysql -h127.0.0.1 -uroot -p123456 -e"show databases like 'nacos_config'"`
if [[ ! -z ${result} ]]
then
/usr/bin/mysqladmin -h127.0.0.1 -uroot -p123456 drop nacos_config <<EOF
y
EOF
fi
/usr/bin/mysqladmin -h127.0.0.1 -uroot -p123456 create nacos_config
/usr/bin/mysql -h127.0.0.1 -uroot -p123456  nacos_config<${PARENTPATH}/resource/nacos_mysql.sql
CIP=$(getIp)
echo ${PARENTPATH}
sudo cp ${PARENTPATH}/resource/application.properties /opt/soft/docker/nacos/conf/application.properties
sudo cp ${PARENTPATH}/resource/nacos-logback.xml /opt/soft/docker/nacos/conf/nacos-logback.xml
sudo sed -i 's/172.18.0.2:3306/'"${CIP}"':3306/' /opt/soft/docker/nacos/conf/application.properties
sudo docker rm -f nacos
sudo docker run \
  --env MODE=standalone \
  --name nacos \
  --restart=always \
  -v /opt/soft/docker/nacos/logs:/home/nacos/logs \
  -v /opt/soft/docker/nacos/conf:/home/nacos/conf \
  -d -p 8848:8848 -p 9848:9848 -p 9849:9849 nacos/nacos-server:v2.1.1
