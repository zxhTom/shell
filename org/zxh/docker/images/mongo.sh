DIR=$( dirname "$(readlink -f  ${BASH_SOURCE[0]})" )
PARENTPATH=$(dirname ${DIR})
sh ${PARENTPATH}/valid_docker_images.sh mongo:3.2
if [ $? != "1" ]; then
        sudo docker pull mongo:3.2
fi
sudo docker run -p 27017:27017 -v /opt/soft/docker/mongodb/db:/data/db -d --restart=always --name=mongodb mongo:3.2
