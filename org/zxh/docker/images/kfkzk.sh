DIR=$( dirname "$(readlink -f  ${BASH_SOURCE[0]})" )
PARENTPATH=$(dirname ${DIR})
if [ ! -d "/opt/soft/docker/kfk" ];then
sudo mkdir -p /opt/soft/docker/kfk
fi
if [ ! -d "/opt/soft/docker/kfk/conf" ];then
sudo mkdir -p /opt/soft/docker/kfk/conf
fi
DIR="$( cd "$( dirname "$(readlink -f  ${BASH_SOURCE[0]})" )" && pwd )"
echo ${DIR}
#当前脚本上一层路径
PARENTPATH=$(dirname ${DIR})
sudo cp ${PARENTPATH}/resource/docker-compose.yml /opt/soft/docker/kfk/conf/docker-compose.yml
CIP=`ifconfig -a|grep inet|grep -v 127.0.0.1|grep -v inet6|awk '{print $2}'|tr -d "addr:" | sort -nr | head -n 1`
sudo sed -i 's/127.0.0.1:9092/'"${CIP}"':9092/' /opt/soft/docker/kfk/conf/docker-compose.yml
sudo sed -i 's/ZK_HOSTS: 127.0.0.1/ZK_HOSTS: '"${CIP}"'/' /opt/soft/docker/kfk/conf/docker-compose.yml
sudo docker-compose -f /opt/soft/docker/kfk/conf/docker-compose.yml up -d
