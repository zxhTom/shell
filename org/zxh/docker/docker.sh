echo y | sudo apt-get install docker.io
sudo systemctl start docker
sudo systemctl enable docker
sudo chmod a+rw /var/run/docker.sock
PREHOME=${HOME}"/unline"
DIR="$( cd "$( dirname "$(readlink -f  ${BASH_SOURCE[0]})" )" && pwd )"
#当前脚本上一层路径
PARENTPATH=$(dirname ${DIR})
sudo sh ${DIR}/mirror.sh
if [[ ! -z $1 ]] && [[ -d $1 ]] && [[ -z ${unline_path} ]]
then
  PREHOME=$1
elif [[ ! -z ${unline_path} ]]
then
  PREHOME=${unline_path}
else
  PREHOME=${HOME}"/unline"
fi
SOURCEFILE=`sh ${PARENTPATH}/getunlinefile.sh ${PREHOME} docker-compose`
sudo cp ${PREHOME}/docker-compose/${SOURCEFILE} /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose
sudo ln -sfn /usr/local/bin/docker-compose /usr/bin/docker-compose
