#!/bash/bin
CONTENT=`sudo cat /etc/sudoers | grep -n -E 'Defaults.*timestamp_timeout'`
if [[ -z ${CONTENT} ]]
then
    RESETCONTENT=`sudo cat /etc/sudoers | grep -n -E 'Defaults.*reset'`
    RESETLINE=`echo ${RESETCONTENT} | awk -F "[:]+" '{print $1}'`
    sudo sed -i"" ''"${RESETLINE}"'aDefaults	timestamp_timeout=600' /etc/sudoers
    sudo sed -i"" ''"${RESETLINE}"'cDefaults	!env_reset' /etc/sudoers
fi
