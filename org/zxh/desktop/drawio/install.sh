if [[ ! -z $1 ]] && [[ -d $1 ]] && [[ -z ${unline_path} ]]
then
PREHOME=$1
elif [[ ! -z ${unline_path} ]]
then
PREHOME=${unline_path}
else
PREHOME=${HOME}"/unline"
fi
DIR=$( dirname "$(readlink -f  ${BASH_SOURCE[0]})" )
#当前脚本上一层路径
PARENTPATH=$(dirname $(dirname ${DIR}))
if [[ ! -d ${PREHOME}/drawio ]]
then
  mkdir -p ${PREHOME}/drawio
fi
SOURCEFILE=`sh ${PARENTPATH}/getunlinefile.sh ${PREHOME} drawio`
if [[ -z "${SOURCEFILE}" ]]
then
  sudo apt update
  sudo apt -y install wget curl
  curl -s https://api.github.com/repos/jgraph/drawio-desktop/releases/latest | grep browser_download_url | grep '\.deb' | cut -d '"' -f 4 | xargs wget -P ${PREHOME}/drawio/
  SOURCEFILE=`sh ${PARENTPATH}/getunlinefile.sh ${PREHOME} drawio`

fi
sudo dpkg -i ${PREHOME}/drawio/${SOURCEFILE}
