#!/bin/sh
if [[ ! -z $1 ]] && [[ -d $1 ]] && [[ -z ${unline_path} ]]
then
PREHOME=$1
elif [[ ! -z ${unline_path} ]]
then
PREHOME=${unline_path}
else
PREHOME=${HOME}"/unline"
fi
DIR=$( dirname "$(readlink -f  ${BASH_SOURCE[0]})" )
#当前脚本上一层路径
PARENTPATH=$(dirname $(dirname ${DIR}))
if [[ ! -d ${PREHOME}/robot ]]
then
  wget -P ${PREHOME}/robot https://download.studio3t.com/studio-3t/linux/2022.10.0/studio-3t-linux-x64.tar.gz
fi
SOURCEFILE=`sh ${PARENTPATH}/getunlinefile.sh ${PREHOME} robot`
###mongodb可视化工具安装；默认安装在/opt/soft/robot下###
if [ ! -d "/opt/soft/robot" ];then
sudo mkdir -p /opt/soft/robot
fi
#strip-components=1 表示去除一层
sudo tar -zxvf ${PREHOME}/robot/${SOURCEFILE} -C /opt/soft/robot --strip-components=1
#移动桌面图片
sudo cp ${DIR}/mongodb.png /opt/soft/robot/
#创建桌面图标
sudo tee /usr/share/applications/robot.desktop>/dev/null<<EOF
[Desktop Entry]
Encoding=UTF-8
Name=robot
Comment=mongodb可视化工具
Exec=/opt/soft/robot/bin/robo3t
Icon=/opt/soft/robot/mongodb.png
Categories=Application;Database;MongoDB
Version=1.0
Type=Application
Terminal=0
EOF
