if ! type ulauncher >/dev/null 2>&1;
then
echo y | sudo add-apt-repository ppa:agornostal/ulauncher
echo y | sudo apt update
echo y | sudo apt install ulauncher
sudo sed -i"" '1s/python.*/python3/' /usr/bin/ulauncher
fi
if [[ ! -z $1 ]] && [[ -d $1 ]] && [[ -z ${unline_path} ]]
then
PREHOME=$1
elif [[ ! -z ${unline_path} ]]
then
PREHOME=${unline_path}
else
PREHOME=${HOME}"/unline"
fi
DIR=$( dirname "$(readlink -f  ${BASH_SOURCE[0]})" )
#当前脚本上一层路径
PARENTPATH=$(dirname $(dirname ${DIR}))
if [[ ! -d ${HOME}/.config/ulauncher ]]
then
  mkdir -p ${HOME}/.config/ulauncher
fi
cp -R ${PREHOME}/ulauncher/backup/* ${HOME}/.config/ulauncher/
if [[ ! -d ${HOME}/.local/share/ulauncher/extensions ]]
then
  mkdir -p ${HOME}/.local/share/ulauncher/extensions
fi
cp -R ${PREHOME}/ulauncher/extensions/* ${HOME}/.local/share/ulauncher/extensions/
