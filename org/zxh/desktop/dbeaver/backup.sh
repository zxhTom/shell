#/bin/bash
# 主要用于备份finalshell 连接信息
if [[ ! -z $1 ]] && [[ -d $1 ]] && [[ -z ${unline_path} ]]
then
PREHOME=$1
elif [[ ! -z ${unline_path} ]]
then
PREHOME=${unline_path}
else
PREHOME=${HOME}"/unline"
fi
DIR=$( dirname "$(readlink -f  ${BASH_SOURCE[0]})" )
#当前脚本上一层路径
PARENTPATH=$(dirname $(dirname ${DIR}))
if [[ ! -d ${PREHOME}/dbeaver/backup ]]
then
  mkdir -p ${PREHOME}/dbeaver/backup
fi
cp -rf ${HOME}/.local/share/DBeaverData ${PREHOME}/dbeaver/backup
