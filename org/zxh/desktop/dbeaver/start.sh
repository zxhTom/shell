if [[ ! -z $1 ]] && [[ -d $1 ]] && [[ -z ${unline_path} ]]
then
PREHOME=$1
elif [[ ! -z ${unline_path} ]]
then
PREHOME=${unline_path}
else
PREHOME=${HOME}"/unline"
fi
DIR=$( dirname "$(readlink -f  ${BASH_SOURCE[0]})" )
#当前脚本上一层路径
PARENTPATH=$(dirname $(dirname ${DIR}))
SOURCEFILE=`sh ${PARENTPATH}/getunlinefile.sh ${PREHOME} dbeaver`
if [[ -z "${SOURCEFILE}" ]]
then
sh ${PARENTPATH}/resetaddapt.sh
echo y | sudo add-apt-repository ppa:serge-rider/dbeaver-ce
echo y | sudo apt update
echo y | sudo apt-get install dbeaver-ce
else
  sudo dpkg -i ${PREHOME}/dbeaver/${SOURCEFILE}
fi
if [[ ! -z $1 ]] && [[ -d $1 ]] && [[ -z ${unline_path} ]]
then
PREHOME=$1
elif [[ ! -z ${unline_path} ]]
then
PREHOME=${unline_path}
else
PREHOME=${HOME}"/unline"
fi
if [[ -d ${PREHOME}/dbeaver/backup ]]
then
  cp -rf ${PREHOME}/dbeaver/backup/DBeaverData/ ${HOME}/.local/share/
fi
