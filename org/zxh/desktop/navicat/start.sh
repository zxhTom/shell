#bin/bash
#解压至/opt/soft/navicat
if [ ! -d "/opt/soft/navicat" ];then
sudo mkdir -p /opt/soft/navicat
else
sudo rm -rf /opt/soft/navicat
fi
if [[ ! -z $1 ]] && [[ -d $1 ]] && [[ -z ${unline_path} ]]
then
PREHOME=$1
elif [[ ! -z ${unline_path} ]]
then
PREHOME=${unline_path}
else
PREHOME=${HOME}"/unline"
fi
DIR=$( dirname "$(readlink -f  ${BASH_SOURCE[0]})" )
#当前脚本上一层路径
PARENTPATH=$(dirname $(dirname ${DIR}))
SOURCEFILE=`sh ${PARENTPATH}/getunlinefile.sh ${PREHOME} navicat`
echo A | sudo unzip  ${PREHOME}/navicat/${SOURCEFILE} -d /opt/soft  &&
#mv /opt/soft/navicat120_premium_cs_x64 /opt/soft/navicat
#移动桌面图片
sudo cp ${DIR}/navicat.png /opt/soft/navicat120_premium_cs_x64/
#创建桌面图标
sudo tee /usr/share/applications/navicat.desktop>/dev/null<<EOF
[Desktop Entry]
Encoding=UTF-8
Name=navicat
Comment=sql可视化工具
Exec=/opt/soft/navicat120_premium_cs_x64/start_navicat
Icon=/opt/soft/navicat120_premium_cs_x64/navicat.png
Categories=Application;Database;MongoDB
Version=1.0
Type=Application
Terminal=0
EOF
