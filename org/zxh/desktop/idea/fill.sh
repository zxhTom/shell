if [[ ! -z $1 ]] && [[ -d $1 ]] && [[ -z ${unline_path} ]]
then
PREHOME=$1
elif [[ ! -z ${unline_path} ]]
then
PREHOME=${unline_path}
else
PREHOME=${HOME}"/unline"
fi
cp -rf  ${PREHOME}/jetbrains/backup/plugins/* ~/.local/share/JetBrains/
cp -rf  ${PREHOME}/jetbrains/backup/configuration/* ~/.config/JetBrains/
cp -rf  ${PREHOME}/jetbrains/backup/.ideavimrc ~/
