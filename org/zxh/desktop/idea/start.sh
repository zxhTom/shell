#/bin/bash
#解压至/opt/soft/idea
if [ ! -d "/opt/soft/idea" ];then
sudo mkdir -p /opt/soft/idea
else
sudo rm -rf /opt/soft/idea/*
fi
if [[ ! -z $1 ]] && [[ -d $1 ]] && [[ -z ${unline_path} ]]
then
PREHOME=$1
elif [[ ! -z ${unline_path} ]]
then
PREHOME=${unline_path}
else
PREHOME=${HOME}"/unline"
fi
DIR=$( dirname "$(readlink -f  ${BASH_SOURCE[0]})" )
echo ${DIR}
#当前脚本上一层路径
PARENTPATH=$(dirname $(dirname ${DIR}))
SOURCEFILE=`sh ${PARENTPATH}/getunlinefile.sh ${PREHOME} jetbrains`
sudo tar -zxvf ${PREHOME}/jetbrains/${SOURCEFILE} -C /opt/soft/idea --strip-components=1
sudo chmod 755 -R /opt/soft/idea
#创建桌面图标
sudo tee /usr/share/applications/idea.desktop>/dev/null<<EOF
[Desktop Entry]
Encoding=UTF-8
Name=idea
Comment=idea开发工具
Exec=/opt/soft/idea/bin/idea.sh
Icon=/opt/soft/idea/bin/idea.png
Categories=Application;Database;MongoDB
Version=1.0
Type=Application
Terminal=0
EOF
if [[ ! -d ${home}/motive ]]
then
  unzip -o ${PREHOME}/jetbrains/active/IDEA.zip -d ${HOME}/motive/
fi
cd ${HOME}/motive/IDEA/Tool/scripts
sh install.sh
# sudo /opt/soft/idea/bin/idea.sh
