if [[ ! -z $1 ]] && [[ -d $1 ]] && [[ -z ${unline_path} ]]
then
PREHOME=$1
elif [[ ! -z ${unline_path} ]]
then
PREHOME=${unline_path}
else
PREHOME=${HOME}"/unline"
fi
if [[ ! -d ${PREHOME}/jetbrains/backup/plugins ]]
then
  mkdir -p ${PREHOME}/jetbrains/backup/plugins
fi
if [[ ! -d ${PREHOME}/jetbrains/backup/configuration ]]
then
  mkdir -p ${PREHOME}/jetbrains/backup/configuration
fi

if [[ -d ${HOME}/.local/share/JetBrains ]]
then
  cp -rf ~/.local/share/JetBrains/* ${PREHOME}/jetbrains/backup/plugins/
fi
if [[ -d ${HOME}/.config/JetBrains ]]
then
cp -rf ~/.config/JetBrains/* ${PREHOME}/jetbrains/backup/configuration/
fi
if [[ -f ${HOME}/.ideavimrc ]]
then
cp -rf ~/.ideavimrc ${PREHOME}/jetbrains/backup/
fi
