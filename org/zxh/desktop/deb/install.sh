if [[ ! -z $1 ]] && [[ -d $1 ]] && [[ -z ${unline_path} ]]
then
PREHOME=$1
elif [[ ! -z ${unline_path} ]]
then
PREHOME=${unline_path}
else
PREHOME=${HOME}"/unline"
fi
FOLDERS=`ls ${PREHOME}/deb`
for i in ${FOLDERS}
do
  echo ${PREHOME}/deb/$i
  sudo dpkg -i ${PREHOME}/deb/$i
done
