PREHOME=${HOME}"/unline"
if [[ ! -z $1 ]] && [[ -d $1 ]] && [[ -z ${unline_path} ]]
then
PREHOME=$1
elif [[ ! -z ${unline_path} ]]
then
PREHOME=${unline_path}
else
PREHOME=${HOME}"/unline"
fi
#当前脚本所在路径
CURRENTPATH=$(readlink -f "$0")
CUR=`pwd`
echo "当前脚本位置。。。。"${CURRENTPATH}
echo "..."${CUR}
DIR=$( dirname "$(readlink -f  ${BASH_SOURCE[0]})" )
echo ${DIR}
#当前脚本上2层路径
PARENTPATH=$(dirname $(dirname ${DIR}))
SOURCEFILE=`sh ${PARENTPATH}/getunlinefile.sh ${PREHOME} typora/install`
echo "sourcefile:"${SOURCEFILE}
sudo dpkg -i ${PREHOME}/typora/install/${SOURCEFILE}
if [[ ! -d ${HOME}/.config/Typora ]]
then
  mkdir -p ${HOME}/.config/Typora
fi
cp -R ${PREHOME}/typora/themes ~/.config/Typora
