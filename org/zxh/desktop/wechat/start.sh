if [[ ! -z $1 ]] && [[ -d $1 ]] && [[ -z ${unline_path} ]]
then
PREHOME=$1
elif [[ ! -z ${unline_path} ]]
then
PREHOME=${unline_path}
else
PREHOME=${HOME}"/unline"
fi
DIR=$( dirname "$(readlink -f  ${BASH_SOURCE[0]})" )
#当前脚本上一层路径
PARENTPATH=$(dirname $(dirname ${DIR}))
SOURCEFILE=`sh ${PARENTPATH}/getunlinefile.sh ${PREHOME} wechat`
sudo chmod +x ${PREHOME}/wechat/deepin-wine-for-ubuntu/install.sh
sudo sh ${PREHOME}/wechat/deepin-wine-for-ubuntu/install.sh
sudo dpkg -i ${PREHOME}/wechat/${SOURCEFILE}
if [[ ! -d ${HOME}/.local/bin ]]
then
  mkdir -p ${HOME}/.local/bin
fi
echo "#!/bin/bash" >> ${HOME}/.local/bin/weixin
echo "ps -ef | grep '/opt/weixin/weixin' | grep -v grep | awk '{print $2}' | xargs kill -9" >> ${HOME}/.local/bin/weixin
echo "nohup /opt/weixin/weixin>/dev/null &" >> ${HOME}/.local/bin/weixin
chmod +x ${HOME}/.local/bin/weixin
sudo sed -i"" '2cName=weixin' /usr/share/applications/weixin.desktop
sudo sed -i"" '4cExec='"${HOME}"'/.local/bin/weixin %U' /usr/share/applications/weixin.desktop
