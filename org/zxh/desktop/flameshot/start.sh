echo y |sudo apt-get install g++ cmake build-essential qt5-default qttools5-dev-tools libqt5svg5-dev qttools5-dev
# Run-time
echo y |sudo apt-get install libqt5dbus5 libqt5network5 libqt5core5a libqt5widgets5 libqt5gui5 libqt5svg5
# Optional
echo y |sudo apt-get install git openssl ca-certificates
if [[ ! -z $1 ]] && [[ -d $1 ]] && [[ -z ${unline_path} ]]
then
PREHOME=$1
elif [[ ! -z ${unline_path} ]]
then
PREHOME=${unline_path}
else
PREHOME=${HOME}"/unline"
fi
DIR=$( dirname "$(readlink -f  ${BASH_SOURCE[0]})" )
#当前脚本上一层路径
PARENTPATH=$(dirname $(dirname ${DIR}))
SOURCEFILE=`sh ${PARENTPATH}/getunlinefile.sh ${PREHOME} flameshot`
sudo dpkg -r flameshot
sudo dpkg -i ${PREHOME}/flameshot/${SOURCEFILE}
