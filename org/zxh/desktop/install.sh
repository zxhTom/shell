CURRENTPATH=$(readlink -f "$0")
arr=(`echo ${CURRENTPATH} | tr '/' ' '`)
shell_name=${arr[${#arr[@]}-1]}
echo "current shell name = "${shell_name}
DIR=$( dirname "$(readlink -f  ${BASH_SOURCE[0]})" )
echo "current shell in path : "$DIR
files=`ls ${DIR}`
install_files=(start.sh install.sh finalshell_install_linux.sh)
for file in ${files[@]}
do
  if [[ "${file}" = "${shell_name}" ]]
  then
    echo "do not install self"
  else
    echo "start install file="$file
    for install_file in ${install_files[@]}
    do
      if [[ -f ${DIR}/${file}/${install_file} ]]
      then
        sh ${DIR}/${file}/${install_file}
      fi
    done
    # if [[ -f ${DIR}/${file}/start.sh ]]
    # then
    # sh ${DIR}/$file/start.sh
    # fi
    # if [[ -f ${DIR}/${file}/install.sh ]]
    # then
    # sh ${DIR}/$file/install.sh
    # fi
  fi
done
