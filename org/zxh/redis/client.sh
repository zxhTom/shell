#/bin/bash
#解压至/opt/soft/client/redis
if [ ! -d "/opt/soft/client/redis" ];then
sudo mkdir -p /opt/soft/client/redis
else
sudo rm -rf /opt/soft/client/redis/*
fi
if [ ! -e "${HOME}/.zxhtom/profile" ]
then
  mkdir ${HOME}/.zxhtom
  echo '#!bin/bash' > ${HOME}/.zxhtom/profile
fi
if [[ ! -z $1 ]] && [[ -d $1 ]] && [[ -z ${unline_path} ]]
then
  PREHOME=$1
elif [[ ! -z ${unline_path} ]]
then
  PREHOME=${unline_path}
else
  PREHOME=${HOME}"/unline"
fi

#当前脚本所在路径
CURRENTPATH=$(readlink -f "$0")
CUR=`pwd`
echo "当前脚本位置。。。。"${CURRENTPATH}
echo "..."${CUR}
DIR=$( dirname "$(readlink -f  ${BASH_SOURCE[0]})" )
echo ${DIR}
#当前脚本上一层路径
PARENTPATH=$(dirname ${DIR})
SOURCEFILE=`sh ${PARENTPATH}/getunlinefile.sh ${PREHOME} redisclient`
sudo tar -zxvf ${PREHOME}/redisclient/${SOURCEFILE} -C /opt/soft/client/redis --strip-components=1
BINPATH=`ls /opt/soft/client/redis | grep bin`
if [[ -z $BINPATH ]]
then
TOOLSNAME=`ls -t /opt/soft/client/redis | head -n 1`
fi
grep -q 'export REDIS_CLIENT_HOME=/opt/soft/client/redis/'"${TOOLSNAME}"'' ~/.zxhtom/profile
if [ $? -eq 1 ];then
sudo sed -i'' '$aexport REDIS_CLIENT_HOME=/opt/soft/client/redis/'"${TOOLSNAME}"'' ~/.zxhtom/profile
fi
grep -q 'export PATH=${REDIS_CLIENT_HOME}/bin:${PATH}' ~/.zxhtom/profile
if [ $? -eq 1 ];then
sudo sed -i'' '$aexport PATH=${REDIS_CLIENT_HOME}/bin:${PATH}' ~/.zxhtom/profile
fi
#刷新环境变量
grep -q 'source '"${HOME}"'/.zxhtom/profile' ~/.zshrc
if [ $? -eq 1 ];then
sudo sed -i'' '$asource '"${HOME}"'/.zxhtom/profile' ~/.zshrc
fi
sudo source ~/.zshrc
