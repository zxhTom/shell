#!/bin/bash
# -*- coding: UTF-8 -*-
#----------------------公共方法存放区域开始----------------------#
# 获取当前脚本执行路径
SELFPATH=$(cd "$(dirname "$0")"; pwd)
suffix=.tar.gz
tgz=.tgz
# 获取当前redis文件名称 grep -c 1表示下面正则只会匹配一条数据，这样是为了防止当前目录下出现多个redis版本信息
REDISNAME=`ls -l | grep redis-[0-9]*.[0-9]*.[0-9]| awk -F" " 'NR==1{print $9}'`
#获取当前文件夹中redis文件名称.目前此方法有问题。没有在使用
getRedisNameByNowFloder(){
	#设置默认redis版本信息
	result="4.10.6";
	for file in $(ls redis*)
		do
  		if [[ "$file" =~ "redis-" ]];then
   	 		result= ${file%%:*}
			break
 		fi
		done
	echo $result;
}
#----------------------公共方法存放区域结束----------------------#

#----------------------根据官网数据复制集群数据----------------------#
initData(){
cd $SELFPATH
echo 开始部署
#目前测试阶段，每次都删除数据，最终此命令删除
#rm -rf redis-cluster
#首先看有没有redis文件或者是redis压缩包
echo "redisname $REDISNAME "
if [[ "$REDISNAME" =~ ^redis-[0-9]{1,}\.[0-9]{1,}\.[0-9]+$  ]];then
	echo "匹配到合格数据 $REDISNAME"
else
	echo "您的redis数据不合法或者不存在，本系统将清楚脏数据"
	#获取默认redis数据
	wget http://download.redis.io/releases/redis-4.0.6.tar.gz
	#解压
	tar -xzf redis-4.0.6.tar.gz
	#解压后最好删除压缩包
	#rm -rf redis-4.0.6.tar.gz
	REDISNAME=redis-4.0.6
	cd $REDISNAME
	make
fi 
cd $SELFPATH
#因为在每个redis.conf中配置的其他生成文件比如node-conf，这样的文件将会汇总到当前问文件夹下
#为了方便管理这里我们需要将这些文件统一生成到制定的文件夹下
redisCluster="redis-cluster"
nodeConfig="$redisCluster/nodes"
#判断当前文件是否存在，不存在则创建
if [ ! -f $SELFPATH/$nodeConfig ]; then
	mkdir -p  $SELFPATH/$nodeConfig
fi

#进入集群文件夹下面
cd $redisCluster

#创建集群分文件
mkdir redis{7000,7001,7002,7003,7004,7005}

#循环上面6个文件夹，将准备好的redis中的redis.conf拷贝过来
for dir in redis* ; do cp -v $SELFPATH/$REDISNAME/redis.conf $dir ; done

#循环复制redis.conf后，我们需要做的是循环改变redis.conf的内容
for dir in redis* ; do
#通过正则获取文件名中的数字作为端口号。
myport=`basename $dir|sed 's/[a-zA-Z]*//g'`; 
#对应下面的命令解释
##设置端口
##取消ip绑定，这里的绑定并不是我们常规理解的绑定，其实恰恰相反，绑定了ip就是只有这个ip可以访问redis，不绑定的话我们就可以任何网络访问本redis了。对应的我们得关掉安全模式
##设置该redis为集群模式，因为该教程是基于redis集群搭建的，所以这里必须改为yes
##安全模式，默认是yes，也就是说默认情况下外网是无法访问到本redis的。对应bind字段理解
##上面说了这事搭建redis集群准备的，集群中就是通过多个redis来分摊slot槽的。node就是记录slot分配情况
sed -r -i "
s@^\s*daemonize.*@daemonize yes@;
s@^\s*pidfile.*@pidfile /var/run/redis_$myport.pid@;
s@^\s*port.*@port $myport@;
s@^\s*bind 127.0.0.1.*@#bind 127.0.0.1@;
s@^\s*#?\s*cluster-enabled.*@cluster-enabled yes@;
s@^\s*#?\s*protected-mode.*@protected-mode no@;
s@^\s*#?\s*cluster-config-file.*@cluster-config-file ./$nodeConfig/nodes-$myport.conf@;
" $dir/redis.conf; 
done
}
#----------------------备份官网及修改数据结束----------------------#
#----------------------循环启动上一步我们复制的redis服务开始----------------------#
startServers(){
	
	#因为第一次启动没有生成文件会报错，所以我们还是多执行几次启动命令
	#设置启动层次
	index=1
	while((index<5))
	do 
		#启动redis-cluster文件中的redis服务,因为linux中方法是分离的，所以虽然我们上面的方法cd redis-cluster
        	#但是此时我们还是在redis-cluster的上一级。
        	echo "我们当前路径 $REDISNAME "
		for dir in $SELFPATH/redis-cluster/redis* ; do $SELFPATH/$REDISNAME/src/redis-server $dir/redis.conf ; done
        	redisCount=`ps -ef | grep redis| awk -F" " 'END{print $10}'`
       		#获取ps-ef命令能不能输出cluster的进程，
        	if [[ "$redisCount" =~ "[cluster]" ]];then
                	echo "多个redis服务启动成功。"
			break
        	else
                	echo “好像失败了,已经第 $index 此了”
        	fi
		let "index+=1"
	done
}
#----------------------循环启动上一步我们复制的redis服务结束----------------------#
#----------------------集群启动开始----------------------#
cluster(){
	#集群启动前我们需要删除集群中的节点信息
	echo "集群正在启动中...."
	#首先获取本机的外网ip
	outIp=`curl ifconfig.me|awk '{print $0}'`
	echo "您的外网ip= $outIp "
	$SELFPATH/$REDISNAME/src/redis-trib.rb create --replicas 1 $outIp:7000 $outIp:7001 $outIp:7002 $outIp:7003 $outIp:7004 $outIp:7005
}
#----------------------集群启动结束----------------------#
#----------------------安装依赖开始----------------------#
installNecess(){
	cd $SELFPATH
	#安装必要的环境
	yum -y install gcc zlib-devel openssl openssl-developenssl openssl-devel curl curl-devel
	#检测当前文件是否有ruby
	rubyName=ruby-2.3.6
	ruby=`ruby --version|awk -F" " '{print $1}'`
        if [[ "$ruby" =~ "ruby-"  ]];then
                echo "您的电脑上已经安装了ruby,此版本不在安装ruby了"    
        else
		echo "您的电脑还没有安装ruby,让哥来给你安装吧"
		index=1
		while((index<5))
		do
			rubyName=`ls -l | grep ruby-*[^a-z]| awk -F" " 'NR==1{print $9}'`
                	echo "文件 $rubyName "
                	if [[ "$rubyName" == ""  ]];then
                        	echo "我也是醉了，你练文件也想让我给你下载啊，好吧"
                        	#下载ruby2.3.6版本 默认的
                        	wget https://cache.ruby-lang.org/pub/ruby/2.3/ruby-2.3.6.tar.gz
                	else
				#说明获取到了文件名 开始解压
				if [[ "$rubyName" =~ "tar.gz"  ]];then
					tar -xzf $rubyName
				else
					break
				fi
			fi
		done
	fi
	echo "准备进去 $rubyName"
        cd  $rubyName/
        ./configure
        make
        sudo make install
        cd $SELFPATH
	cd $rubyName/ext/zlib/
	ruby extconf.rb
	echo "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"
	make && make install
	
	####################################################################################################################################
	cd $SELFPATH
	rubygemsName=rubygems-2.7.3
        gem=`gem --version|awk -F" " '{print $1}'`
        if [[ "$gem" != " "  ]];then
                echo "您的电脑上已经安装了rubygems,此版本不在安装rubygems了"    
        else
                echo "您的电脑还没有安装rubygems,让哥来给你安装吧"
                index=1
                while((index<5))
                do
                        rubygemsName=`ls -l | grep rubygems-*[^a-z]| awk -F" " 'NR==1{print $9}'`
                        echo "文件 $rubygemsName "
                        if [[ "$rubygemsName" == ""  ]];then
                                echo "我也是醉了，你练文件也想让我给你下载啊，好吧"
                                #下载rubygems2.7.4版本 默认的
                                wget https://rubygems.org/rubygems/rubygems-2.7.3.tgz
                        else
                                #说明获取到了文件名 开始解压
                                if [[ "$rubygemName" =~ "tgz"  ]];then
					tar -xzf $rubygemsName
				else
					break
				fi
                        fi
                done
        fi
	cd $rubygemName
        ruby setup.rb
        echo "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"
        make && make install
	cd $SELFPATH
	#更换镜像地址
	gem  source -r  https://rubygems.org/
    	gem  source -a  http://gems.ruby-china.org/

}
#----------------------安装依赖结束----------------------#
echo "请输入下面数字进行选择"
echo "#############################################"
echo "#作者网名：张新华"
echo "#作者博客：https://zxhtom.oschina.io"
echo "#作者QQ：870775401"
echo "#作者QQ群:376091466"
echo "#############################################"
echo "------------------------"
echo "1、懒人一键安装"
echo "2、安装必要环境"
echo "3、安装git"
echo "4、卸载"
echo "5、redis服务启动"
echo "6、集群启动"
echo "7、终极一键"
echo "------------------------"
read num
case "$num" in 
	[1] )
		initData
	;;
	[2] )
		installNecess
	;;
	[3] )
	;;
	[4] )
	;;
	[5] )
		startServers
	;;
	[6] )
		cluster
	;;
	[7] )
		#准备文件
                initData
		#首先安装必要环境
		installNecess
		#开启redis服务
		startServers
		#集群服务开启
		cluster
	;;
	*) echco "";;#相当于else
esac
touch zxh.sh
echo "echo testzxh" >> zxh.sh
chmod 700 zxh.sh 
