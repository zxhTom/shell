#!/bin/bash
# 构建Python read.py并更名为project
CUR=`pwd`
# DIR=$( dirname "$(readlink -f  ${BASH_SOURCE[0]})" )
DIR=$(cd "$(dirname "$0")";pwd)
#当前脚本上一层路径 org/zxh
PARENTPATH=$(dirname ${DIR})
ROOTPATH=$(dirname $(dirname ${PARENTPATH}))
echo "root="${PARENTPATH}
if [[ ! -f ${HOME}/.config/center.yml ]]
then
  cp ${PARENTPATH}/python/center.yml ${HOME}/.config/
fi

if [[ ! -d ${HOME}/zxh/bin/python ]]
then
  mkdir -p ${HOME}/zxh/bin/python
fi
cp -rf ${PARENTPATH}/python/* ${HOME}/zxh/bin/python
chmod +x ${HOME}/zxh/bin/python/read.py
sudo ln -sf ${HOME}/zxh/bin/python/read.py /usr/local/bin/project

#######################shuiliandong#######################

if [[ ! -d ${HOME}/zxh/bin/bash ]]
then
  mkdir -p ${HOME}/zxh/bin/bash
fi
cp -rf ${ROOTPATH}/com/yapai/* ${HOME}/zxh/bin/bash
chmod +x ${HOME}/zxh/bin/bash/shuiliandong.sh
sudo ln -sf ${HOME}/zxh/bin/bash/shuiliandong.sh /usr/local/bin/deploy
