sudo apt -y install xcape
res=$(ps -ef | grep "xcape -e ISO_Group_Shift=Tab" | grep -v grep | awk '{print $1}')
if [[ -z $res ]]
then
  killall xcape
  xcape -e "ISO_Group_Shift=Tab"
fi
DIR=$( dirname "$(readlink -f  ${BASH_SOURCE[0]})" )
cp ${DIR}/.xmodmap ${HOME}/
xmodmap ${HOME}/.xmodmap
