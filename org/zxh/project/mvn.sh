if [[ ! -z $1 ]] && [[ -d $1 ]] && [[ -z ${unline_path} ]]
then
  PREHOME=$1
elif [[ ! -z ${unline_path} ]]
then
  PREHOME=${unline_path}
else
  PREHOME=${HOME}"/unline"
fi
export JAVA_HOME=/opt/soft/java/jdk8
CURRENTPATH=$(readlink -f "$0")
CUR=`pwd`
echo "当前脚本位置。。。。"${CURRENTPATH}
echo "..."${CUR}
DIR="$( cd "$( dirname "$(readlink -f  ${BASH_SOURCE[0]})" )" && pwd )"
PARENTPATH=$(dirname ${DIR})
cd ${PREHOME}/drivers
sh ${PARENTPATH}/maven/transtomaven.sh
sh ${PARENTPATH}/python/driver.sh
sh ${PARENTPATH}/python/installchromedriver.sh
cd ${PARENTPATH}/python
python3 read.py
export JAVA_HOME=/opt/soft/java/jdk8
/opt/soft/maven/bin/mvn clean install -DskipTests -Dgpg.passphrase=zxhtom -f ~/zxh/project/git/bottom/bottom/pom.xml
/opt/soft/maven/bin/mvn clean install -DskipTests -Dgpg.passphrase=zxhtom -f ~/zxh/project/git/office-root/office-root/pom.xml
/opt/soft/maven/bin/mvn clean install -DskipTests -Dgpg.passphrase=zxhtom -f ~/zxh/project/git/sdk/pom.xml
cp ${HOME}/zxh/project/git/sdk/crawler-accessor/target/crawler.jar ${HOME}/zxh/jar
cp -R ${PREHOME}/juejin/test ${HOME}/zxh/chrome
