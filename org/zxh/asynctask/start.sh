if [[ ! -z $1 ]] && [[ -d $1 ]] && [[ -z ${unline_path} ]]
then
PREHOME=$1
elif [[ ! -z ${unline_path} ]]
then
PREHOME=${unline_path}
else
PREHOME=${HOME}"/unline"
fi
CURRENTPATH=$(readlink -f "$0")
CUR=`pwd`
echo "当前脚本位置。。。。"${CURRENTPATH}
echo "..."${CUR}
DIR=$( dirname "$(readlink -f  ${BASH_SOURCE[0]})" )
echo ${DIR}
#当前脚本上一层路径
PARENTPATH=$(dirname ${DIR})
grep -q 'alias task=' ${HOME}/.zxhtom/profile
if [ $? -eq 1 ];then
  echo @@@@
  sudo cat ${DIR}/alias >> ${HOME}/.zxhtom/profile
fi
sudo ln -sf ${HOME}/.config/nvim/plugged/asynctasks.vim/bin/asynctask ${HOME}/.local/bin/
# copy task default
if [[ -d ${PREHOME}/asynctask/.vim ]]
then
  cp -rf ${PREHOME}/asynctask/.vim ${HOME}/
fi
