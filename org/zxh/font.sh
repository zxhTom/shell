#!/bin/bash
if [[ ! -z $1 ]] && [[ -d $1 ]] && [[ -z ${unline_path} ]]
then
PREHOME=$1
elif [[ ! -z ${unline_path} ]]
then
PREHOME=${unline_path}
else
PREHOME=${HOME}"/unline"
fi
#安装nerd字体，neovim/vim中显示图标会正常
sudo cp -R ${PREHOME}/fonts/ /usr/share/fonts/truetype/custom
#生成核心字体信息
sudo mkfontscale  
sudo mkfontdir
sudo fc-cache -fv
# https://github.com/ronniedroid/getnf
