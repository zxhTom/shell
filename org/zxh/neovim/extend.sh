#!/bin/bash
declare -a HANDLERLIST
INDEX=0
getNvimHomePath(){
    HOPATH=`cat ~/.config/nvim/init.vim | grep 'call plug#begin' |awk -F "['\(\)]+" '{print $2}'`
    if [[ ${HOPATH} =~ ~(.*) ]]
    then
        HOPATH=${HOME}${BASH_REMATCH[1]}
    fi
}
doPlug(){
    if [[ $2 =~ :.* ]]
    then
        echo "need handler:["$2"]"
        HANDLERLIST[INDEX]=$2
        let INDEX++
    else
        echo ${HOPATH}
        cd ${HOPATH}
        SUBPLUGPATH=`echo ${PLUGPATH} | awk -F "/" '{print $2}'`
        cd $SUBPLUGPATH
        echo $2 > plug_temp.sh
        sh plug_temp.sh
    fi
}
###################main######################
getNvimHomePath
IFS_old=$IFS      # 记录老的分隔符
IFS=$'\n'  # 换行截取，这样就不会按空格截取了
NEEDHANDLERLINES=`cat ~/.config/nvim/init.vim| grep -o -E '^Plug[^{]+{.*}'`
#NEWARR=(${NEEDHANDLERLINES//Plug/ })
for i in ${NEEDHANDLERLINES[*]}
do
    if [[ $i =~ \'(.*)\'.*\{(.*)\} ]]
    then
        PLUGPATH=${BASH_REMATCH[1]}
        ACTION=${BASH_REMATCH[2]}
        if [[ ${ACTION} =~ \'([^\']+)\'.*\'(.*)\' ]]
        then
            if [[ ${BASH_REMATCH[1]} == "do" ]]
            then
                doPlug ${PLUGPATH} ${BASH_REMATCH[2]}
            fi
        fi
    fi
done
for hi in ${HANDLERLIST[*]}
do
    echo $hi
done
