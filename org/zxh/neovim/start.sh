#!/bin/bash
sudo rm -rf ~/.config/nvim
if [[ ! -z $1 ]] && [[ -d $1 ]] && [[ -z ${unline_path} ]]
then
PREHOME=$1
elif [[ ! -z ${unline_path} ]]
then
PREHOME=${unline_path}
else
PREHOME=${HOME}"/unline"
fi
if [[ -d ${PREHOME}/neovim ]]
then
cp -R ${PREHOME}/neovim ${HOME}/.config/nvim
mkdir -p ${HOME}/.config/coc/extensions/coc-java-data/server
if [[ -f ${HOME}/.config/nvim/serverjdt/jdt-language-server-0.57.0-202006172108.tar.gz ]]
then
  tar -xzvf ${HOME}/.config/nvim/serverjdt/jdt-language-server-0.57.0-202006172108.tar.gz -C ${HOME}/.config/coc/extensions/coc-java-data/server
fi
if [[ ! -d ${HOME}/.config/coc ]]
then
tar -xzvf ${HOME}/.config/nvim/serverjdt/jdt-language-server-0.57.0-202006172108.tar.gz -C ${HOME}/.config/coc/extensions/coc-java-data/server
fi
fi
if [[ ! -d ${HOME}/.config/nvim ]]
then
  mkdir -p ${HOME}/.config/nvim
fi
if [[ -d ${PREHOME}/ripgrep ]]
then
#本地安装
RIPFILE=`ls ${PREHOME}/ripgrep | grep rip | head -n 1`
sudo dpkg -i ${PREHOME}/ripgrep/${RIPFILE}
else
#在线安装
curl -LO https://github.com/BurntSushi/ripgrep/releases/download/13.0.0/ripgrep_13.0.0_amd64.deb -o ~/Download
sudo dpkg -i ${HOME}/Download/ripgrep_13.0.0_amd64.deb
fi
DIR="$( cd "$( dirname "$(readlink -f  ${BASH_SOURCE[0]})" )" && pwd )"
echo ${DIR}
#当前脚本上一层路径
PARENTPATH=$(dirname ${DIR})
ROOTNTPATH=$(dirname ${PARENTPATH})
type python || echo y | sudo apt-get install python2-dev
sudo update-alternatives --install /usr/bin/python python /usr/bin/python2 1
PY3=`python3 --version | awk -F "[ :]+" '{print $2}' | awk -F "." '{print $1$2}'`
if [[ -z ${PY3} || 37 -gt ${PY3} ]]
then
    echo y | sudo apt-get install python3.7
    #sh ${PARENTPATH}/upgradeauto.sh
    sudo rm -rf /usr/bin/python3
	sudo rm -rf /usr/bin/pip3
    sudo ln -s /usr/bin/python3.7 /usr/bin/python3
	sudo ln -s /usr/bin/pip3 /usr/bin/pip3
	sudo sh ${PARENTPATH}/upgradepython.sh
fi

if ! type pip2 >/dev/null 2>&1;
then
echo y | sudo apt install python-pip
fi
if ! type pip3 >/dev/null 2>&1;
then
echo y | sudo apt install python3-pip
# set aliyun source
pip3 config set global.index-url https://mirrors.aliyun.com/pypi/simple
pip3 config set install.trusted-host https://pypi.tuna.aliyun.com/pypi/simple
python3 -m pip install -U pip
fi
NVIMVERSION="aaaaaaaaaaaaa"
# NVIMVERSION2=`type nvim | grep 'not found'`
if ! type nvim >/dev/null 2>&1;
#if [[ "${NVIMVERSION}" = "aaaaaaaaaaaaa" ]] && [[ ! -z ${NVIMVERSION2} ]]
then
	echo y | sudo add-apt-repository ppa:neovim-ppa/unstable
	echo y | sudo apt-get update
	echo y | sudo apt-get upgrade
	# echo y | sudo apt-get install neovim
  if [[ ! -d ${HOME}/temp/neovim ]]
  then
    mkdir -p ${HOME}/temp/neovim
  fi
  SOURCEFILE=`sh ${PARENTPATH}/getunlinefile.sh ${PREHOME} neovim/exe`
  if [[ ! -f ${PREHOME}/neovim/exe/${SOURCEFILE} ]]
  then
  curl -o ${HOME}/temp/neovim/nvim-linux64.tar.gz https://github.com/neovim/neovim/releases/download/v0.9.0/nvim-linux64.tar.gz 
  tar -zxvf ${HOME}/temp/neovim/nvim-linux64.tar.gz -C ${HOME}/temp/neovim/
  else
  tar -zxvf ${PREHOME}/neovim/exe/${SOURCEFILE} -C ${HOME}/temp/neovim/
  fi
  ln -sf ${HOME}/temp/neovim/nvim-linux64/bin/nvim ${HOME}/.local/bin/nvim
	echo y | sudo apt-get install lolcat
	sudo pip3 install neovim
	sudo pip install neovim
    python3 -m pip install neovim
	npm config set registry https://registry.npm.taobao.org
	sudo npm i -g bash-language-server
else
	NVIMVERSION=`type nvim`
	echo "neovim version="${NVIMVERSION}
fi
if [[ ! -f ${HOME}/.config/nvim/init.vim ]]
then
echo '' > ${HOME}/.config/nvim/init.vim
sed -i'' '$acall plug#begin('\''~/.config/nvim/plugged'\'')' ${HOME}/.config/nvim/init.vim
sed -i'' '$acall plug#end()' ${HOME}/.config/nvim/init.vim
fi
PLUGINS=`ls ${HOME}/.config/nvim/plugged`
ADDLINE=`cat ${HOME}/.config/nvim/init.vim | grep -n 'call plug#end()' | awk -F '[:]+' '{print $1}'`
for fn in ${PLUGINS[*]}
do
    grep -q ${fn} ${HOME}/.config/nvim/init.vim
	if [[ $? -eq 1 ]]
	then
		if [[ -f ${HOME}/.config/nvim/plugged/${fn}/.git/config ]]
		then
		GITURL=`cat ${HOME}/.config/nvim/plugged/${fn}/.git/config | grep 'url' | awk -F '[= ]+' '{print $2}'`
		FILODER=(`echo $GITURL | awk -F "[/]" '{print $(NF-1)"/"$NF}' | awk -F ".git" '{print $1}'`)
		echo ${FILODER}
		sed -i'' ''"${ADDLINE}"'iPlug '\'''"${FILODER}"''\''' ${HOME}/.config/nvim/init.vim
		else
		sed -i'' ''"${ADDLINE}"'iPlug '\''./'"${fn}"''\''' ${HOME}/.config/nvim/init.vim
		fi
	fi
	if [[ -f ${DIR}/${fn}_config ]]
	then
		echo "${fn}_config no"
		grep -q ${fn}"install..." ${HOME}/.config/nvim/init.vim
		if [[ $? -eq 1 ]]
		then
			cat ${DIR}/${fn}_config  >>  ${HOME}/.config/nvim/init.vim
		fi
	fi
done
sh ${DIR}/extend.sh
sh ${PARENTPATH}/terminal/neovimcp.sh
