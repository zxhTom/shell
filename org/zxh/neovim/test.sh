PLUGINS=(`ls ${HOME}/.config/nvim/plugged`)
for fn in ${PLUGINS[*]}
do
	if [[ -f ${HOME}/.config/nvim/plugged/${fn}/.git/config ]]
	then
	GITURL=`cat ${HOME}/.config/nvim/plugged/${fn}/.git/config | grep 'url' | awk -F '[= ]+' '{print $2}'`
	FILODER=(`echo $GITURL | awk -F "[/]" '{print $(NF-1)"/"$NF}' | awk -F "." '{print $1}'`)
	echo ${FILODER}
        fi

done
