#/bin/bash
# 主要用于备份i3 连接信息
if [[ ! -z $1 ]] && [[ -d $1 ]] && [[ -z ${unline_path} ]]
then
PREHOME=$1
elif [[ ! -z ${unline_path} ]]
then
PREHOME=${unline_path}
else
PREHOME=${HOME}"/unline"
fi
DIR=$( dirname "$(readlink -f  ${BASH_SOURCE[0]})" )
#当前脚本上一层路径
PARENTPATH=$(dirname $(dirname ${DIR}))
if [[ ! -d ${PREHOME}/i3/backup ]]
then
  mkdir -p ${PREHOME}/i3/backup
fi
cp -rf ${HOME}/.config/i3 ${PREHOME}/i3/backup
