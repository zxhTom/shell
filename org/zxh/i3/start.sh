echo y | sudo apt install ranger
echo y | sudo apt install i3status
echo y | sudo apt install suckless-tools
echo y | sudo apt install i3blocks
echo y | sudo apt install xfce4-terminal
echo y | sudo apt install ncmpcpp
echo y | sudo apt install conky
echo y | sudo apt-get install xfce4
echo y | sudo apt install libxcb1-dev libxcb-keysyms1-dev libpango1.0-dev libxcb-util0-dev libxcb-icccm4-dev libyajl-dev libstartup-notification0-dev libxcb-randr0-dev libev-dev libxcb-cursor-dev libxcb-xinerama0-dev libxcb-xkb-dev libxkbcommon-dev libxkbcommon-x11-dev autoconf xutils-dev libtool
echo y | sudo apt-get install feh 
echo y | sudo apt-get install neofetch 
echo y | sudo apt-get install compton
echo y | sudo apt-get install picom
echo y | sudo apt-get i3
# autoconfi
echo y | sudo apt-get install autoconf autotools-dev m4 autoconf2.13 autoconf-archive gnu-standards autoconf-doc libtool

sudo apt update
echo y | sudo apt install i3-gaps
echo y | sudo apt-get install rofi

if [[ ! -z $1 ]] && [[ -d $1 ]] && [[ -z ${unline_path} ]]
then
PREHOME=$1
elif [[ ! -z ${unline_path} ]]
then
PREHOME=${unline_path}
else
PREHOME=${HOME}"/unline"
fi
cp -r ${PREHOME}/i3/backup/* ${HOME}/.config/
