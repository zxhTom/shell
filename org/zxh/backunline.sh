# backup unline folders
if [[ ! -z $1 ]] && [[ -d $1 ]] && [[ -z ${unline_path} ]]
then
PREHOME=$1
elif [[ ! -z ${unline_path} ]]
then
PREHOME=${unline_path}
else
PREHOME=${HOME}"/unline"
fi
currentpath=`pwd`
rm -rf ${HOME}/shell.zip
rm -rf ${HOME}/unline.zip
cd ${HOME}
ROOT_UNLINE_PATH=$(dirname ${PREHOME})
cd ${ROOT_UNLINE_PATH}
zip -r ${HOME}/unline.zip unline/
zip -r ${HOME}/shell.zip shell/
cd ${currentpath}
if [[ ! -d ${HOME}/back ]]
then
  mkdir -p ${HOME}/back
fi
cp ${HOME}/shell.zip ${HOME}/back/
cp ${HOME}/unline.zip ${HOME}/back/
#unzip -o ${HOME}/unline.zip -d ${HOME}/zxh
#unzip -o ${HOME}/shell.zip -d ${HOME}/shell
