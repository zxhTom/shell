#/bin/bash
DIR=$( dirname "$(readlink -f  $0)" )
echo ${DIR}
#当前脚本上一层路径
PARENTPATH=$(dirname ${DIR})
system_password=`sed -n '1p' ${DIR}/description | awk -F '[=]+' '{print $2}'`
export unline_path=$1
sh ${DIR}/system.sh -p $1
sh ${DIR}/moreutil.sh