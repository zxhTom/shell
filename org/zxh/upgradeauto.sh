echo y | sudo apt-get install python3.8
echo y | sudo update-alternatives --install /usr/bin/python3 python3 /usr/bin/python3.8 1
echo y | sudo update-alternatives --config python3
echo y | sudo apt-get install python3.8-dev
echo y | sudo apt-get install python3.8-venv
echo y | sudo apt-get install python3-pip
echo y | sudo python3 -m pip install --upgrade pip
DIR="$( cd "$( dirname "$(readlink -f  ${BASH_SOURCE[0]})" )" && pwd )"
echo ${DIR}
#当前脚本上一层路径
PARENTPATH=$(dirname ${DIR})
sh ${DIR}/upgradepython.sh
