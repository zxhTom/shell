
import os
from git.repo import Repo
 
local_path = os.path.join('test','t1')
# Repo.clone_from('https://gitee.com/zxhGroup/bottom.git',to_path=local_path,branch='master')
repo = Repo(local_path)
  
branches = repo.remote().refs
for item in branches:
    print(item.remote_head)
commit_log = repo.git.log('--pretty={"commit":"%h","author":"%an","summary":"%s","date":"%cd"}', max_count=50,
                                  date='format:%Y-%m-%d %H:%M')
log_list = commit_log.split("\n")
for im in log_list:
    im=eval(im)
    print(im['date']+':'+im['commit']+':'+im['author'])
real_log_list = [eval(item) for item in log_list]
#print(real_log_list)
