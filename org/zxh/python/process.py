from git.repo import Repo
from git import RemoteProgress
import time
import os
scale=20
start = time.perf_counter()
class CloneProgress(RemoteProgress):
    def update(self, op_code, cur_count, max_count=None, message=''):
        if message:
            i=int(cur_count*scale/max_count)
            a = "*" * i
            b = "." * (scale - i)
            c = (i / scale) * 100
            dur = time.perf_counter() - start
            print("\r{:^3.0f}%[{}->{}]{:.2f}s".format(c,a,b,dur),end = "")
            time.sleep(0.1)
url="git@gitee.com:zxhTom/shell.git"
download_path=os.path.join('test','shell')
branch_name="master"
# Repo.clone_from(url,to_path=download_path,branch=branch_name,progress=CloneProgress())

local_path = os.path.join('NB')
try:

    repo = Repo(local_path)
except Exception as e:
    print("no git path")
finally:
    print("ended....")
