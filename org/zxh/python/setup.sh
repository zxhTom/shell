#!/bin/bash

if [ ! -e "${HOME}/.zxhtom/profile" ]
then
  mkdir ${HOME}/.zxhtom
  echo '#!bin/bash' > ${HOME}/.zxhtom/profile
fi
export PYTHONPATH=${HOME}/.local/python
grep -q 'export PYTHONPATH=${HOME}/.local/python' ~/.zxhtom/profile
if [ $? -eq 1 ];then
sudo sed -i'' '$aexport PYTHONPATH=${HOME}/.local/python' ~/.zxhtom/profile
fi

if [[ ! -d ${HOME}/.local/python ]]
then
    mkdir -p ${HOME}/.local/python
fi
DIR=$( dirname "$(readlink -f  ${BASH_SOURCE[0]})" )

# cp common tools python script
#cp ${DIR}/ping.py ${HOME}/.local/python
#cp ${DIR}/readfile.py ${HOME}/.local/python
