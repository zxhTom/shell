import properties
import os
import subprocess
p = properties.Properties('/home/zxhtom/zxh/jar/crawler.properties','utf-8')
p.set('browser.chrome','/opt/soft/selenium/chromedriver_linux64/chromedriver')
p.set('browser.binary',subprocess.getoutput('which google-chrome'))
p.set('browser.force.show','false')
p.set('browser.window.img','/home/zxhtom/zxh/chrome/img')
p.set('cookie','/home/zxhtom/zxh/chrome/test')
p.set('study.cookie','/home/zxhtom/zxh/chrome/study')
p.set('study.url','https://juejin.cn/post/7126726691656826917')
p.set('study.fast','false')
p.set('study.fastIndex','1')
p.set('sleep.radio','1')
p.set('user-data-dir','~/.config/zxhtom/google-chrome/')
p.set('back_path','/home/zxhtom/zxh/project/note/blog/source/_posts/args')
p.set('black_path','/home/zxhtom/zxh/chrome/black.txt')
p.set('blacklog_path','/home/zxhtom/zxh/chrome/blacklog.txt')
