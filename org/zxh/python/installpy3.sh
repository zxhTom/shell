DIR="$( cd "$( dirname "$(readlink -f  ${BASH_SOURCE[0]})" )" && pwd )"
echo ${DIR}
#当前脚本上一层路径
PARENTPATH=$(dirname ${DIR})
PY3=`python3 --version | awk -F "[ :]+" '{print $2}' | awk -F "." '{print $1$2}'`
if [[ -z ${PY3} || 37 -gt ${PY3} ]]
then
    echo y | sudo apt-get install python3.7
    #sh ${PARENTPATH}/upgradeauto.sh
    sudo rm -rf /usr/bin/python3
	sudo rm -rf /usr/bin/pip3
    sudo ln -s /usr/bin/python3.7 /usr/bin/python3
	sudo ln -s /usr/bin/pip3 /usr/bin/pip3
	sudo sh ${PARENTPATH}/upgradepython.sh
fi