import subprocess

scheduler_order = "df -h"
return_info = subprocess.Popen(
    scheduler_order, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT
)

for next_line in return_info.stdout:
    return_line = next_line.decode("utf-8", "ignore")
    print(return_line)
