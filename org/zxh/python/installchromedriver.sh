if [[ ! -z $1 ]] && [[ -d $1 ]] && [[ -z ${unline_path} ]]
then
PREHOME=$1
elif [[ ! -z ${unline_path} ]]
then
PREHOME=${unline_path}
else
PREHOME=${HOME}"/unline"
fi
DIR=$( dirname "$(readlink -f  ${BASH_SOURCE[0]})" )
echo ${DIR}
#当前脚本上一层路径
PARENTPATH=$(dirname ${DIR})
SOURCEFILE=`sh ${PARENTPATH}/getunlinefile.sh ${PREHOME} google`
sudo dpkg -i ${PREHOME}/google/${SOURCEFILE}
chrome_version=`google-chrome -version | awk '{print $3}'`
sudo python3 ${DIR}/chromedriver.py ${chrome_version}
if [[ ! -d ${HOME}/zxh/jar ]]
then
  mkdir -p ${HOME}/zxh/jar
fi
touch ${HOME}/zxh/jar/crawler.properties
python3 ${DIR}/test.py
