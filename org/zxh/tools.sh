#!/bin/bash

if [[ -n "$__MODULE_SH__" ]]
then
  return
fi

__MODULE_SH__='tools.sh'

getIp() {
  echo `ifconfig | grep -EA 1  '^(eno|ens|eth|enp)' | grep -Ev '^(eno|ens|eth|enp)' | awk '{print $2}'`
}
