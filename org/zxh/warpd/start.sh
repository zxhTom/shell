if [[ ! -z $1 ]] && [[ -d $1 ]] && [[ -z ${unline_path} ]]
then
PREHOME=$1
elif [[ ! -z ${unline_path} ]]
then
PREHOME=${unline_path}
else
PREHOME=${HOME}"/unline"
fi
#当前脚本所在路径
CURRENTPATH=$(readlink -f "$0")
CUR=`pwd`
echo "当前脚本位置。。。。"${CURRENTPATH}
echo "..."${CUR}
DIR=$( dirname "$(readlink -f  ${BASH_SOURCE[0]})" )
echo ${DIR}
#当前脚本上一层路径
PARENTPATH=$(dirname ${DIR})
if [[ ! -d ${HOME}/temp/github ]]
then
  mkdir -p ${HOME}/temp/github
fi
sh ${PARENTPATH}/tools/gitclone.sh https://github.com/rvaiya/warpd.git  ${HOME}/temp/github/warpd
echo y | sudo apt-get install \
	libxi-dev \
	libxinerama-dev \
	libxft-dev \
	libxfixes-dev \
	libxtst-dev \
	libx11-dev \
	libcairo2-dev \
	libxkbcommon-dev \
	libwayland-dev &&
cd ${HOME}/temp/github/warpd
make && sudo make install
