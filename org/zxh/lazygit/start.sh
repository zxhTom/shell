#!/bin/bash
if [[ ! -z $1 ]] && [[ -d $1 ]] && [[ -z ${unline_path} ]]
then
PREHOME=$1
elif [[ ! -z ${unline_path} ]]
then
PREHOME=${unline_path}
else
PREHOME=${HOME}"/unline"
fi
if [[ -f ${PREHOME}/bin/lazygit ]]
then
    if [[ ! -f ${HOME}/.local/bin ]]
    then
	    mkdir -p ${HOME}/.local/bin
    fi
    sudo  cp ${PREHOME}/bin/lazygit ${HOME}/.local/bin/
    sudo chmod +x ${HOME}/.local/bin/lazygit
    if [[ ! -f /usr/bin/lazygit ]]
    then
    sudo ln -s ${HOME}/.local/bin/lazygit /usr/bin/lazygit
    fi
fi