#!/bin/bash
ZSHVERSION=`zsh --version | awk '{print $2}'`
PLUGINTEXT="plugins=(\nzsh-autosuggestions\ngit\nrand-quote\nthemes\ngitignore\ncp\nz\nvi-mode\ncommand-not-found\nsafe-paste\ncolored-man-pages\nsudo\nhistory-substring-search\nzsh-syntax-highlighting\nwd\nzsh-vi-mode\n)\n"
if [[ -n ${ZSHVERSION}  ]]
then
  echo "您已经安装zsh，版本="${ZSHVERSION}
else
# 安装zsh
echo "start install zsh"
echo y | sudo apt install zsh
echo y | sudo apt install xsel
echo y | sudo apt install xclip
if [ ! -e "${HOME}/.zxhtom/profile" ]
then
  mkdir ${HOME}/.zxhtom
  echo '#!bin/bash' > ${HOME}/.zxhtom/profile
fi
echo "alias pbcopy='xsel --clipboard --input'" >> ${HOME}/.zxhtom/profile
echo "alias pbpaste='xsel --clipboard --output'" >> ${HOME}/.zxhtom/profile
type python || echo y | sudo apt-get install python-dev
fi
# 检测是否已经下载过oh-my-zsh
OMZ=${HOME}"/.oh-my-zsh"
if [[ -d "${OMZ}" ]]
then
  echo "已存在"
else
  sh gitclone.sh  https://github.com/robbyrussell/oh-my-zsh ${OMZ}
  # 复制配置文件到user路径
  cp ~/.oh-my-zsh/templates/zshrc.zsh-template ~/.zshrc
  # 下载所需插件
  sh gitclone.sh https://github.com/zsh-users/zsh-autosuggestions.git ~/.oh-my-zsh/custom/plugins/zsh-autosuggestions
  sh gitclone.sh https://github.com/zsh-users/zsh-syntax-highlighting.git ~/.oh-my-zsh/custom/plugins/zsh-syntax-highlighting
  sh gitclone.sh https://github.com/jeffreytse/zsh-vi-mode ~/.oh-my-zsh/custom/plugins/zsh-vi-mode
  sh gitclone.sh https://github.com/wting/autojump ~/.oh-my-zsh/plugins/autojump
  sed -i'' '$a[[ -s /home/zxhtom/.autojump/etc/profile.d/autojump.sh ]] && source /home/zxhtom/.autojump/etc/profile.d/autojump.sh' ~/.zshrc
  cp -r ${OMZ}/plugins/autojump ${OMZ}/custom/plugins/
fi
#sh gitclone.sh https://github.com/zsh-users/zsh-autosuggestions.git ~/.oh-my-zsh/custom/plugins/zsh-autosuggestions
#sh gitclone.sh https://github.com/zsh-users/zsh-syntax-highlighting.git ~/.oh-my-zsh/custom/plugins/zsh-syntax-highlighting
#sudo apt-get install autojump
NEEDELETELINES=(`cat ~/.zshrc | awk '/^[ ]*plugins/,/\)/{print NR,$0}' | awk '{print $1}'`)
echo "@@@"${#NEEDELETELINES[@]}
FIRSTLINE=${NEEDELETELINES[0]}
LASTLINE=${NEEDELETELINES[${#NEEDELETELINES[@]}-1]}
echo "firstline="${FIRSTLINE}"lastline="${LASTLINE}
sed -i'' ''"${FIRSTLINE}"','"${LASTLINE}"'d' ~/.zshrc
sed -i'' ''"${FIRSTLINE}"'c'"${PLUGINTEXT}"'' ~/.zshrc
# 获取主题配置所在行号
THEMELINE=`cat ~/.zshrc  | grep -nE "ZSH_THEME" | grep -v "^[0-9: ]*#" | awk -F "[:]+" '{print $1}'`
sed -i'' ''"${THEMELINE}"'cZSH_THEME="agnoster"' ~/.zshrc
# 提取脚本赋予命令
if [[ ! -z $1 ]] && [[ -z ${system_password} ]]
then
  system_password=$1
fi
DIR=$( dirname "$(readlink -f  ${BASH_SOURCE[0]})" )
#当前脚本上一层路径
PARENTPATH=$(dirname ${DIR})
# 设置默认密码
if [[ -z ${system_password} ]]
then
  system_password=`cat ${PARENTPATH}/description | grep -E 'system_password' | awk -F '=' '{print $2}'`
fi
# 设置zsh作为shell
chsh -s $(which zsh) << EOF
${system_password}
EOF
cu=`pwd`
cd ${HOME}/.oh-my-zsh/plugins/autojump
python3 install.py
cd ${cu}
chmod +x ${HOME}/.autojump/bin/autojump
if ! type python >/dev/null 2>&1;
then
  echo y | sudo apt install python-is-python3
fi
cp -f example.zsh ${HOME}/.oh-my-zsh/custom/
