#!/bin/bash
#配置github国内加速
#199.232.69.194 global-ssl.fastly.net
#140.82.112.4 github.com
#199.232.4.133 raw.githubusercontent.com
#185.199.108.154 github.githubassets.com 
while read line
do
if [[ -z ${line} ]]
then
  continue
fi
ZHUSHI=`echo ${line} | grep '#'`
if [[ -n ${ZHUSHI} ]]
then
  continue
fi
echo "line="${line}
DOMAIN=`echo ${line} | awk '{print $2}'`
echo "DOMAIN="${DOMAIN}
ETCCONTENT=`cat /etc/hosts | grep -v '#' | grep -nE '[0-9,]+ '"${DOMAIN}"'$' | awk -F "[: ]+" '{print $1}'`
echo "ETCCONTENT="${ETCCONTENT}
if [[ -z ${ETCCONTENT} ]]
then
  echo "不存在"
  sudo sed -i '$a'"${line}"'' /etc/hosts
else
# 替换
  echo "存在"
  for nui in ${ETCCONTENT[*]}
  do
    echo "nui="${nui}
    sudo sed -i ''"${nui}"'c'"${line}"'' /etc/hosts
  done
fi
done < github
