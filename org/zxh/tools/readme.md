# 使用前提

  - 使用改脚本完成Ubuntu系统初始化必须支持`[[]]`条件判断。需要我们手动执行`sudo dpkg-reconfigure dash`并选择`No`. （已在first.sh解决）
  - docker使用中需要创建用户组并且重启生效，所以也需要您提前手动配置好(已在first.sh解决，只需最后reboot)
  
  ```shell

  sudo groupadd docker
  sudo gpasswd -a $USER docker
  newgrp docker
  reboot

  ```

# 脚本说明

first.sh : 负责初始化ubuntu配置，包括ssh配置，root密码配置
github.sh: 负责配置github外网访问配置
git.sh   : 负责配置gitusername,email 生成ssh等
install.sh:负责安装ubuntu必须服务
gitclone.sh:负责拉取远程项目；网络拉取失败会尝试拉取本地离线版本重试
zsh.sh   : 负责初始化oh-my-zsh配置
client/install.sh : 负责安装数据库客户端工具
