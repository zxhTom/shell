ZSH_AUTOSUGGEST_COMPLETION_IGNORE=("git *")
ZSH_AUTOSUGGEST_STRATEGY=(history completion)
bindkey '^ ' autosuggest-accept
bindkey '\eq' autosuggest-execute