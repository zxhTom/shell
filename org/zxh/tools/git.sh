#!/bin/bash
GITVERSION="$(git --version | awk '{print $3}')"
if [[ -z ${GITVERSION} ]]
then
  echo "GIT NOT FOUND"
  echo y | sudo apt-get install git
else
  echo "version="${GITVERSION}
fi
git config --global user.name zxhtom
git config --global user.email 870775401@qq.com
rm -rf ~/.ssh/{id_rsa,id_rsa.pub}
ssh-keygen -C '870775401@qq.com' -t rsa << EOF



EOF
if [[ ! -z $1 ]] && [[ -d $1 ]] && [[ -z ${unline_path} ]]
then
  PREHOME=$1
elif [[ ! -z ${unline_path} ]]
then
  PREHOME=${unline_path}
else
  PREHOME=${HOME}"/unline"
fi

cp -Rf ${PREHOME}/gitsshkey/* ~/.ssh
