#/bin/bash
#解压至/opt/soft/java
if [ ! -d "/opt/soft/java/default" ];then
sudo mkdir -p /opt/soft/java/default
else
sudo rm -rf /opt/soft/java/default/*
fi
if [ ! -e "${HOME}/.zxhtom/profile" ]
then
  mkdir ${HOME}/.zxhtom
  echo '#!bin/bash' > ${HOME}/.zxhtom/profile
fi
if [[ ! -z $1 ]] && [[ -d $1 ]] && [[ -z ${unline_path} ]]
then
PREHOME=$1
elif [[ ! -z ${unline_path} ]]
then
PREHOME=${unline_path}
else
PREHOME=${HOME}"/unline"
fi
#当前脚本所在路径
CURRENTPATH=$(readlink -f "$0")
CUR=`pwd`
echo "当前脚本位置。。。。"${CURRENTPATH}
echo "..."${CUR}
DIR=$( dirname "$(readlink -f  ${BASH_SOURCE[0]})" )
echo ${DIR}
#当前脚本上一层路径
PARENTPATH=$(dirname ${DIR})
SOURCEFILE=`sh ${PARENTPATH}/getunlinefile.sh ${PREHOME} go`
echo ${SOURCEFILE}
sudo tar -C /usr/local -xzf ${PREHOME}/go/${SOURCEFILE}
sudo ln -sf /usr/local/go/bin/go /bin/go
