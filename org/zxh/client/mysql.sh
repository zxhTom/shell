sudo apt-get install mysql-client <<EOF
y
EOF
if [[ ! -z $1 ]] && [[ -d $1 ]] && [[ -z ${unline_path} ]]
then
PREHOME=$1
elif [[ ! -z ${unline_path} ]]
then
PREHOME=${unline_path}
else
PREHOME=${HOME}"/unline"
fi
DIR=$( dirname "$(readlink -f  ${BASH_SOURCE[0]})" )
echo ${DIR}
#当前脚本上一层路径
PARENTPATH=$(dirname ${DIR})
cp ${PREHOME}/mysql/.mylogin.cnf ${HOME}/
sudo chmod 600 ${HOME}/.mylogin.cnf
sudo chown ${USER}:${USER} ${HOME}/.mylogin.cnf
# sqlupdate 用于数据库比对工具安装
if [[ ! -d ${PREHOME}/mysql/sqlupdate ]]
then
  mkdir -p ${PREHOME}/mysql/sqlupdate
  wget -P ${PREHOME}/mysql/sqlupdate https://bisqwit.iki.fi/src/arch/sqlupdate-1.6.6.tar.gz
fi
if [[ ! -d /opt/soft/mysql/sqlupdate ]]
then
  sudo mkdir -p /opt/soft/mysql/sqlupdate
fi
SOURCEFILE=`sh ${PARENTPATH}/getunlinefile.sh ${PREHOME} mysql/sqlupdate`
sudo tar -zxvf ${PREHOME}/mysql/sqlupdate/${SOURCEFILE} -C /opt/soft/mysql/sqlupdate --strip-components=1
cd /opt/soft/mysql/sqlupdate/
gcc --version
sudo ./configure
sudo make && sudo make install
