if [[ ! -e /opt/soft/client/taos/taosTools-2.2.4-Linux-x64.tar.gz ]]
then
sudo wget https://www.taosdata.com/assets-download/3.0/taosTools-2.2.4-Linux-x64.tar.gz -P /opt/soft/client/taos
fi
if [[ ! -e /opt/soft/client/taos/TDengine-client-3.0.1.4-Linux-x64.tar.gz ]]
then
sudo wget https://www.taosdata.com/assets-download/3.0/TDengine-client-3.0.1.4-Linux-x64.tar.gz -P /opt/soft/client/taos/
fi

sudo tar zxvf /opt/soft/client/taos/taosTools-2.2.4-Linux-x64.tar.gz -C /opt/soft/client/taos/
sudo tar zxvf /opt/soft/client/taos/TDengine-client-3.0.1.4-Linux-x64.tar.gz -C /opt/soft/client/taos/
sh /opt/soft/client/taos/taosTools-2.2.4/install-taostools.sh
CUHOME=`pwd`
cd /opt/soft/client/taos/TDengine-client-3.0.1.4/
sudo sh install_client.sh
cd ${CUHOME}
