#/bin/bash
#解压至/opt/soft/client/kafka
if [ ! -d "/opt/soft/client/zookeeper" ];then
sudo mkdir -p /opt/soft/client/zookeeper
else
sudo rm -rf /opt/soft/client/zookeeper/*
fi
if [ ! -e "${HOME}/.zxhtom/profile" ]
then
  mkdir ${HOME}/.zxhtom
  echo '#!bin/bash' > ${HOME}/.zxhtom/profile
fi
PREHOME=${HOME}"/unline"
if [[ ! -z $1 ]] && [[ -d $1 ]] && [[ -z ${unline_path} ]]
then
PREHOME=$1
elif [[ ! -z ${unline_path} ]]
then
PREHOME=${unline_path}
else
PREHOME=${HOME}"/unline"
fi
#当前脚本所在路径
CURRENTPATH=$(readlink -f "$0")
CUR=`pwd`
echo "当前脚本位置。。。。"${CURRENTPATH}
echo "..."${CUR}
DIR=$( dirname "$(readlink -f  ${BASH_SOURCE[0]})" )
echo ${DIR}
#当前脚本上一层路径
PARENTPATH=$(dirname ${DIR})
SOURCEFILE=`sh ${PARENTPATH}/getunlinefile.sh ${PREHOME} zookeeperclient`
sudo tar -zxvf ${PREHOME}/zookeeperclient/${SOURCEFILE} -C /opt/soft/client/zookeeper --strip-components=1
BINPATH=`ls /opt/soft/client/zookeeper | grep bin`
if [[ -z $BINPATH ]]
then
TOOLSNAME=`ls -t /opt/soft/client/zookeeper | head -n 1`
fi
grep -q 'export ZOOKEEPER_CLIENT_HOME=/opt/soft/client/zookeeper/'"${TOOLSNAME}"'' ~/.zxhtom/profile
if [ $? -eq 1 ];then
sudo sed -i'' '$aexport ZOOKEEPER_CLIENT_HOME=/opt/soft/client/zookeeper/'"${TOOLSNAME}"'' ~/.zxhtom/profile
fi
grep -q 'export PATH=${ZOOKEEPER_CLIENT_HOME}/bin:${PATH}' ~/.zxhtom/profile
if [ $? -eq 1 ];then
sudo sed -i'' '$aexport PATH=${ZOOKEEPER_CLIENT_HOME}/bin:${PATH}' ~/.zxhtom/profile
fi
#刷新环境变量
grep -q 'source '"${HOME}"'/.zxhtom/profile' ~/.zshrc
if [ $? -eq 1 ];then
sudo sed -i'' '$asource '"${HOME}"'/.zxhtom/profile' ~/.zshrc
fi
sudo source ~/.zshrc
