CURRENTPATH=$(readlink -f "$0")
arr=(`echo ${CURRENTPATH} | tr '/' ' '`)
shell_name=${arr[${#arr[@]}-1]}
echo "current shell name = "${shell_name}
DIR=$( dirname "$(readlink -f  ${BASH_SOURCE[0]})" )
echo "current shell in path : "$DIR
files=`ls ${DIR}`
for file in ${files[@]}
do
	if [[ "${file}" = "${shell_name}" ]]
	then
		echo "do not install self"
	else
		echo "start install file="$file
		sh ${DIR}/$file
	fi
done
