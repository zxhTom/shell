#!/bin/bash
if [[ ! -z $1 ]] && [[ -d $1 ]] && [[ -z ${unline_path} ]]
then
PREHOME=$1
elif [[ ! -z ${unline_path} ]]
then
PREHOME=${unline_path}
else
PREHOME=${HOME}"/unline"
fi
if [[ ! -f ${PREHOME}/bin/lazydocker ]]
then
  if [[ ! -d ${PREHOME}/tar ]]
  then
    mkdir -p ${PREHOME}/tar
  fi
  if [[ ! -f ${PREHOME}/tar/lazydocker_0.8_Linux_x86_64.tar.gz ]]
  then
    wget -P ${PREHOME}/tar/ https://github.com/jesseduffield/lazydocker/releases/download/v0.8/lazydocker_0.8_Linux_x86_64.tar.gz
  fi
  if [[ ! -d ${HOME}/temp ]]
  then
    mkdir -p ${HOME}/temp
  fi
  tar -zxvf ${PREHOME}/tar/lazydocker_0.8_Linux_x86_64.tar.gz -C ${HOME}/temp/
  cp ${HOME}/temp/lazydocker ${PREHOME}/bin/
fi
# begin install to local bin repository
if [[ ! -f ${HOME}/.local/bin ]]
then
  mkdir -p ${HOME}/.local/bin
fi
sudo  cp ${PREHOME}/bin/lazydocker ${HOME}/.local/bin/
sudo chmod +x ${HOME}/.local/bin/lazydocker
if [[ ! -f /usr/bin/lazydocker ]]
then
sudo ln -s ${HOME}/.local/bin/lazydocker /usr/bin/lazydocker
fi
