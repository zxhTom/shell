#!/bin/bash
if [[ ! -z $1 ]] && [[ -d $1 ]] && [[ -z ${unline_path} ]]
then
PREHOME=$1
elif [[ ! -z ${unline_path} ]]
then
PREHOME=${unline_path}
else
PREHOME=${HOME}"/unline"
fi
if [[ ! -f ${PREHOME}/bin/ytop ]]
then
  if [[ ! -d ${PREHOME}/tar ]]
  then
    mkdir -p ${PREHOME}/tar
  fi
  if [[ ! -f ${PREHOME}/tar/ytop-0.5.1-x86_64-unknown-linux-gnu.tar.gz ]]
  then
    wget -P ${PREHOME}/tar/ https://github.com/cjbassi/ytop/releases/download/0.5.1/ytop-0.5.1-x86_64-unknown-linux-gnu.tar.gz
  fi
  if [[ ! -d ${HOME}/temp ]]
  then
    mkdir -p ${HOME}/temp
  fi
  tar -zxvf ${PREHOME}/tar/ytop-0.5.1-x86_64-unknown-linux-gnu.tar.gz -C ${HOME}/temp/
  cp ${HOME}/temp/ytop ${PREHOME}/bin/
fi
# begin install to local bin repository
if [[ ! -f ${HOME}/.local/bin ]]
then
  mkdir -p ${HOME}/.local/bin
fi
sudo  cp ${PREHOME}/bin/ytop ${HOME}/.local/bin/
sudo chmod +x ${HOME}/.local/bin/ytop
if [[ ! -f /usr/bin/ytop ]]
then
sudo ln -s ${HOME}/.local/bin/ytop /usr/bin/ytop
fi
