DIR="$( cd "$( dirname "$(readlink -f  ${BASH_SOURCE[0]})" )" && pwd )"
echo ${DIR}
sh ${DIR}/lazygit/start.sh
sh ${DIR}/neovim/start.sh
sh ${DIR}/asynctask/start.sh
sh ${DIR}/client/install.sh
sh ${DIR}/font.sh
sh ${DIR}/tmux/start.sh
sh ${DIR}/keyboard.sh
sh ${DIR}/cron.sh
sh ${DIR}/resetaddapt.sh
sh ${DIR}/project/mvn.sh
sh ${DIR}/bin/install.sh
