#!/bin/bash
#解压至/opt/soft/maven
if [ ! -d "/opt/soft/maven" ];then
sudo mkdir -p /opt/soft/maven
else
sudo rm -rf /opt/soft/maven/*
fi
if [[ ! -z $1 ]] && [[ -d $1 ]] && [[ -z ${unline_path} ]]
then
  PREHOME=$1
elif [[ ! -z ${unline_path} ]]
then
  PREHOME=${unline_path}
else
  PREHOME=${HOME}"/unline"
fi

#当前脚本所在路径
CURRENTPATH=$(readlink -f "$0")
CUR=`pwd`
echo "当前脚本位置。。。。"${CURRENTPATH}
echo "..."${CUR}
DIR=$( dirname "$(readlink -f  ${BASH_SOURCE[0]})" )
echo ${DIR}
#当前脚本上一层路径
PARENTPATH=$(dirname ${DIR})
SOURCEFILE=`sh ${PARENTPATH}/getunlinefile.sh ${PREHOME} maven`
sudo tar -zxvf ${PREHOME}/maven/${SOURCEFILE} -C /opt/soft/maven --strip-components=1
localRepository=`cat /opt/soft/maven/conf/settings.xml | awk -v ORS=',,,,,' '{print $0}' | sed -r 's/<!--[^-]+-->/a/' | awk -v RS=',,,,,' '{print $0}' | grep -n 'localRepository'`
if [[ -z ${localRepository} ]]
then
# 去最后一次出现localRepository行号
echo "未检测到仓库路径....."
LINENUM=`cat /opt/soft/maven/conf/settings.xml| grep -n 'localRepository'|awk -F "[: ]+" '{print $1}' | sort -nr | head -n 1`
LINENUM=$(($LINENUM+1))
mkdir -p ${HOME}/env/repository
sudo sed -i'' ''"${LINENUM}"'a<localRepository>'"${HOME}"'/env/repository</localRepository>' /opt/soft/maven/conf/settings.xml
fi
grep -q 'export MAVEN_HOME=/opt/soft/maven' ~/.zxhtom/profile
if [ $? -eq 1 ];then
sudo sed -i '$aexport MAVEN_HOME=/opt/soft/maven' ~/.zxhtom/profile
fi
grep -q 'export M2_HOME=/opt/soft/maven' ~/.zxhtom/profile
if [ $? -eq 1 ];then
sudo sed -i '$aexport M2_HOME=/opt/soft/maven' ~/.zxhtom/profile
fi
grep -q 'export PATH=${MAVEN_HOME}/bin:${PATH}' ~/.zxhtom/profile
if [ $? -eq 1 ];then
sudo sed -i '$aexport PATH=${MAVEN_HOME}/bin:${PATH}' ~/.zxhtom/profile
fi
#刷新环境变量
grep -q 'source '"${HOME}"'/.zxhtom/profile' ~/.zshrc
if [ $? -eq 1 ];then
sudo sed -i'' '$asource '"${HOME}"'/.zxhtom/profile' ~/.zshrc
fi
source ~/.zshrc
