if [[ ! -z $1 ]] && [[ -d $1 ]] && [[ -z ${unline_path} ]]
then
PREHOME=$1
elif [[ ! -z ${unline_path} ]]
then
PREHOME=${unline_path}
else
PREHOME=${HOME}"/unline"
fi
#当前脚本所在路径
CURRENTPATH=$(readlink -f "$0")
CUR=`pwd`
echo "当前脚本位置。。。。"${CURRENTPATH}
echo "..."${CUR}
DIR=$( dirname "$(readlink -f  ${BASH_SOURCE[0]})" )
echo ${DIR}
#当前脚本上一层路径
PARENTPATH=$(dirname ${DIR})
if [[ ! -d ${HOME}/.local/github ]]
then
  mkdir -p ${HOME}/.local/github
fi
sh ${PARENTPATH}/tools/gitclone.sh https://github.com/charmbracelet/glow.git  ${HOME}/.local/github/glow
cd ${HOME}/.local/github/glow
go build
sudo ln -sf ${HOME}/.local/github/glow/glow ${HOME}/.local/bin/glow 
