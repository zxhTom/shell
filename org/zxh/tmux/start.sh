#!/bin/bash
PREHOME="${HOME}/unline"
if [[ ! -z $1 ]] && [[ -d $1 ]] && [[ -z ${unline_path} ]]
then
PREHOME=$1
elif [[ ! -z ${unline_path} ]]
then
PREHOME=${unline_path}
else
PREHOME=${HOME}"/unline"
fi
DIR=$( dirname "$(readlink -f  ${BASH_SOURCE[0]})" )
#当前脚本上一层路径
PARENTPATH=$(dirname ${DIR})
# 检测是否安装tmux
tmuxIndx=0
type tmux || tmuxIndex=1
if [[ $tmuxIndex -eq 0 ]]
then
    echo exit
else
    #echo y | sudo apt-get install tmux
    sh ${DIR}/tmuxinstall.sh
    echo y | sudo apt-get install gawk
fi

if [[ -d ${PREHOME}/tmux ]]
then
  if [[ ! -d ${HOME}/.tmux ]]
  then
    mkdir -p ${HOME}/.tmux/
  fi
  cp -R ${PREHOME}/tmux/* ${HOME}/.tmux/
fi
# 移动配置tmux.conf到默认位置，每次关闭tmux后都回读取默认地址配置
mv ~/.tmux/tmux.conf ~/.tmux.conf
# 解压插件到默认位置 `~/.tmux/` 目录下
if [[ ! -d ${HOME}/.tmux/plugins ]]
then
unzip ~/.tmux/plugins.zip -d ~/.tmux/
fi
sh ${PARENTPATH}/tools/gitclone.sh https://github.com/tmux-plugins/tpm ~/.tmux/tpm
# sudo chown -R ${USER}:${USER} ~/.tmux
tmux source-file ${HOME}/.tmux.conf
sh ~/.tmux/plugins/tmux-fingers/tmux-fingers.tmux
sh ${DIR}/popup.sh
sh ${PARENTPATH}/terminal/neovimcp.sh
