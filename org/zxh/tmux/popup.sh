echo "start install popup...."
PREHOME="${HOME}/unline"
if [[ ! -z $1 ]] && [[ -d $1 ]] && [[ -z ${unline_path} ]]
then
PREHOME=$1
elif [[ ! -z ${unline_path} ]]
then
PREHOME=${unline_path}
else
PREHOME=${HOME}"/unline"
fi
grep -q "alias p" ~/.zxhtom/profile
if [ $? -eq 1 ];then
cat ${HOME}/.tmux/script/alias >> ${HOME}/.zxhtom/profile
fi
DIR=$( dirname "$(readlink -f  ${BASH_SOURCE[0]})" )
echo ${DIR}
#当前脚本上一层路径
PARENTPATH=$(dirname ${DIR})
sh ${PARENTPATH}/tools/gitclone.sh https://github.com/junegunn/fzf.git ~/.fzf/
SOURCEFILE=`sh ${PARENTPATH}/getunlinefile.sh ${PREHOME} fzf`
if [[ ! -d ${HOME}/.fzf/bin ]]
then
    mkdir -p ${HOME}/.fzf/bin
fi
tar -zxf ${PREHOME}/fzf/${SOURCEFILE} -C ${HOME}/.fzf/bin
sh ${HOME}/.fzf/install<<EOF
y
y
y
EOF
cp ~/.tmux/script/fzfp ~/.fzf/bin
chmod +x ~/.fzf/bin/fzfp
sudo cp ~/.tmux/script/toggle-tmux-popup /usr/bin/
sudo chmod +x /usr/bin/toggle-tmux-popup
