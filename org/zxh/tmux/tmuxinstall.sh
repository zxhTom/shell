if [[ ! -z $1 ]] && [[ -d $1 ]] && [[ -z ${unline_path} ]]
then
PREHOME=$1
elif [[ ! -z ${unline_path} ]]
then
PREHOME=${unline_path}
else
PREHOME=${HOME}"/unline"
fi

DIR=$( dirname "$(readlink -f  ${BASH_SOURCE[0]})" )
echo ${DIR}
if [ ! -d "/opt/soft/tmux" ];then
sudo mkdir -p /opt/soft/tmux
fi
#当前脚本上一层路径
PARENTPATH=$(dirname ${DIR})
SOURCEFILE=`sh ${PARENTPATH}/getunlinefile.sh ${PREHOME} tmux/executor`
sudo tar -zxvf ${PREHOME}/tmux/executor/${SOURCEFILE} -C /opt/soft/tmux --strip-components=1
echo y | sudo apt-get install libevent-dev
echo y | sudo apt-get install  ncurses-dev
cur=`pwd`
cd /opt/soft/tmux
sudo ./configure && sudo make
sudo make install
sudo cp tmux /usr/bin
cd ${cur}
